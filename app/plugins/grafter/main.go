package main

// compile cmd
// tinygo build -o grafter-0.0.1.wasm -scheduler=none --no-debug -target=wasi ./

import (
	_ "embed"
	"fmt"
	"io"
	"os"
	"path/filepath"
	"regexp"
	"strings"

	gitroot "gitroot.com/libs/goplugin"
)

//go:embed defaultConf.yml
var defaultConf string

type Plugin struct {
	server   gitroot.Server
	conf     pluginConf
	newGraft *graft
}

type pluginConf struct {
	defaultTargetBranch string
}

type graft struct {
	isCreation         bool
	filename           string
	branch             string
	sshUser            string
	commitsLines       []string
	pushContent        []string
	content            string
	isCommitReview     bool
	isPostCommitReview bool
	currentHash        string
}

var defaultGraft = `---
- target: %s
- status: draft
---

# Want to merge %s

Please add description of your work here.

When your changes are ready to be reviewed, add at the end of this file a comment with:

` + strings.Repeat("`", 3) + `markdown
---

/review
` + strings.Repeat("`", 3) + "\n"

var defaultReviewMessage = `
---

Please review changes with:

` + strings.Repeat("`", 3) + `shell
git fetch origin %s
git checkout %s
git diff origin/HEAD..HEAD
` + strings.Repeat("`", 3) + `

Make your changes about the code inside the code directly (remarks, questions can be made in the form of comments). General remarks on the why can be made here. When done commit and push directly on this branch. Graft plugin will make the diff and display it here.
`

var targetRegexp = regexp.MustCompile(`- target:\s(.*)`)

func (p *Plugin) Init(repoName string, conf []map[string]string) error {
	defaultTargetBranch := "main"
	if len(conf) > 0 {
		if b, ok := conf[0]["defaultTargetBranch"]; ok {
			defaultTargetBranch = b
		}
	}
	p.conf = pluginConf{
		defaultTargetBranch: defaultTargetBranch,
	}
	p.newGraft = nil
	return nil
}

func (p *Plugin) StartCommit(commit gitroot.Commit) error {
	//does nothing if user push on main
	if commit.Branch == p.conf.defaultTargetBranch {
		return nil
	}
	commitLine := fmt.Sprintf("### %s\n%s\n", commit.Message, commit.Hash)
	if p.newGraft == nil {
		graftFilepath := fmt.Sprintf("grafts/%s.md", commit.Branch)
		file, err := p.server.Worktree().Open(graftFilepath)
		if err != nil && !os.IsNotExist(err) {
			return err
		}
		if file == nil {
			p.newGraft = &graft{
				isCreation:   true,
				filename:     graftFilepath,
				branch:       commit.Branch,
				sshUser:      commit.SshUser,
				commitsLines: []string{commitLine},
				content:      fmt.Sprintf(defaultGraft, p.conf.defaultTargetBranch, commit.Branch),
			}
		} else {
			filecontent, err := io.ReadAll(file)
			if err != nil {
				return err
			}
			p.newGraft = &graft{
				isCreation:   false,
				filename:     graftFilepath,
				branch:       commit.Branch,
				sshUser:      commit.SshUser,
				commitsLines: []string{commitLine},
				content:      string(filecontent),
			}
		}
	} else {
		p.newGraft.commitsLines = append(p.newGraft.commitsLines, commitLine)
	}
	p.newGraft.pushContent = []string{}
	reviews := strings.Split(p.newGraft.content, "/review")
	somethingAfterUserReview := len(reviews) >= 3 && strings.Contains(reviews[len(reviews)-1], "---")
	p.newGraft.isCommitReview = len(reviews) >= 3 && !somethingAfterUserReview
	p.newGraft.isPostCommitReview = somethingAfterUserReview
	p.newGraft.currentHash = commit.Hash
	return nil
}

func (p *Plugin) AddFile(path string) error {
	//does nothing if user push on main
	if p.newGraft == nil {
		return nil
	}
	if path != p.newGraft.filename {
		from, _ := filepath.Abs(path)
		relPath, _ := filepath.Rel("/grafts/", from)
		p.newGraft.pushContent = append(p.newGraft.pushContent, fmt.Sprintf("- ++ [%s](%s)", path, filepath.Clean(relPath)))
	}
	return nil
}

func (p *Plugin) ModFile(path string) error {
	//does nothing if user push on main
	if p.newGraft == nil {
		return nil
	}
	if path != p.newGraft.filename {
		from, _ := filepath.Abs(path)
		relPath, _ := filepath.Rel("/grafts/", from)
		p.newGraft.pushContent = append(p.newGraft.pushContent, fmt.Sprintf("- +- [%s](%s)", path, filepath.Clean(relPath)))
		if p.newGraft.isPostCommitReview {
			if diff, err := p.server.DiffWithParent(path, p.newGraft.currentHash); err == nil {
				p.newGraft.pushContent = append(p.newGraft.pushContent, diff)
			} else {
				p.server.LogError("can't get diff", err)
			}
		}
	}
	return nil
}

func (p *Plugin) DelFile(path string) error {
	//does nothing if user push on main
	if p.newGraft == nil {
		return nil
	}
	if path != p.newGraft.filename {
		from, _ := filepath.Abs(path)
		relPath, _ := filepath.Rel("/grafts/", from)
		p.newGraft.pushContent = append(p.newGraft.pushContent, fmt.Sprintf("- -- [%s](%s)", path, filepath.Clean(relPath)))
	}
	return nil
}

func (p *Plugin) RenameFile(fromPath string, toPath string) error {
	//does nothing if user push on main
	if p.newGraft == nil {
		return nil
	}
	if toPath != p.newGraft.filename {
		from, _ := filepath.Abs(toPath)
		relPath, _ := filepath.Rel("/grafts/", from)
		p.newGraft.pushContent = append(p.newGraft.pushContent, fmt.Sprintf("- %s -> [%s](%s)", fromPath, toPath, filepath.Clean(relPath)))
	}
	return nil
}

func (p *Plugin) EndCommit(commit gitroot.Commit) error {
	//does nothing if user push on main
	if p.newGraft == nil {
		return nil
	}
	if len(p.newGraft.pushContent) > 0 {
		p.newGraft.commitsLines[len(p.newGraft.commitsLines)-1] = fmt.Sprintf("%s\n%s\n", p.newGraft.commitsLines[len(p.newGraft.commitsLines)-1], strings.Join(p.newGraft.pushContent, "\n"))
	} else {
		// a commit which touch others files or only graft file is ignored
		p.newGraft.commitsLines = p.newGraft.commitsLines[:len(p.newGraft.commitsLines)-1]
	}
	return nil
}

func (p *Plugin) Finish() error {
	// does nothing if user push on main
	if p.newGraft == nil {
		return nil
	}
	if len(p.newGraft.commitsLines) > 0 {
		commit := "commit"
		if len(p.newGraft.commitsLines) > 1 {
			commit = "commits"
		}
		content := fmt.Sprintf("%s\n---\n\n## Push %d %s\n\n%s", p.newGraft.content, len(p.newGraft.commitsLines), commit, strings.Join(p.newGraft.commitsLines, "\n"))
		p.server.ModifyContent(p.newGraft.filename, content)
	}
	if p.newGraft.isCommitReview {
		content := strings.Replace(p.newGraft.content, "- status: draft", "- status: review\n- reviewers: []", 1)
		content = content + fmt.Sprintf(defaultReviewMessage, p.newGraft.branch, p.newGraft.branch)
		p.server.ModifyContent(p.newGraft.filename, content)
	}
	needMerge := strings.Contains(p.newGraft.content, "/merge")
	if needMerge {
		content := strings.Replace(p.newGraft.content, "- status: review", "- status: done", 1)
		p.server.ModifyContent(p.newGraft.filename, content)
	}
	message := "graft updated"
	if p.newGraft.isCreation {
		message = "graft created"
	}
	p.server.CommitAllIfNeeded(message)
	if needMerge {
		all := targetRegexp.FindStringSubmatch(p.newGraft.content)
		if len(all) > 1 {
			p.server.Merge(all[1], p.newGraft.branch)
		} else {
			p.server.Merge(p.conf.defaultTargetBranch, p.newGraft.branch)
		}
	}
	return nil
}

func Build(server gitroot.Server) gitroot.Plugin {
	return &Plugin{
		server: server,
	}
}

//go:wasmexport install
func main() {
	gitroot.Register(defaultConf, "**/*", []string{"*"}, Build)
}
