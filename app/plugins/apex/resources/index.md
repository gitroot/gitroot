# Welcome on your web page

This is the face of your project on the web.

## Why it's not my README.md?

README.md is good for developpers, but not necessary for your user. Explain here the `what` and `why` of your project, not the `how`.

## How to customize?

Just modify this file, commit and push. You can also change the menu in the conf.
