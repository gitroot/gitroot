package main

import (
	"slices"
	"strconv"
	"strings"
)

type conf struct {
	repoName string
	style    string
	menu     []link
}

type link struct {
	index   int
	link    string
	display string
}

func NewConf(repoName string, c []map[string]string) *conf {
	conf := &conf{
		repoName: repoName,
		style:    "/styles/style.css",
		menu: []link{
			{index: 0, display: "🏠 Home", link: "/"},
			{index: 1, display: "📖 Readme", link: "/README.html"},
		},
	}
	if len(c) > 0 {
		if css, ok := c[0]["style"]; ok {
			conf.style = css
		}
		all := map[int]link{}
		for key, value := range c[0] {
			if strings.HasPrefix(key, "menu") {
				indexKind := strings.Split(key, "_")
				index, err := strconv.Atoi(indexKind[1])
				if err != nil {
					continue
				}
				kind := indexKind[2]
				if val, ok := all[index]; ok {
					l := link{
						index:   index,
						link:    val.link,
						display: val.display,
					}
					if kind == "link" {
						l.link = value
					} else {
						l.display = value
					}
					all[index] = l
				} else {
					l := link{
						index:   index,
						link:    "",
						display: "",
					}
					if kind == "link" {
						l.link = value
					} else {
						l.display = value
					}
					all[index] = l
				}
			}
		}
		conf.menu = make([]link, 0)
		for _, l := range all {
			if l.link != "" && l.display != "" {
				conf.menu = append(conf.menu, l)
			}
		}
		slices.SortFunc(conf.menu, func(a link, b link) int {
			return a.index - b.index
		})
	}
	return conf
}
