package main

// compile cmd
// tinygo build -o apex-0.0.1.wasm -scheduler=none --no-debug -target=wasi ./

import (
	_ "embed"
	"errors"
	"fmt"
	"io/fs"
	"path/filepath"
	"strings"

	gitroot "gitroot.com/libs/goplugin"
)

const simple = `<html>
<head>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<title>%s</title>
<link rel="stylesheet" href="%s">
</head>
<body>
<header>
	<h1>%s</h1>
	<nav>
	%s
	</nav>
</header>
%s
<footer>
	<p>Clone with <code>git clone ssh://%s/%s</code></p>
	<small>Hosted with ❤️ by Gitroot</small>
</footer>
</body>
</html>
`

//go:embed resources/defaultConf.yml
var defaultConf string

//go:embed resources/styles/pico.min.css
var picoStyle string

//go:embed resources/styles/simple.min.css
var simpleStyle string

//go:embed resources/index.md
var index string

type Plugin struct {
	server gitroot.Server
	config *conf
}

func (p *Plugin) Init(repoName string, c []map[string]string) error {
	p.config = NewConf(repoName, c)
	p.server.Log("Style=" + p.config.style)
	if _, err := fs.Stat(p.server.Webcontent(), "styles/style.css"); errors.Is(err, fs.ErrNotExist) {
		if p.config.style == "pico.min.css" {
			p.server.ModifyWebContent(p.config.style, picoStyle)
		} else if p.config.style == "simple.min.css" {
			p.server.ModifyWebContent(p.config.style, simpleStyle)
		} else {
			// TODO download if distant? Copy if local?
		}
	} else if err != nil {
		p.server.LogError("can't stats styles", err)
	}
	if _, err := fs.Stat(p.server.Webcontent(), "index.html"); errors.Is(err, fs.ErrNotExist) {
		p.server.ModifyContent("index.md", index)
		p.server.CommitAllIfNeeded("init web page")
		p.AddFile("index.md")
	} else if err != nil {
		p.server.LogError("can't stats index", err)
	}
	return nil
}

func (p *Plugin) StartCommit(commit gitroot.Commit) error {
	return nil
}

func (p *Plugin) AddFile(fp string) error {
	if strings.HasSuffix(fp, ".html") || strings.HasSuffix(fp, ".css") {
		content, err := fs.ReadFile(p.server.Worktree(), fp)
		if err != nil {
			p.server.LogError("AddFile ReadFile "+fp, err)
			return nil
		}
		p.server.ModifyWebContent(fp, string(content))
	} else if strings.HasSuffix(fp, ".md") {
		content, err := fs.ReadFile(p.server.Worktree(), fp)
		if err != nil {
			p.server.LogError("AddFile ReadFile "+fp, err)
			return nil
		}
		menu := p.buildMenu(fp)
		stylePath := p.relativePath(fp, p.config.style)
		newContent := fmt.Sprintf(simple, p.config.repoName, stylePath, p.config.repoName, menu, string(mdToHTML(content, p.readInclude)), "TODO", p.config.repoName)
		p.server.ModifyWebContent(fmt.Sprintf("%s.html", strings.TrimSuffix(fp, ".md")), newContent)
	}
	return nil
}

func (p *Plugin) relativePath(fromPath string, toPath string) string {
	absFromPath, _ := filepath.Abs(fromPath)
	dirFromPath, _ := filepath.Split(absFromPath)
	absToPath, _ := filepath.Abs(toPath)
	dirToPath, fileToPath := filepath.Split(absToPath)
	path, err := filepath.Rel(dirFromPath, dirToPath)
	if err != nil {
		return toPath
	}
	return fmt.Sprintf("%s/%s", path, fileToPath)
}

func (p *Plugin) buildMenu(fp string) string {
	menu := "<ul>"
	for _, link := range p.config.menu {
		l := p.relativePath(fp, link.link)
		menu = menu + fmt.Sprintf("<li><a href=\"%s\">%s</a></li>", l, link.display)
	}
	return menu + "</ul>"
}

func (p *Plugin) readInclude(from, path string, address []byte) []byte {
	content, _ := fs.ReadFile(p.server.Worktree(), path)
	return content
}

func (p *Plugin) DelFile(fp string) error {
	return nil
}

func (p *Plugin) ModFile(fp string) error {
	return p.AddFile(fp)
}

func (p *Plugin) EndCommit(commit gitroot.Commit) error {
	return nil
}

func (p *Plugin) RenameFile(fromPath string, toPath string) error {
	return nil
}

func (p *Plugin) Finish() error {
	return nil
}

func Build(server gitroot.Server) gitroot.Plugin {
	return &Plugin{
		server: server,
	}
}

//go:wasmexport install
func main() {
	gitroot.Register(defaultConf, "**/*", []string{"main"}, Build)
}
