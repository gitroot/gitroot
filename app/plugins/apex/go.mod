module gitroot.com/plugins/apex

go 1.22.4

replace gitroot.com/libs/goplugin => ../../libs/goplugin

require (
	github.com/gomarkdown/markdown v0.0.0-20240930133441-72d49d9543d8
	gitroot.com/libs/goplugin v0.0.0-00010101000000-000000000000
)
