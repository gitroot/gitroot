#!/usr/bin/env bash

build() {
    start=`date +%s`
    echo "🏁 $1"
    cd $1
    tinygo build -buildmode=c-shared -o $1-0.0.1.wasm -scheduler=none --no-debug -target=wasip1 ./
    cd ..
    end=`date +%s`
    echo "🟢 $1 in `expr $end - $start` seconds"
}

startAll=`date +%s`
build apex
build grafter
build ladybug
build silo
endAll=`date +%s`
echo "🟢 all in `expr $endAll - $startAll` seconds"