module gitroot.com/plugins/issue

go 1.22.4

replace gitroot.com/libs/goplugin => ../../libs/goplugin

require (
	github.com/sigurn/crc16 v0.0.0-20240131213347-83fcde1e29d1
	gitroot.com/libs/goplugin v0.0.0-00010101000000-000000000000
)
