package main

import (
	"bytes"
	"fmt"
	"io/fs"
	"path/filepath"
	"regexp"
	"strings"
)

type conf struct {
	title       string
	description string
	format      string
	tableHeader string
	filepath    string
	where       string
	selections  []*regexp.Regexp
	to          string
	paginator   int
	sort        string
	sortOrder   string
}

func (c conf) initFile() string {
	page := fmt.Sprintf("# %s\n", c.title)
	if c.description != "" {
		page = fmt.Sprintf("# %s\n\n%s\n", c.title, c.description)
	}
	if c.format == "table" {
		nb := len(c.selections) + 1
		if c.tableHeader != "" {
			page = fmt.Sprintf("%s\n%s\n", page, c.tableHeader)
		} else {
			page = fmt.Sprintf("%s\n|%s\n", page, strings.Repeat("   |", nb))
		}
		page = fmt.Sprintf("%s|%s", page, strings.Repeat("---|", nb))
	}
	return page
}

func (c conf) relativePath(toPath string) string {
	absFromPath, _ := filepath.Abs(c.to)
	dirFromPath, _ := filepath.Split(absFromPath)
	absToPath, _ := filepath.Abs(toPath)
	dirToPath, fileToPath := filepath.Split(absToPath)
	path, err := filepath.Rel(dirFromPath, dirToPath)
	if err != nil {
		return toPath
	}
	return fmt.Sprintf("%s/%s", path, fileToPath)
}

func (c conf) makeLink(p *Plugin, cacheLine CacheLine) string {
	path := c.relativePath(cacheLine.fp)
	prepend := ""
	if c.format == "list" {
		prepend = "- "
	} else if c.format == "task" {
		prepend = "- [ ] "
	} else if c.format == "table" {
		prepend = "| "
	} else if c.format == "embed" {
		prepend = "### "
	}

	append := ""
	if c.format == "table" {
		append = " |"
	} else if c.format == "embed" {
		append = "\n\n"
	}
	t := cacheLine.selects
	if len(t) > 0 {
		for i, a := range t {
			if i == 0 && c.format == "embed" {
				append = fmt.Sprintf("%s%s", append, a)
			} else if c.format == "table" {
				append = fmt.Sprintf("%s %s |", append, a)
			} else if a != " " { //special case du to serialization in cache
				append = fmt.Sprintf("%s %s", append, a)
			}
		}
	}
	if c.format == "embed" {
		append = fmt.Sprintf("%s\n%s\n", append, c.findEmbed(p, cacheLine.fp))
	}

	return fmt.Sprintf("%s[%s](%s)%s", prepend, cacheLine.title, path, append)
}

func findTitle(path string, content []byte) string {
	if strings.HasSuffix(path, ".md") { //special case, search first title
		re := regexp.MustCompile(`(?m)^#\s(.*)$`)
		t := re.FindSubmatch(content)
		if len(t) > 1 {
			return string(t[1])
		}
	}
	return path
}

func (c conf) findEmbed(p *Plugin, path string) string {
	content, err := fs.ReadFile(p.server.Worktree(), path)
	if err != nil {
		p.server.LogError("can't find file", err)
		return ""
	}
	if strings.HasSuffix(path, ".md") { //special case, search first description
		re := regexp.MustCompile(`(?m)^#\s(.*)$`)
		t := re.FindSubmatch(content)
		hasTitle := len(t) > 1
		hasMeta := bytes.Contains(content, []byte("---"))
		contents := bytes.Split(content, []byte("\n"))
		for i, l := range contents {
			titleCase := hasTitle && bytes.HasPrefix(l, []byte("# "))
			metaCase := !hasTitle && hasMeta && i > 0 && bytes.HasPrefix(l, []byte("---"))
			bodyCase := !hasTitle && !hasMeta
			if titleCase || metaCase || bodyCase {
				res := ""
				if i+1 < len(contents) && len(contents[i+1]) > 0 {
					res = fmt.Sprintf("%s\n%s", res, string(contents[i+1]))
				}
				if i+2 < len(contents) && len(contents[i+2]) > 0 {
					res = fmt.Sprintf("%s\n%s", res, string(contents[i+2]))
				}
				if i+3 < len(contents) && len(contents[i+3]) > 0 {
					res = fmt.Sprintf("%s\n%s", res, string(contents[i+3]))
				}
				return res
			}
		}
	} else {
		contents := bytes.Split(content, []byte("\n"))
		for i, l := range contents {
			if bytes.Contains(l, []byte(c.where)) {
				var buffer bytes.Buffer
				buffer.Write([]byte("```\n"))
				if i > 0 {
					buffer.Write(contents[i-1])
					buffer.Write([]byte("\n"))
				}
				buffer.Write(l)
				if i < len(contents) {
					buffer.Write([]byte("\n"))
					buffer.Write(contents[i+1])
				}
				buffer.Write([]byte("\n```"))
				return buffer.String()
			}
		}
	}

	return "-"
}
