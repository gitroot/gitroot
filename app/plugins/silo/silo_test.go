package main

import (
	"embed"
	"io/fs"
	"os"
	"testing"
)

//go:embed test_ressources/*
var fakeFs embed.FS

type fakeServer struct {
	calledModifyContent string
	dirCache            string
}

func (s *fakeServer) Worktree() fs.FS {
	return fakeFs
}

func (s *fakeServer) Webcontent() fs.FS {
	return nil
}

func (s *fakeServer) Cache() fs.FS {
	if s.dirCache == "" {
		s.dirCache, _ = os.MkdirTemp("", "silo*")
		os.Stdout.WriteString("dirCache = " + s.dirCache + "\n")
	}

	return os.DirFS(s.dirCache)
}

func (s *fakeServer) ModifyContent(filepath, content string) {
	s.calledModifyContent = content
}

func (s *fakeServer) ModifyWebContent(filepath, content string) {

}

func (s *fakeServer) ModifyCacheContent(filepath, content string) {
	os.Stdout.WriteString(filepath + " => " + content + "\n")
	os.WriteFile(s.dirCache+"/"+filepath, []byte(content), fs.ModePerm)
}

func (s *fakeServer) CommitAllIfNeeded(message string) {

}

func (s *fakeServer) DiffWithParent(file string, hash string) (string, error) {
	return "", nil
}

func (s *fakeServer) Log(message string) {

}

func (s *fakeServer) LogError(message string, err error) {

}

func (s *fakeServer) Merge(from string, to string) {

}

type dataTest struct {
	conf       []map[string]string
	inputFile  []string
	outputFile string
	deleteFile []string
}

var data = []dataTest{{
	conf:       []map[string]string{{"title": "Roadmaps", "for": "*/*.md", "where": "type: 'test'", "to": "test_ressources/board.md"}},
	inputFile:  []string{"test_ressources/issue.md", "test_ressources/issue2.md"},
	outputFile: "test_ressources/board.md",
}, {
	conf:       []map[string]string{{"title": "Roadmaps", "for": "*/*.md", "where": "type: 'test'", "to": "test_ressources/board.md"}},
	deleteFile: []string{"test_ressources/issue.md"},
	outputFile: "test_ressources/board_delete.md",
}, {
	conf:       []map[string]string{{"title": "Roadmaps", "for": "*/*.md", "where": "type: 'test'", "format": "table", "to": "test_ressources/boardNotExisting.md", "description": "> Beautiful!!"}},
	inputFile:  []string{"test_ressources/issue.md", "test_ressources/issue2.md"},
	outputFile: "test_ressources/board_table.md",
}, {
	conf:       []map[string]string{{"title": "Roadmaps", "for": "*/*.md", "where": "type: 'test'", "format": "table", "to": "test_ressources/boardNotExisting2.md", "description": "> Beautiful!!", "select": "priority: (\\d+)"}},
	inputFile:  []string{"test_ressources/issue.md", "test_ressources/issue2.md"},
	outputFile: "test_ressources/board_table_select.md",
}, {
	conf:       []map[string]string{{"title": "Roadmaps", "for": "*/*.md", "where": "type: 'test'", "format": "table", "to": "test_ressources/boardNotExisting3.md", "description": "> Beautiful!!", "select": "priority2: (\\d+)"}},
	inputFile:  []string{"test_ressources/issue.md", "test_ressources/issue2.md"},
	outputFile: "test_ressources/board_table_select_inexistent.md",
}, {
	conf:       []map[string]string{{"title": "Roadmaps", "for": "*/*.md", "where": "type: 'test'", "format": "task", "to": "test_ressources/boardNotExisting4.md", "description": "> Beautiful!!", "select": "(priority:) (\\d+)"}},
	inputFile:  []string{"test_ressources/issue.md", "test_ressources/issue2.md"},
	outputFile: "test_ressources/board_task.md",
}, {
	conf:       []map[string]string{{"title": "Roadmaps", "for": "*/*.md", "where": "type: 'test'", "format": "embed", "to": "test_ressources/boardNotExisting5.md", "description": "> Beautiful!!", "select": "(priority:) (\\d+)"}},
	inputFile:  []string{"test_ressources/issue.md", "test_ressources/issue2.md"},
	outputFile: "test_ressources/board_embed.md",
}}

func TestIssue(t *testing.T) {
	s := &fakeServer{}
	for _, d := range data {
		p := Build(s)
		p.Init("repoName", d.conf)
		if d.inputFile != nil {
			for _, fileInput := range d.inputFile {
				p.AddFile(fileInput)
			}
			p.Finish()
			res, _ := fs.ReadFile(fakeFs, d.outputFile)
			if s.calledModifyContent != string(res) {
				t.Errorf("%s= \n---\n%s\n---\n\nwant\n\n---\n%s\n---", d.outputFile, s.calledModifyContent, string(res))
			}
		}
		if d.deleteFile != nil {
			for _, fileInput := range d.deleteFile {
				p.DelFile(fileInput)
			}
			p.Finish()
			res, _ := fs.ReadFile(fakeFs, d.outputFile)
			if s.calledModifyContent != string(res) {
				t.Errorf("%s= \n---\n%s\n---\n\nwant\n\n---\n%s\n---", d.outputFile, s.calledModifyContent, string(res))
			}
		}
	}
}
