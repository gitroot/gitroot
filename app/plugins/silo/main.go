package main

// compile cmd
// tinygo build -o silo-0.0.1.wasm -scheduler=none --no-debug -target=wasi ./

import (
	_ "embed"
	"fmt"
	"math"
	"path/filepath"
	"regexp"
	"slices"
	"strconv"
	"strings"

	gitroot "gitroot.com/libs/goplugin"
)

//go:embed defaultConf.yml
var defaultConf string

const cachePath string = "boards"

type Plugin struct {
	toAdd  map[*conf][]CacheLine
	toDel  map[*conf][]string
	server gitroot.Server
	config []*conf
	commit gitroot.Commit
}

func (p *Plugin) Init(repoName string, c []map[string]string) error {
	p.toAdd = make(map[*conf][]CacheLine, 0)
	p.toDel = make(map[*conf][]string, 0)
	currentConf := make([]*conf, len(c))
	for i, cc := range c {
		format, ok := cc["format"]
		if !ok {
			format = "list"
		}
		selection, ok := cc["select"]
		if !ok {
			selection = ""
		}
		selections := make([]*regexp.Regexp, 0)
		if selection != "" {
			for _, se := range strings.Split(selection, ";") {
				if r, err := regexp.Compile(se); err != nil {
					p.server.LogError("conf regexp.Compile select="+se, err)
				} else {
					selections = append(selections, r)
				}
			}
		}
		paginator := -1
		page, ok := cc["paginator"]
		if ok {
			if pg, err := strconv.Atoi(page); err == nil {
				if pg > 0 {
					paginator = pg
				}
			} else {
				p.server.LogError("can't parse page", err)
			}
		}
		sort := "file"
		if s, ok := cc["sort"]; ok {
			sort = s
		}
		sortOrder := "asc"
		if so, ok := cc["sortOrder"]; ok {
			sortOrder = so
		}
		currentConf[i] = &conf{
			title:       cc["title"],
			description: cc["description"],
			format:      format,
			tableHeader: cc["tableheader"],
			filepath:    cc["for"],
			where:       cc["where"],
			selections:  selections,
			to:          cc["to"],
			paginator:   paginator,
			sort:        sort,
			sortOrder:   sortOrder,
		}
	}
	p.config = currentConf
	return nil
}

func (p *Plugin) StartCommit(commit gitroot.Commit) error {
	p.commit = commit
	return nil
}

func (p *Plugin) AddFile(fp string) error {
	for _, line := range p.extractCacheLines(fp) {
		p.appendInCache(line)
	}
	return nil
}

func (p *Plugin) DelFile(fp string) error {
	p.deleteInCache(fp)
	return nil
}

func (p *Plugin) ModFile(fp string) error {
	for _, line := range p.extractCacheLines(fp) {
		p.modifyInCache(fp, line)
	}
	return nil
}

func (p *Plugin) RenameFile(fromPath string, toPath string) error {
	for _, line := range p.extractCacheLines(toPath) {
		p.modifyInCache(fromPath, line)
	}
	return nil
}

func (p *Plugin) EndCommit(commit gitroot.Commit) error {
	return nil
}

func (p *Plugin) Finish() error {
	confs := make(map[string]*conf)
	for _, conf := range p.config {
		confs[conf.to] = conf
	}
	cachedFiles := make(map[string][]CacheLine)
	for _, cachedLine := range p.fromCache() {
		if _, ok := confs[cachedLine.toPath]; ok {
			if oldContent, ok := cachedFiles[cachedLine.toPath]; !ok {
				cachedFiles[cachedLine.toPath] = []CacheLine{cachedLine}
			} else {
				cachedFiles[cachedLine.toPath] = append(oldContent, cachedLine)
			}
		}
	}

	contents := make(map[string]string)
	for confTo, cachedFile := range cachedFiles {
		if conf, ok := confs[confTo]; ok {
			slices.SortFunc(cachedFile, func(a CacheLine, b CacheLine) int {
				res := 0
				if conf.sort == "title" {
					res = strings.Compare(a.title, b.title)
				} else if conf.sort == "select[0]" && len(a.selects) > 0 && len(b.selects) > 0 {
					i1, err := strconv.Atoi(a.selects[0])
					i2, err2 := strconv.Atoi(b.selects[0])
					if err != nil || err2 != nil {
						res = strings.Compare(a.selects[0], b.selects[0])
					} else {
						res = i1 - i2
					}
				} else if conf.sort == "select[1]" && len(a.selects) > 1 && len(b.selects) > 1 {
					i1, err := strconv.Atoi(a.selects[1])
					i2, err2 := strconv.Atoi(b.selects[1])
					if err != nil || err2 != nil {
						res = strings.Compare(a.selects[1], b.selects[1])
					} else {
						res = i1 - i2
					}
				} else {
					res = strings.Compare(a.fp, b.fp)
				}
				if conf.sortOrder == "desc" {
					return res * -1
				}
				return res
			})
			currentFile := confTo
			dir, file := filepath.Split(confTo)
			confToNoExt, _ := strings.CutSuffix(file, ".md")
			nbPages := int(math.Ceil(float64(len(cachedFile)) / float64(conf.paginator)))
			for nbLine, cachedLine := range cachedFile {
				newLine := conf.makeLink(p, cachedLine)
				curPage := int(math.Ceil(float64(nbLine+1) / float64(conf.paginator)))
				if conf.paginator != -1 {
					if curPage > 1 {
						currentFile = fmt.Sprintf("%s/%s_%d.md", dir, confToNoExt, curPage)
					}
				}
				if oldContent, ok := contents[currentFile]; !ok {
					contents[currentFile] = fmt.Sprintf("%s\n%s", conf.initFile(), newLine)
				} else {
					contents[currentFile] = fmt.Sprintf("%s\n%s", oldContent, newLine)
				}
				if conf.paginator != -1 && nbPages > 1 {
					nextPage := int(math.Ceil(float64(nbLine+2) / float64(conf.paginator)))
					if nextPage != curPage || nbLine+1 >= len(cachedFile) {
						buttonPages := ""
						for i := range nbPages {
							if i == 0 && curPage > 1 {
								buttonPages = fmt.Sprintf("[%d](./%s.md)\n", i+1, confToNoExt)
							} else if i == 0 {
								buttonPages = fmt.Sprintf("%d\n", i+1)
							} else if curPage != i+1 {
								buttonPages = fmt.Sprintf("%s[%d](./%s_%d.md)\n", buttonPages, i+1, confToNoExt, i+1)
							} else {
								buttonPages = fmt.Sprintf("%s%d\n", buttonPages, i+1)
							}
						}
						contents[currentFile] = fmt.Sprintf("%s\n\n%s", contents[currentFile], buttonPages)
					}
				}
			}
		}
	}
	for pathTo, content := range contents {
		p.server.ModifyContent(pathTo, content)
	}
	p.server.CommitAllIfNeeded("silo plugin")
	return nil
}

func Build(server gitroot.Server) gitroot.Plugin {
	return &Plugin{
		server: server,
	}
}

//go:wasmexport install
func main() {
	gitroot.Register(defaultConf, "**/*", []string{"main"}, Build)
}
