package main

import (
	"bytes"
	"errors"
	"fmt"
	"io/fs"
	"path/filepath"
	"slices"
	"strings"
)

type CacheLine struct {
	fp      string
	toPath  string
	title   string
	selects []string
}

func (p *Plugin) extractCacheLines(fp string) []CacheLine {
	res := []CacheLine{}
	for _, conf := range p.config {
		if ok, err := filepath.Match(conf.filepath, fp); ok {
			if err != nil {
				p.server.LogError("extractCacheLines filepath.Match "+fp, err)
				continue
			}
			content, err := fs.ReadFile(p.server.Worktree(), fp)
			if err != nil {
				p.server.Log("extractCacheLines ReadFile " + fp + " == " + err.Error())
				continue
			}
			if bytes.Contains(content, []byte(conf.where)) {
				res = append(res, p.extractCacheLine(fp, conf, content))
			}
		}
	}
	return res
}
func (p *Plugin) extractCacheLine(fp string, c *conf, content []byte) CacheLine {
	res := CacheLine{
		fp:      fp,
		toPath:  c.to,
		title:   findTitle(fp, content),
		selects: make([]string, 0),
	}
	for _, selection := range c.selections {
		t := selection.FindSubmatch(content)
		if len(t) > 1 {
			toAppend := ""
			for _, a := range t[1:] {
				if toAppend == "" {
					toAppend = string(a)
				} else {
					toAppend = fmt.Sprintf("%s %s", toAppend, a)
				}
			}
			res.selects = append(res.selects, toAppend)
		} else {
			res.selects = append(res.selects, " ")
		}
	}
	return res
}

func (c CacheLine) Marshal() string {
	return fmt.Sprintf("%s||%s||%s||%s", c.fp, c.toPath, c.title, strings.Join(c.selects, ";;"))
}

func Unmarshal(s string) CacheLine {
	data := strings.Split(s, "||")
	return CacheLine{
		fp:      data[0],
		toPath:  data[1],
		title:   data[2],
		selects: slices.DeleteFunc(strings.Split(data[3], ";;"), func(s string) bool { return s == "" }),
	}
}

func (p *Plugin) fromCache() []CacheLine {
	cache, err := fs.ReadFile(p.server.Cache(), cachePath)
	if err != nil && errors.Is(err, fs.ErrNotExist) {
		p.server.Log("cache not existing")
		return []CacheLine{}
	} else if err != nil {
		p.server.LogError("can't read cache in fromCache", err)
	}
	res := []CacheLine{}
	for _, line := range bytes.Split(cache, []byte("\n")) {
		if len(line) > 0 {
			res = append(res, Unmarshal(string(line)))
		}
	}
	return res
}

func (p *Plugin) deleteInCache(fp string) {
	cache, err := fs.ReadFile(p.server.Cache(), cachePath)
	if err != nil && errors.Is(err, fs.ErrNotExist) {
		return
	} else if err != nil {
		p.server.LogError("can't read cache", err)
	}
	var buffer bytes.Buffer
	for _, line := range strings.Split(string(cache), "\n") {
		if !strings.HasPrefix(line, fp) {
			buffer.WriteString(fmt.Sprintf("%s\n", line))
		}
	}
	p.server.ModifyCacheContent(cachePath, buffer.String())
}

func (p *Plugin) appendInCache(cacheLine CacheLine) {
	cache, err := fs.ReadFile(p.server.Cache(), cachePath)
	if err != nil && !errors.Is(err, fs.ErrNotExist) {
		p.server.LogError("can't read cache", err)
		return
	}
	marshalled := cacheLine.Marshal()
	if cache == nil {
		p.server.ModifyCacheContent(cachePath, marshalled)
	} else if !bytes.Contains(cache, []byte(marshalled)) {
		p.server.ModifyCacheContent(cachePath, fmt.Sprintf("%s\n%s", cache, marshalled))
	}

}

func (p *Plugin) modifyInCache(oldFp string, cacheLine CacheLine) {
	cache, err := fs.ReadFile(p.server.Cache(), cachePath)
	if err != nil && errors.Is(err, fs.ErrNotExist) {
		return
	} else if err != nil {
		p.server.LogError("can't read cache", err)
	}
	var buffer bytes.Buffer
	for _, line := range strings.Split(string(cache), "\n") {
		if strings.HasPrefix(line, oldFp) && strings.Contains(line, cacheLine.toPath) {
			buffer.WriteString(fmt.Sprintf("%s\n", cacheLine.Marshal()))
		} else {
			buffer.WriteString(fmt.Sprintf("%s\n", line))
		}
	}
	p.server.ModifyCacheContent(cachePath, buffer.String())
}
