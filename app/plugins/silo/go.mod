module gitroot.com/plugins/silo

go 1.22.4

replace gitroot.com/libs/goplugin => ../../libs/goplugin

require gitroot.com/libs/goplugin v0.0.0-00010101000000-000000000000
