package main

import (
	"github.com/42wim/sshsig"
	"github.com/go-git/go-git/v5/plumbing"
	"github.com/go-git/go-git/v5/plumbing/object"
)

const (
	namespace = "git"
)

func Verify(p string, c *object.Commit) error {
	encoded := &plumbing.MemoryObject{}
	c.EncodeWithoutSignature(encoded)
	r, err := encoded.Reader()
	if err != nil {
		return err
	}
	defer r.Close()
	return sshsig.Verify(r, []byte(c.PGPSignature), []byte(p), namespace)
}
