package repository

import (
	"errors"

	"github.com/go-git/go-git/v5"
	"github.com/go-git/go-git/v5/plumbing"
	"github.com/go-git/go-git/v5/plumbing/object"
	"github.com/go-git/go-git/v5/plumbing/protocol/packp"
	"github.com/go-git/go-git/v5/plumbing/storer"
	"gitroot.com/server/user"
)

var ErrCantWrite = errors.New("user can't write")

func (repo *GitRootRepository) Merge(fromBranch string, toBranch string, commiter *user.Commiter, pusher user.SimpleUser) error {
	if ok, _, err := repo.CanWrite(commiter.Signer.PublicKey(), fromBranch); err != nil || !ok {
		if !ok {
			return repo.errorHandler.With("commiter", commiter.Email).With("ssh", commiter.Signer.PublicKey()).Wrapf(ErrCantWrite, "commiter can't write")
		}
		return repo.errorHandler.Wrapf(err, "commiter can write err")
	}

	if ok, _, err := repo.CanWrite(pusher.Ssh, fromBranch); err != nil || !ok {
		if !ok {
			return repo.errorHandler.With("sshUser", pusher.Ssh).Wrapf(ErrCantWrite, "sshUser can't write")
		}
		return repo.errorHandler.Wrapf(err, "sshUser can write err")
	}

	needReturnHead := false
	headBranch, err := repo.CurrentBranch()
	if err != nil {
		return repo.errorHandler.Wrapf(err, "can't get CurrentBranch")
	}
	if headBranch.Short() != fromBranch {
		if err := repo.Checkout(plumbing.NewBranchReferenceName(fromBranch)); err != nil {
			return repo.errorHandler.With("fromBranch", fromBranch).Wrapf(err, "can't Checkout")
		}
		needReturnHead = true
	}
	headHash, err := repo.repo.Head()
	if needReturnHead && err != nil {
		if err := repo.Checkout(headBranch); err != nil {
			return repo.errorHandler.With("headBranch", headBranch).Wrapf(err, "can't Checkout headBranch")
		}
	}
	if err != nil {
		return repo.errorHandler.With("toBranch", toBranch).Wrapf(err, "can't Head")
	}
	ref, err := repo.repo.Reference(plumbing.NewBranchReferenceName(toBranch), true)
	//before error try to return to head branch
	if needReturnHead && err != nil {
		if err := repo.Checkout(headBranch); err != nil {
			return repo.errorHandler.With("headBranch", headBranch).Wrapf(err, "can't Checkout headBranch")
		}
	}
	if err != nil {
		return repo.errorHandler.With("toBranch", toBranch).Wrapf(err, "can't Reference")
	}
	err = repo.repo.Merge(*ref, git.MergeOptions{Strategy: git.FastForwardMerge})
	//before error try to return to head branch
	if needReturnHead {
		if err := repo.Checkout(headBranch); err != nil {
			return repo.errorHandler.With("headBranch", headBranch).Wrapf(err, "can't Checkout headBranch")
		}
	}
	if err != nil {
		return repo.errorHandler.With("fromBranch", fromBranch).With("toBranch", toBranch).Wrapf(err, "can't Merge")
	}

	newHeadHash, err := repo.repo.Head()
	if err != nil {
		return repo.errorHandler.Wrapf(err, "can't newHeadHash")
	}
	commit, err := repo.repo.CommitObject(newHeadHash.Hash())
	if err != nil {
		return repo.errorHandler.With("hash", newHeadHash.Hash()).Wrapf(err, "can't get commit")
	}
	newCommands := make([]*packp.Command, 0)
	commitIter := object.NewCommitPreorderIter(commit, make(map[plumbing.Hash]bool), make([]plumbing.Hash, 0))
	commitIter.ForEach(func(commit *object.Commit) error {
		if commit.Hash.String() == headHash.Hash().String() {
			return storer.ErrStop
		}
		newCommands = append(newCommands, &packp.Command{
			Name: plumbing.NewBranchReferenceName(fromBranch),
			Old:  commit.ParentHashes[0],
			New:  commit.Hash,
		})
		return nil
	})
	repo.manager.backgroundManager.PostPush(commiter.SimpleUser, repo.Name(), newCommands)

	return nil
}
