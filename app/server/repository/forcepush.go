package repository

import (
	"github.com/go-git/go-git/v5/plumbing"
	"github.com/samber/oops"
)

func (repo *GitRootRepository) IsForcePush(branch plumbing.ReferenceName, old plumbing.Hash) (bool, error) {
	commitOld, err := repo.repo.CommitObject(old)
	if err != nil {
		return false, oops.Wrapf(err, "commitOld")
	}
	ref, err := repo.repo.Reference(branch, true)
	if err != nil {
		return false, oops.Wrapf(err, "Reference")
	}
	commitBranch, err := repo.repo.CommitObject(ref.Hash())
	if err != nil {
		return false, oops.Wrapf(err, "commitBranch")
	}
	ok, err := commitOld.IsAncestor(commitBranch)
	if err != nil {
		return false, oops.Wrapf(err, "IsAncestor")
	}
	return !ok, err
}
