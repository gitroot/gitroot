package repository

import (
	"fmt"
	"strings"

	"github.com/go-git/go-git/v5/plumbing"
	"github.com/go-git/go-git/v5/plumbing/format/diff"
)

func (repo *GitRootRepository) GetDiff(hash plumbing.Hash, filepath string) (string, error) {
	com, err := repo.repo.CommitObject(hash)
	if err != nil {
		return "", repo.errorHandler.Wrapf(err, "can't find CommitObject")
	}
	parent, _ := com.Parent(0)
	diffStr := "```diff"
	patch, _ := parent.Patch(com) // TODO find a way to get diff only on one file
	for _, d := range patch.FilePatches() {
		from, to := d.Files()
		if from != nil && to != nil && from.Path() == to.Path() && from.Path() == filepath {
			for _, chunk := range d.Chunks() {
				op := "="
				if chunk.Type() == diff.Add {
					op = "+"
				} else if chunk.Type() == diff.Delete {
					op = "-"
				}
				chunksStr := strings.Split(chunk.Content(), "\n")
				for i, c := range strings.Split(chunk.Content(), "\n") {
					chunksStr[i] = fmt.Sprintf("%s %s", op, c)
				}
				diffStr = fmt.Sprintf("%s\n%s", diffStr, strings.Join(chunksStr, "\n"))
			}
			break
		}
	}
	return diffStr + "\n```\n", nil
}
