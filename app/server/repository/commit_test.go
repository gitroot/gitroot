package repository

import (
	"context"
	"fmt"
	"testing"

	"github.com/go-git/go-git/v5/plumbing"
	"gitroot.com/server/configuration"
	"gitroot.com/server/user"
)

// | commit5
// | | - commit4 -- branch2
// | | commit3
// | - commit2 -- branch1
// - commit1 -- main
func TestFoundAncestor(t *testing.T) {
	dataDir := t.TempDir()
	conf := configuration.NewConfiguration(dataDir)
	userManager, _ := user.NewManager(conf)
	m := NewManager(context.TODO(), conf, userManager)
	err := m.createUserRepo("foundAncestor", "main", []user.SimpleUser{}, []byte(""))
	if err != nil {
		t.Fatal(err)
	}
	repo, err := m.Open(context.TODO(), "foundAncestor")
	if err != nil {
		t.Fatal(err)
	}
	h1 := makeCommit(repo, 1, t, userManager)
	if err := repo.Branch("branch1", h1); err != nil {
		t.Fatal(err)
	}
	h2 := makeCommit(repo, 2, t, userManager)
	com2, _ := repo.repo.CommitObject(h2)
	comAncestor, _ := repo.foundAncestor(com2)
	if comAncestor.Hash != h1 {
		t.Fatal("should be h1")
	}
	h3 := makeCommit(repo, 3, t, userManager)
	if err := repo.Branch("branch2", h3); err != nil {
		t.Fatal(err)
	}
	h4 := makeCommit(repo, 4, t, userManager)
	com4, _ := repo.repo.CommitObject(h4)
	comAncestor4, _ := repo.foundAncestor(com4)
	if comAncestor4.Hash != h3 {
		t.Fatal("should be h3", comAncestor4.Hash, h1, h2, h3)
	}
	repo.Checkout(plumbing.NewBranchReferenceName("main"))
	makeCommit(repo, 5, t, userManager)
	comAncestor, _ = repo.foundAncestor(com2)
	if comAncestor.Hash != h1 {
		t.Fatal("should be h1")
	}
}

func makeCommit(repo *GitRootRepository, nb int, t *testing.T, u *user.Manager) plumbing.Hash {
	if err := repo.Write("README.md", []byte(fmt.Sprintf("commit%d", nb))); err != nil {
		t.Fatal(err)
	}
	h, err := repo.CommitAll(fmt.Sprintf("commit%d", nb), u.RootCommiter())
	if err != nil {
		t.Fatal(err)
	}
	return h
}
