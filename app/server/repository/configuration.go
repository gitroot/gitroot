package repository

import (
	"github.com/go-git/go-git/v5/plumbing"
	"github.com/goccy/go-yaml"
)

type repoConfiguration struct {
	DefaultBranch string
	NoForcePush   []string
}

type RepoConfiguration struct {
	DefaultBranch plumbing.ReferenceName
	NoForcePush   []plumbing.ReferenceName
}

func newRepoConfiguration(defaultBranch string) repoConfiguration {
	return repoConfiguration{
		DefaultBranch: defaultBranch,
		NoForcePush:   []string{defaultBranch},
	}
}

func (r repoConfiguration) toFileContent() []byte {
	content, _ := yaml.Marshal(r)
	return content
}

func readRepoConfiguration(fileContent []byte) (RepoConfiguration, error) {
	repoConf := repoConfiguration{}
	err := yaml.Unmarshal(fileContent, &repoConf)
	if err != nil {
		return RepoConfiguration{}, err
	}
	noForcePush := make([]plumbing.ReferenceName, len(repoConf.NoForcePush))
	for i, rc := range repoConf.NoForcePush {
		noForcePush[i] = plumbing.NewBranchReferenceName(rc)
	}
	return RepoConfiguration{
		DefaultBranch: plumbing.NewBranchReferenceName(repoConf.DefaultBranch),
		NoForcePush:   noForcePush,
	}, nil
}

func (r RepoConfiguration) IsNoPushBranch(branch plumbing.ReferenceName) bool {
	for _, b := range r.NoForcePush {
		if b == branch {
			return true
		}
	}
	return false
}
