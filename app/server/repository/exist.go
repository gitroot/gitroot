package repository

import "os"

func (m *Manager) Exists(repoName string) bool {
	_, err := os.Stat(m.conf.GetDirPathForRepo(repoName))
	return !os.IsNotExist(err)
}
