package repository

import (
	"fmt"

	"github.com/go-git/go-git/v5"
	"github.com/go-git/go-git/v5/plumbing"
	"github.com/go-git/go-git/v5/plumbing/object"
	"github.com/go-git/go-git/v5/plumbing/storer"
	"github.com/samber/oops"
	"gitroot.com/server/logger"
)

func (repo *GitRootRepository) WalkCommit(from plumbing.Hash, to plumbing.Hash, callback func(*object.Commit) error) error {
	if to == plumbing.ZeroHash { // branch deletion
		return nil
	}
	if from == plumbing.ZeroHash { // branch creation
		com, err := repo.repo.CommitObject(to)
		if err != nil {
			return oops.With("hash", to.String()).Wrapf(err, "can't get CommitObject")
		}
		com2, err := repo.foundAncestor(com)
		if err != nil {
			return oops.With("hash", to.String()).Wrapf(err, "can't get FoundAncestor")
		}
		from = com2.Hash
	}
	cIter, err := repo.repo.Log(&git.LogOptions{From: to})
	if err != nil {
		return err
	}
	display := fmt.Sprintf("----- [%s] -> [%s]\n", from.String(), to.String())
	cIter.ForEach(func(commit *object.Commit) error {
		if commit.Hash == from {
			return storer.ErrStop
		}
		display = fmt.Sprintf("%s [%s] %s : %s\n", display, commit.Hash.String(), commit.Message, commit.Author.Email)
		return nil
	})
	display = display + "-----\n"
	//os.Stdout.Write([]byte(display)) // TODO make a special logger?
	cIter.Close()
	cIter, err = repo.repo.Log(&git.LogOptions{From: to})
	if err != nil {
		return err
	}
	defer cIter.Close()
	return cIter.ForEach(func(commit *object.Commit) error {
		if commit.Hash == from {
			return storer.ErrStop
		}
		return callback(commit)
	})
}

func (repo *GitRootRepository) FoundAncestorHash(hash plumbing.Hash) (*object.Commit, error) {
	com, err := repo.repo.CommitObject(hash)
	if err != nil {
		return nil, oops.With("hash", hash.String()).Wrapf(err, "can't get CommitObject")
	}
	parent, err := repo.foundAncestor(com)
	if err != nil {
		return nil, oops.With("hash", hash.String()).Wrapf(err, "can't FoundAncestor")
	}
	return parent, nil
}

func (repo *GitRootRepository) foundAncestor(fromCommit *object.Commit) (*object.Commit, error) {
	var goodBranch *object.Commit
	var mainBranch *object.Commit
	bIter, err := repo.repo.Branches()
	if err != nil {
		return nil, repo.errorHandler.Wrapf(err, "Branches fail")
	}
	repoConfiguration, err := repo.Configuration()
	if err != nil {
		return nil, repo.errorHandler.Wrapf(err, "configuration fail")
	}
	err = bIter.ForEach(func(ref *plumbing.Reference) error {
		repo.manager.logger.Info("Branch",
			logger.NewLoggerPair("name", ref.Name().String()),
			logger.NewLoggerPair("hash", ref.Hash().String()),
			logger.NewLoggerPair("symbolic", ref.Type() == plumbing.SymbolicReference))
		branchCommit, err := repo.repo.CommitObject(ref.Hash())
		if err != nil {
			repo.manager.logger.Error("CommitObject fail", err, logger.NewLoggerPair("hash", ref.Hash().String()))
			return nil
		}
		if ref.Name() == repoConfiguration.DefaultBranch {
			mainBranch = branchCommit
		}
		if ref.Hash() == fromCommit.Hash { // eliminate current branch
			return nil
		}
		if ok, err := branchCommit.IsAncestor(fromCommit); ok {
			repo.manager.logger.Info("Ref found", logger.NewLoggerPair("hash", ref.Hash().String()))
			if goodBranch == nil {
				goodBranch = branchCommit
				return nil
			}
			if ok, err := goodBranch.IsAncestor(branchCommit); ok {
				goodBranch = branchCommit
			} else if err != nil {
				repo.manager.logger.Error("Ancestor between branches fail", err, logger.NewLoggerPair("goodBranch", goodBranch.Hash.String()), logger.NewLoggerPair("branchCommit", branchCommit.Hash.String()))
			}
		} else if err != nil {
			repo.manager.logger.Error("IsAncestor fail", err, logger.NewLoggerPair("hash", ref.Hash().String()))
			return nil
		} else {
			repo.manager.logger.Info("Not ancestor", logger.NewLoggerPair("hash", ref.Hash().String()))
		}
		return nil
	})
	if goodBranch != nil && err == nil {
		repo.manager.logger.Info("Selected Ref", logger.NewLoggerPair("for", fromCommit.Hash.String()), logger.NewLoggerPair("hash", goodBranch.Hash.String()))
		return goodBranch, nil
	} else if goodBranch == nil {
		repo.manager.logger.Info("Branch not found try with mergeBase", logger.NewLoggerPair("for", fromCommit.Hash.String()))
		if mainBranch != nil {
			try, err := mainBranch.MergeBase(fromCommit)
			if err != nil {
				return nil, oops.Wrapf(err, "can't mergeBase")
			} else if len(try) > 0 {
				repo.manager.logger.Info("Selected Ref", logger.NewLoggerPair("for", fromCommit.Hash.String()), logger.NewLoggerPair("hash", try[0].Hash.String()))
				return try[0], nil
			}
		}
		return nil, oops.With("for", fromCommit.Hash.String()).Errorf("Ancestor not found")
	}
	return nil, err
}
