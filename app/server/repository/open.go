package repository

import (
	"context"

	"github.com/go-git/go-billy/v5/memfs"
	"github.com/go-git/go-git/v5"
	"github.com/go-git/go-git/v5/plumbing/cache"
	"github.com/go-git/go-git/v5/storage"
	"github.com/go-git/go-git/v5/storage/filesystem"
	"github.com/samber/oops"
	"gitroot.com/server/fs"
	"gitroot.com/server/logger"
)

func (m *Manager) OpenForgeRepo(ctx context.Context) (*GitRootRepository, error) {
	return m.Open(ctx, m.conf.ForgeConfigName())
}

func (m *Manager) Open(ctx context.Context, repoName string) (*GitRootRepository, error) {
	m.logger.Info("Open repository", logger.NewLoggerPair("repo", repoName))
	if r, ok := m.cacheRepos[repoName]; ok {
		m.logger.Debug("Open repository from cache", logger.NewLoggerPair("repo", repoName))
		return r, nil
	}

	errBuilder := oops.With("OpenRead", repoName)

	bfs := fs.NewGitRootFs(ctx, m.conf.GetDirPathForRepo(repoName))
	repo, err := git.Open(filesystem.NewStorage(bfs, cache.NewObjectLRUDefault()), memfs.New())
	if err != nil {
		return nil, errBuilder.Wrapf(err, "can't build root fs")
	}

	worktree, err := repo.Worktree()
	if err != nil {
		return nil, oops.Wrapf(err, "can't worktree repo")
	}

	head, err := repo.Head()
	if err != nil {
		//can be not found on creation
		m.logger.Info("head not found")
	} else {
		if err := worktree.Reset(&git.ResetOptions{
			Mode:   git.HardReset,
			Commit: head.Hash(),
		}); err != nil {
			return nil, err
		}
	}

	m.cacheLock.Lock()
	defer m.cacheLock.Unlock()
	m.cacheRepos[repoName] = m.NewGitRootRepository(repoName, errBuilder, repo, worktree)
	m.logger.Debug("Open repository added in cache", logger.NewLoggerPair("repo", repoName))
	return m.cacheRepos[repoName], nil
}

func (m *Manager) ForReceivePack(storer storage.Storer, repoName string) (*GitRootRepository, *GitRootRepository, error) {
	m.logger.Info("Open repository FromStorer", logger.NewLoggerPair("repo", repoName))

	errBuilder := oops.Code("FromStorer")

	repo, err := git.Open(storer, memfs.New())
	if err != nil {
		return nil, nil, oops.Wrapf(err, "can't open git repo")
	}

	worktree, err := repo.Worktree()
	if err != nil {
		return nil, nil, oops.Wrapf(err, "can't worktree repo")
	}

	head, err := repo.Head()
	if err != nil {
		return nil, nil, err
	}

	if err := worktree.Reset(&git.ResetOptions{
		Mode:   git.HardReset,
		Commit: head.Hash(),
	}); err != nil {
		return nil, nil, err
	}

	m.cacheLock.Lock()
	if r, ok := m.cacheRepos[repoName]; ok {
		m.logger.Debug("Open repository FromStorer from cache", logger.NewLoggerPair("repo", repoName))
		m.cacheLock.Unlock()
		return r, m.NewGitRootRepository(repoName, errBuilder, repo, worktree), nil
	}
	m.cacheLock.Unlock()
	r, err := m.Open(m.ctx, repoName) //will add it in cache
	return r, m.cacheRepos[repoName], err
}
