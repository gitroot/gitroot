# GitRoot

Welcome in your new git repository.

## First configure your repo

GitRoot force to use signed commit. Configure your git client for this repository by executing:

```sh
.gitroot/init.sh
```

## Add collaborators

In any GitRoot repository you will find a `.gitroot/users.yml` file. To add a user just append its informations in the right group.

```yml
group:
  branches:
    - name: main
  users:
    - pseudo: PSEUDO
      avatar: ''
      emails:
        - EMAIL
      ssh:
        - SSH
```

`group` is the group name where user will reside. You can choose any name you want, gitroot don't use it. Group is just a convenience way to group users.

Each group manage `branches`. Every branch name in this group means only user of this group can write to them. All others branches will be "open" and anyone can write to them.

Each group have `users`. A user is at least a ssh key. All others informations are here for convience and not used by gitroot (today).

## Active plugins

In any GitRoot repository you will find a `.gitroot/plugins.yml` file. It contain all plugins available in your instance. To active one, just change the boolean `active` to `true`, commit and push.
