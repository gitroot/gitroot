#!/usr/bin/env bash

RED='\033[0;31m'
GREEN='\033[0;32m'
PURPLE='\033[0;35m'
NC='\033[0m'

POSITIONAL_ARGS=()

while [[ $# -gt 0 ]]; do
  case $1 in
    --pubKey)
      PUB_KEY="$2"
      shift # past argument
      shift # past value
      ;;
    --privKey)
      PRIV_KEY="$2"
      shift # past argument
      shift # past value
      ;;
    --email)
      EMAIL="$2"
      shift # past argument
      shift # past value
      ;;
    --name)
      NAME="$2"
      shift # past argument
      shift # past value
      ;;
    -*|--*)
      echo "Unknown option $1"
      exit 1
      ;;
    *)
      POSITIONAL_ARGS+=("$1") # save positional arg
      shift # past argument
      ;;
  esac
done

set -- "${POSITIONAL_ARGS[@]}" # restore positional parameters

if [ -z "$PUB_KEY" ]; then
    echo "Where is your ssh public key?"
    read -p '(~/.ssh/id_rsa.pub): ' SSH_PUB_PATH

    SSH_PUB_PATH=${SSH_PUB_PATH:-"~/.ssh/id_rsa.pub"}
    SSH_PUB_PATH=${SSH_PUB_PATH/#~\//$HOME/}

    if [ -f "$SSH_PUB_PATH" ]; then
        echo -e "${GREEN}Use ssh key ${SSH_PUB_PATH}${NC}"
        PUB_KEY=${SSH_PUB_PATH}
    else
        echo -e "${RED}Ssh key ${SSH_PUB_PATH} not found${NC}"
        exit 1
    fi
fi

if [ -z "$PRIV_KEY" ]; then
    echo "Where is your ssh private key?"
    read -p '(~/.ssh/id_rsa): ' SSH_PRIV_PATH

    SSH_PRIV_PATH=${SSH_PRIV_PATH:-"~/.ssh/id_rsa"}
    SSH_PRIV_PATH=${SSH_PRIV_PATH/#~\//$HOME/}

    if [ -f "$SSH_PRIV_PATH" ]; then
        echo -e "${GREEN}Use ssh key ${SSH_PRIV_PATH}${NC}"
        PRIV_KEY=${SSH_PRIV_PATH}
    else
        echo -e "${RED}Ssh key ${SSH_PRIV_PATH} not found${NC}"
        exit 1
    fi
fi

git config commit.gpgSign true
git config gpg.format ssh
git config user.signingkey $PUB_KEY
git config gpg.ssh.allowedSignersFile "$(pwd)/.gitroot/allowed_signers"
git config core.sshCommand "ssh -i ${PRIV_KEY} -o IdentitiesOnly=yes"

echo -e "${GREEN}Local git repository configured${NC}"

## Email

if [ -z "$EMAIL" ]; then
    EMAIL=$(git config --get user.email)
    echo "What email do you want to use for this repo?"
    read -p "($EMAIL): " EMAIL

    EMAIL=${EMAIL:-"$(git config --get user.email)"}
fi

echo "$EMAIL $(cat $PUB_KEY)" >> "$(pwd)/.gitroot/allowed_signers"
sed -i -e "s/- \"\"/- $EMAIL/g" .gitroot/users.yml
git config user.email "$EMAIL"

## Name

if [ -z "$NAME" ]; then
    NAME=$(git config --get user.name)
    echo "What name do you want to use for this repo?"
    read -p "($NAME): " NAME

    NAME=${NAME:-"$(git config --get user.name)"}
fi

sed -i -e "s/- pseudo: \"\"/- pseudo: \"$NAME\"/g" .gitroot/users.yml
git config user.name "$NAME"

echo -e "${GREEN}GitRoot configuration files updated.${NC}"

echo -e "${PURPLE}Configuration finish. Please commit and push modified files.${NC}"