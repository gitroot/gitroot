# GitRoot

Welcome in your new git forge. This repository contains all configurations for your forge.

## First configure your repo

GitRoot force to use signed commit. Configure your git client for this repository by executing:

```sh
.gitroot/init.sh
```

### Manually

If you prefer to not user the helper script this is what you need to do:

```sh
git config commit.gpgSign true
git config gpg.format ssh
export SSH_PUB_PATH=~/.ssh/ssh.pub #don't forget to put good path
git config user.signingkey $SSH_PUB_PATH
echo "$(git config --get user.email) $(cat $SSH_PUB_PATH)" >> "$(pwd)/.gitroot/allowed_signers"
git config gpg.ssh.allowedSignersFile "$(pwd)/.gitroot/allowed_signers"
sed -i -e 's/- ""/- "$(git config --get user.email)"/g' .gitroot/users.yml
```

If you are in "owner" section of `.gitroot/users.yml`:

```sh
git commit -am "configuration done"
git push origin main
```

Else:

```sh
git checkout -b configuration
git commit -am "configuration done"
git push origin configuration
```

And request a owner to merge your branch.

## Create your first repo

Now all is configured you can create your first repo.

```sh
echo "firstRepo:\n  defaultbranch: main\n" >> .gitroot/repositories.yml
```

If you are in "owner" section of `.gitroot/users.yml`:

```sh
git commit -am "add repo"
git push origin main
```

Else:

```sh
git checkout -b addRepo
git commit -am "add repo"
git push origin addRepo
```

And request a owner to merge your branch.

> All done! You can clone your repo

```sh
export PSEUDO=myFunPseudo #don't forget to change
git clone ssh://$PSEUDO@127.0.0.1:4545/firstRepo
```

Don't forget to configure this new repository. Restart commands from [above](#first-configure-your-repo)

## Add collaborators

In any GitRoot repository you will find a `.gitroot/users.yml` file. To add a user just append its informations in the right group.

```yml
group:
  branches:
    - name: main
  users:
    - pseudo: PSEUDO
      avatar: ''
      emails:
        - EMAIL
      ssh:
        - SSH
```

`group` is the group name where user will reside. You can choose any name you want, gitroot don't use it. Group is just a convenience way to group users.

Each group manage `branches`. Every branch name in this group means only user of this group can write to them. All others branches will be "open" and anyone can write to them.

Each group have `users`. A user is at least a ssh key. All others informations are here for convience and not used by gitroot (today).
