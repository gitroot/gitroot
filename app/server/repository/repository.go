package repository

import (
	"context"
	"errors"
	"io/fs"
	"os"
	"path/filepath"
	"sync"
	"sync/atomic"
	"time"

	"github.com/go-git/go-git/v5"
	"github.com/go-git/go-git/v5/plumbing"
	"github.com/go-git/go-git/v5/plumbing/object"
	"github.com/samber/oops"
	grfs "gitroot.com/server/fs"
	"gitroot.com/server/logger"
	"gitroot.com/server/user"
)

type GitRootRepository struct {
	name             string
	logger           *logger.Logger
	errorHandler     oops.OopsErrorBuilder
	repo             *git.Repository
	worktree         *git.Worktree
	manager          *Manager
	lock             *sync.RWMutex
	nbLockInProgress *atomic.Int32
	repoLockKind     int //0 nolock - 1 read - 2 write
}

func (m *Manager) NewGitRootRepository(name string, errorHandler oops.OopsErrorBuilder, repo *git.Repository, worktree *git.Worktree) *GitRootRepository {
	return &GitRootRepository{
		name:             name,
		logger:           m.logger.NewSubLogger(name),
		errorHandler:     errorHandler,
		repo:             repo,
		worktree:         worktree,
		manager:          m,
		lock:             &sync.RWMutex{},
		nbLockInProgress: &atomic.Int32{},
		repoLockKind:     0,
	}
}

func (repo *GitRootRepository) Name() string {
	return repo.name
}

func (repo *GitRootRepository) RLock() {
	repo.logger.Debug("rlock")
	repo.lock.RLock()
	repo.nbLockInProgress.Add(1)
	repo.repoLockKind = 1
}

func (repo *GitRootRepository) RUnlock() {
	repo.logger.Debug("runlock")
	repo.lock.RUnlock()
	repo.nbLockInProgress.Add(-1)
	if repo.nbLockInProgress.Load() == 0 {
		repo.repoLockKind = 0
	}
}

func (repo *GitRootRepository) WLock() {
	repo.logger.Debug("wlock")
	repo.lock.Lock()
	repo.repoLockKind = 2
}

func (repo *GitRootRepository) WUnlock() {
	repo.logger.Debug("wunlock")
	repo.lock.Unlock()
	repo.repoLockKind = 0
}

func (repo *GitRootRepository) Configuration() (RepoConfiguration, error) {
	content, err := repo.Content(repo.manager.conf.PathFileRepoConfigurationName())
	if err != nil {
		return RepoConfiguration{}, repo.errorHandler.Wrapf(err, "can't read conf file")
	}
	return readRepoConfiguration(content)
}

func (repo *GitRootRepository) ReloadWorktree() error {
	head, err := repo.repo.Head()
	if err != nil {
		return err
	}

	if err := repo.worktree.Reset(&git.ResetOptions{
		Mode:   git.HardReset,
		Commit: head.Hash(),
	}); err != nil {
		return err
	}

	return nil
}

func (repo *GitRootRepository) ToFs(ctx context.Context) fs.FS {
	return grfs.ToFs(ctx, repo.worktree.Filesystem)
}

func (repo *GitRootRepository) ContentRepositoriesConf() ([]byte, error) {
	return repo.Content(repo.manager.conf.PathFileRepositories())
}

func (repo *GitRootRepository) ContentPluginsConf() ([]byte, error) {
	return repo.Content(repo.manager.conf.PathFilePlugins())
}

func (repo *GitRootRepository) ContentPluginsConfAtHash(hash plumbing.Hash) ([]byte, error) {
	return repo.ContentAtHash(repo.manager.conf.PathFilePlugins(), hash)
}

func (repo *GitRootRepository) ContentPluginsConfAtRef(name plumbing.ReferenceName) ([]byte, error) {
	ref, err := repo.repo.Reference(name, false)
	if err != nil {
		return nil, oops.With("ref", name).Wrapf(err, "ContentPluginsConfAtRef")
	}
	return repo.ContentPluginsConfAtHash(ref.Hash())
}

func (repo *GitRootRepository) PathDataWeb(path string) string {
	return filepath.Join(repo.manager.conf.PathDataWeb(repo.name), path)
}

func (repo *GitRootRepository) Content(filepath string) ([]byte, error) {
	head, err := repo.repo.Head()
	if err != nil {
		return nil, oops.Wrapf(err, "Head")
	}
	return repo.ContentAtHash(filepath, head.Hash())
}

func (repo *GitRootRepository) ContentAtHash(filepath string, hash plumbing.Hash) ([]byte, error) {
	c, err := repo.repo.CommitObject(hash)
	if err != nil {
		return nil, err
	}
	tree, err := c.Tree()
	if err != nil {
		return nil, err
	}
	file, err := tree.File(filepath)
	if err != nil {
		return nil, err
	}
	fileContent, err := file.Contents()
	if err != nil {
		return nil, repo.errorHandler.With("filepath", filepath).Wrapf(err, "can't read file")
	}
	return []byte(fileContent), nil
}

func (repo *GitRootRepository) Write(filepath string, filecontent []byte) error {
	f, err := repo.worktree.Filesystem.OpenFile(filepath, os.O_CREATE|os.O_RDWR|os.O_TRUNC, 0666)
	if err != nil {
		return repo.errorHandler.With("filepath", filepath).Wrapf(err, "can't open file")
	}
	_, err = f.Write(filecontent)
	if err != nil {
		return repo.errorHandler.With("filepath", filepath).Wrapf(err, "can't write file")
	}
	if err := f.Close(); err != nil {
		return repo.errorHandler.With("filepath", filepath).Wrapf(err, "can't close file")
	}
	return nil
}

func (repo *GitRootRepository) WriteExec(filepath string, filecontent []byte) error {
	f, err := repo.worktree.Filesystem.OpenFile(filepath, os.O_CREATE|os.O_RDWR|os.O_TRUNC, 0755)
	if err != nil {
		return repo.errorHandler.With("filepath", filepath).Wrapf(err, "can't open file")
	}
	_, err = f.Write(filecontent)
	if err != nil {
		return repo.errorHandler.With("filepath", filepath).Wrapf(err, "can't write file")
	}
	if err := f.Close(); err != nil {
		return repo.errorHandler.With("filepath", filepath).Wrapf(err, "can't close file")
	}
	return nil
}

func (repo *GitRootRepository) CommitAll(message string, commiter *user.Commiter) (plumbing.Hash, error) {
	err := repo.worktree.AddWithOptions(&git.AddOptions{All: true})
	if err != nil {
		return plumbing.ZeroHash, oops.Wrapf(err, "Add .")
	}

	h, err := repo.worktree.Commit(message, &git.CommitOptions{
		Author: &object.Signature{Name: commiter.Pseudo, Email: commiter.Email, When: time.Now()},
		Signer: commiter.Signer,
	})
	if err != nil && !errors.Is(err, git.ErrEmptyCommit) {
		return plumbing.ZeroHash, oops.Wrapf(err, "Commit")
	}

	return h, nil
}

func (repo *GitRootRepository) CurrentBranch() (plumbing.ReferenceName, error) {
	head, err := repo.repo.Head()
	if err != nil {
		return "", oops.Wrapf(err, "Head")
	}
	return head.Name(), nil
}

func (repo *GitRootRepository) Checkout(branch plumbing.ReferenceName) error {
	if err := repo.worktree.Checkout(&git.CheckoutOptions{Branch: branch}); err != nil {
		status, err2 := repo.worktree.Status()
		if err2 != nil {
			return err
		}
		repo.manager.logger.Warn("worktree", logger.NewLoggerPair("status", status.String()))
		return err
	}
	return nil
}

func (repo *GitRootRepository) Branch(branch string, fromHash plumbing.Hash) error {
	return repo.worktree.Checkout(&git.CheckoutOptions{Branch: plumbing.NewBranchReferenceName(branch), Hash: fromHash, Create: true})
}

func (repo *GitRootRepository) Close() error {
	// if repo.canRepack { // TODO repack & prune
	// if err := repo.repo.Prune(git.PruneOptions{}); err != nil {
	// 	return oops.Wrapf(err, "Prune")
	// }
	// if err := repo.repo.RepackObjects(&git.RepackConfig{UseRefDeltas: true}); err != nil {
	// 	return oops.Wrapf(err, "RepackObjects")
	// }
	// }
	return nil
}
