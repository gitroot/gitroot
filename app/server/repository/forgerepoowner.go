package repository

import (
	"context"
	"errors"
	"io/fs"

	"github.com/samber/oops"
	"gitroot.com/server/user"
)

func (m *Manager) ForgeRepoNeedOwner(ctx context.Context, u user.SimpleUser) error {
	errBuilder := oops.Code("NeedOwner").With("FirstPull add owner", u.Pseudo)

	repo, err := m.OpenForgeRepo(ctx)
	defer repo.Close()
	if err != nil {
		return errBuilder.Wrapf(err, "OpenFile")
	}

	repo.RLock()
	_, err = repo.worktree.Filesystem.Stat("first_pull")
	if err != nil && errors.Is(err, fs.ErrNotExist) {
		repo.RUnlock()
		return nil
	} else if err != nil {
		repo.RUnlock()
		return errBuilder.Wrapf(err, "first_pull")
	}
	repo.RUnlock()

	repo.WLock()
	defer repo.WUnlock()

	fileUsersContent, err := repo.AppendUserToGroup("owner", u)
	if err != nil {
		return errBuilder.Wrapf(err, "AppendUser")
	}

	if err := repo.Write(m.conf.PathFileUsers(), fileUsersContent); err != nil {
		return errBuilder.Wrapf(err, "WriteUser")
	}

	_, err = repo.worktree.Remove("first_pull")
	if err != nil {
		return errBuilder.Wrapf(err, "remove first_pull")
	}

	if _, err := repo.CommitAll("init", m.userManager.RootCommiter()); err != nil {
		return errBuilder.Wrapf(err, "CommitAll")
	}

	return nil
}
