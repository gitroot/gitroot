package repository

import (
	"fmt"

	"github.com/go-git/go-git/v5/plumbing"
	"github.com/samber/oops"
	"gitroot.com/server/user"
)

func (repo *GitRootRepository) CanWrite(userSsh string, branch string) (bool, *user.GitRootUser, error) {
	var users []byte
	ref, err := repo.repo.Storer.Reference(plumbing.NewBranchReferenceName(branch))
	if ref != nil && err == nil {
		users, err = repo.contentUsersConfAtHash(ref.Hash())
		if err != nil {
			return false, nil, repo.errorHandler.With("branch", branch).Wrapf(err, "contentUsersConfAtHash")
		}
	} else if ref == nil { //look at main branch
		users, err = repo.contentUsersConf()
		if err != nil {
			return false, nil, repo.errorHandler.Wrapf(err, "contentUsersConf")
		}
	} else {
		return false, nil, repo.errorHandler.Wrapf(err, "Reference")
	}
	currentUser, err := user.FindUser(users, userSsh)
	if err != nil {
		return false, nil, repo.errorHandler.With("ssh", userSsh).Wrapf(err, "FindUser")
	}
	return currentUser.CanWrite(branch), currentUser, nil
}

func (repo *GitRootRepository) AddUserInfo(u *user.GitRootUser, branch string) error {
	currentBranch, err := repo.CurrentBranch()
	if err != nil {
		return oops.Wrapf(err, "can't get currentBranch")
	}
	if err := repo.Checkout(plumbing.NewBranchReferenceName(branch)); err != nil {
		return oops.With("branch", branch).Wrapf(err, "can't checkout")
	}
	fileUsersContent, err := repo.contentUsersConf()
	if err != nil {
		return oops.Wrapf(err, "can't readAll file users")
	}
	newFileUsersContent, err := user.AppendBranch(fileUsersContent, branch, user.SimpleUser{Pseudo: u.Pseudo, Email: u.Email, Ssh: u.PubKey})
	if err != nil {
		return oops.Wrapf(err, "can't AppendUser")
	}

	if err := repo.Write(repo.manager.conf.PathFileUsers(), newFileUsersContent); err != nil {
		return oops.Wrapf(err, "Write")
	}

	if _, err := repo.CommitAll(fmt.Sprintf("Add user for branch %s", branch), repo.manager.userManager.RootCommiter()); err != nil {
		return oops.Wrapf(err, "CommitAll")
	}
	return repo.Checkout(currentBranch)
}

func (repo *GitRootRepository) AppendUserToGroup(groupName string, users ...user.SimpleUser) ([]byte, error) {
	fileUsersContent, err := repo.contentUsersConf()
	if err != nil {
		return nil, oops.Wrapf(err, "can't read users file")
	}
	return user.AppendUserToGroup(fileUsersContent, groupName, users...)
}

func (repo *GitRootRepository) contentUsersConf() ([]byte, error) {
	return repo.Content(repo.manager.conf.PathFileUsers())
}

func (repo *GitRootRepository) contentUsersConfAtHash(hash plumbing.Hash) ([]byte, error) {
	return repo.ContentAtHash(repo.manager.conf.PathFileUsers(), hash)
}

func (repo *GitRootRepository) contentUsersConfAtRef(name plumbing.ReferenceName) ([]byte, error) {
	ref, err := repo.repo.Reference(name, false)
	if err != nil {
		return nil, oops.With("ref", name).Wrapf(err, "ContentUsersConfAtRef")
	}
	return repo.ContentAtHash(repo.manager.conf.PathFileUsers(), ref.Hash())
}
