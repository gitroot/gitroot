package repository

import (
	"context"
	"os"

	"github.com/goccy/go-yaml"
	"github.com/samber/oops"
	"gitroot.com/server/logger"
	"gitroot.com/server/user"
)

type RepoConf struct {
	Name          string `yaml:"-"`
	DefaultBranch string
	Owners        []string
}

func fromFileContent(fileContent []byte) ([]RepoConf, error) {
	repositories := make([]RepoConf, 0)
	repositoriesByName := make(map[string]RepoConf)
	if err := yaml.UnmarshalWithOptions(fileContent, &repositoriesByName, yaml.UseOrderedMap()); err != nil {
		return nil, oops.Wrapf(err, "can't parse yaml users")
	}
	for name, repo := range repositoriesByName {
		repo.Name = name
		repositories = append(repositories, repo)
	}
	return repositories, nil
}

func toFileContent(repositories []RepoConf) ([]byte, error) {
	u := make(map[string]RepoConf)
	for _, j := range repositories {
		u[j.Name] = j
	}
	content, err := yaml.Marshal(u)
	return content, oops.Wrapf(err, "can't marshal yaml repositories")
}

func (m *Manager) ForgeRepoMakeDiffRepos(ctx context.Context, repositories []byte, users []user.SimpleUser, plugins []byte) error {
	entries, err := os.ReadDir(m.conf.PathRepositories())
	if err != nil {
		return oops.With("dataDir", m.conf.PathRepositories()).Wrapf(err, "can't read dir data")
	}

	reposInFile, err := fromFileContent(repositories)
	if err != nil {
		oops.Wrapf(err, "can't decode file repositories")
	}

	m.logger.Debug("reposDiff", logger.NewLoggerPair("repositories", string(repositories)))

	for _, repo := range reposInFile {
		found := false
		for _, e := range entries {
			if e.Name() == repo.Name {
				found = true
				break
			}
		}
		if !found {
			owners := make([]user.SimpleUser, len(repo.Owners))
			for i, u := range repo.Owners {
				owners[i] = user.SimpleUser{
					Pseudo: "",
					Email:  "",
					Ssh:    u,
				}
			}
			if err := m.createUserRepo(repo.Name, repo.DefaultBranch, append(users, owners...), plugins); err != nil {
				return oops.With("name", repo.Name).Wrapf(err, "can't createInitializedRepo")
			}
		}
	}

	for _, e := range entries {
		found := false
		for _, repo := range reposInFile {
			if e.Name() == repo.Name {
				found = true
				break
			}
		}
		if !found {
			if err := m.delete(e.Name()); err != nil {
				return oops.With("repo name", e.Name()).Wrapf(err, "can't delete")
			}
		}
	}

	return nil
}
