package repository

import (
	"bytes"
	_ "embed"

	"github.com/go-git/go-git/v5"
	branch "github.com/go-git/go-git/v5/config"
	"github.com/go-git/go-git/v5/plumbing"
	"github.com/go-git/go-git/v5/plumbing/format/config"
	"github.com/samber/oops"
	"gitroot.com/server/logger"
	"gitroot.com/server/user"
)

//go:embed resources/standard_README.md
var standard_README []byte

//go:embed resources/forgerepo_README.md
var forgerepo_README []byte

//go:embed resources/init.sh
var forgerepo_initSh []byte

func (m *Manager) CreateRootRepoIfNeeded() error {
	if !m.Exists(m.conf.ForgeConfigName()) {
		return m.createRootRepo()
	}
	return nil
}

func (m *Manager) create(repoName string, defaultBranchName string) error {
	repoDir := m.conf.GetDirPathForRepo(repoName)

	repo, err := git.PlainInitWithOptions(repoDir, &git.PlainInitOptions{
		InitOptions:  git.InitOptions{DefaultBranch: plumbing.NewBranchReferenceName(defaultBranchName)},
		Bare:         true,
		ObjectFormat: config.SHA1, // TODO debug sha256
	})
	if err != nil {
		return oops.Wrapf(err, "PlainInitWithOptions")
	}

	if err := repo.CreateBranch(&branch.Branch{
		Name:  defaultBranchName,
		Merge: plumbing.NewBranchReferenceName(defaultBranchName),
	}); err != nil {
		return oops.Wrapf(err, "CreateBranch")
	}

	return nil
}

type extraFiles struct {
	path    string
	content []byte
}

func (m *Manager) createInitializedRepo(repoName string, defaultBranchName string, extraFiles []extraFiles, users []user.SimpleUser, availablePlugins []byte) error {
	m.logger.Info("create repo", logger.NewLoggerPair("repo", repoName))
	err := m.create(repoName, defaultBranchName)
	if err != nil {
		return oops.Wrapf(err, "Create repo")
	}
	repo, err := m.Open(m.ctx, repoName)
	repo.WLock()
	defer repo.WUnlock()
	defer repo.Close()
	if err != nil {
		return oops.Wrapf(err, "Open repo")
	}

	for _, extraFile := range extraFiles {
		if err := repo.Write(extraFile.path, extraFile.content); err != nil {
			return err
		}
	}

	if err := repo.WriteExec(m.conf.ForPathConfig("init.sh"), forgerepo_initSh); err != nil {
		return err
	}

	sshString := bytes.NewBuffer([]byte(""))
	sshString.WriteString(m.userManager.RootCommiter().Email)
	sshString.WriteString(" ")
	sshString.WriteString(m.userManager.RootCommiter().Signer.PublicKey())
	sshString.WriteString("\n")
	for _, u := range users {
		sshString.WriteString(u.Email)
		sshString.WriteString(" ")
		sshString.WriteString(u.Ssh)
		sshString.WriteString("\n")
	}

	if err := repo.Write(m.conf.ForPathConfig("allowed_signers"), sshString.Bytes()); err != nil {
		return err
	}

	if err := repo.Write(m.conf.PathFilePlugins(), availablePlugins); err != nil {
		return err
	}

	if err := repo.Write(m.conf.PathFileRepoConfigurationName(), newRepoConfiguration(defaultBranchName).toFileContent()); err != nil {
		return err
	}

	fileUserContent, err := user.CreateFileUser(
		defaultBranchName,
		append([]user.SimpleUser{m.userManager.RootCommiter().SimpleUser}, users...)...,
	)
	if err != nil {
		return oops.Wrapf(err, "CreateFileUser")
	}
	if err := repo.Write(m.conf.PathFileUsers(), fileUserContent); err != nil {
		return err
	}

	if _, err := repo.CommitAll("init", m.userManager.RootCommiter()); err != nil {
		return oops.Wrapf(err, "CommitAll")
	}

	return nil
}

func (m *Manager) createRootRepo() error {
	repositoriesContent, err := toFileContent([]RepoConf{{Name: m.conf.ForgeConfigName(), DefaultBranch: m.conf.DefaultBranchName()}})
	if err != nil {
		return oops.Wrapf(err, "toFileContent")
	}
	extraFiles := []extraFiles{
		{path: "README.md", content: forgerepo_README},
		{path: m.conf.PathFileRepositories(), content: repositoriesContent},
		{path: "first_pull", content: []byte("")},
	}
	return m.createInitializedRepo(m.conf.ForgeConfigName(), m.conf.DefaultBranchName(), extraFiles, []user.SimpleUser{}, []byte(""))
}

func (m *Manager) createUserRepo(repoName string, defaultBranchName string, users []user.SimpleUser, availablePlugins []byte) error {
	return m.createInitializedRepo(repoName, defaultBranchName, []extraFiles{{path: "README.md", content: standard_README}}, users, availablePlugins)
}
