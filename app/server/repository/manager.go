package repository

import (
	"context"
	"os"
	"sync"

	"github.com/go-git/go-git/v5/plumbing/protocol/packp"
	"gitroot.com/server/logger"
	"gitroot.com/server/user"
)

type Manager struct {
	ctx               context.Context
	logger            *logger.Logger
	conf              needConf
	userManager       needUser
	backgroundManager needBackground
	cacheLock         *sync.Mutex
	cacheRepos        map[string]*GitRootRepository
}

type needConf interface {
	GetDirPathForRepo(repoName string) string
	PathDataWeb(repoName string) string
	ForPathConfig(filename string) string
	PathFileUsers() string
	ForgeConfigName() string
	DefaultBranchName() string
	PathFileRepositories() string
	PathFilePlugins() string
	PathFileRepoConfigurationName() string
	PathRepositories() string
}

type needUser interface {
	RootCommiter() *user.Commiter
}

type needBackground interface {
	PostPush(pusher user.SimpleUser, repoName string, commands []*packp.Command)
}

func NewManager(ctx context.Context, conf needConf, userManager needUser) *Manager {
	return &Manager{
		ctx:         ctx,
		logger:      logger.NewLogger(logger.REPOSITORY_MANAGER),
		conf:        conf,
		userManager: userManager,
		cacheLock:   &sync.Mutex{},
		cacheRepos:  make(map[string]*GitRootRepository),
	}
}

func (m *Manager) SetBackgroundManager(backgroundManager needBackground) {
	m.backgroundManager = backgroundManager
}

func (m *Manager) delete(repoName string) error {
	m.cacheLock.Lock()
	defer m.cacheLock.Unlock()
	delete(m.cacheRepos, repoName)
	path := m.conf.GetDirPathForRepo(repoName)
	return os.Remove(path)
}
