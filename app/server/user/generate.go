package user

import (
	"bytes"
	"crypto/ed25519"
	"crypto/rand"
	"encoding/pem"
	"errors"
	"fmt"
	"io"
	"os"

	"github.com/42wim/sshsig"
	"github.com/samber/oops"
	"golang.org/x/crypto/ssh"
)

type Signer struct {
	private ed25519.PrivateKey
	signer  ssh.Signer
	public  string
}

func generateIfNotExist(path, name string) (*Signer, error) {
	privateSshKeyPath := fmt.Sprintf("%s/%s.priv", path, name)
	publicSshKeyPath := fmt.Sprintf("%s/%s.pub", path, name)
	errorHandler := oops.With("privateSshKeyPath", privateSshKeyPath)

	err := os.MkdirAll(path, os.ModePerm)
	if err != nil {
		return nil, errorHandler.Wrapf(err, "can't MkdirAll")
	}

	var privateKey ed25519.PrivateKey
	if _, err := os.Stat(privateSshKeyPath); err == nil {
		content, err := os.ReadFile(privateSshKeyPath)
		if err != nil {
			return nil, errorHandler.Wrapf(err, "can't read private key")
		}
		privateKeyRaw, err := ssh.ParseRawPrivateKey(content)
		if err != nil {
			return nil, errorHandler.Wrapf(err, "can't parse private key")
		}
		pk, ok := privateKeyRaw.(*ed25519.PrivateKey)
		if !ok {
			return nil, errorHandler.Wrapf(err, "private key is not ed25519")
		}
		privateKey = *pk
	} else if errors.Is(err, os.ErrNotExist) {
		_, privateKey, err = ed25519.GenerateKey(rand.Reader)
		if err != nil {
			return nil, errorHandler.Wrapf(err, "can't generate private key")
		}

		p, err := ssh.MarshalPrivateKey(privateKey, "GitRoot master ssh key")
		if err != nil {
			return nil, errorHandler.Wrapf(err, "can't marshal private key")
		}
		fpriv, err := os.Create(privateSshKeyPath)
		if err != nil {
			return nil, errorHandler.Wrapf(err, "can't create file private key")
		}
		err = pem.Encode(fpriv, p)
		if err != nil {
			return nil, errorHandler.Wrapf(err, "can't encode pem private key")
		}

		sshSigner, err := ssh.NewSignerFromSigner(privateKey)
		if err != nil {
			return nil, errorHandler.Wrapf(err, "can't create signer from private key")
		}

		sshString := bytes.TrimSuffix(ssh.MarshalAuthorizedKey(sshSigner.PublicKey()), []byte("\n"))
		fpub, err := os.Create(publicSshKeyPath)
		if err != nil {
			return nil, errorHandler.Wrapf(err, "can't create file public key")
		}
		_, err = fpub.Write(sshString)
		if err != nil {
			return nil, errorHandler.Wrapf(err, "can't write file public key")
		}
		err = fpub.Close()
		if err != nil {
			return nil, errorHandler.Wrapf(err, "can't close file public key")
		}
	} else {
		return nil, errorHandler.Wrapf(err, "can't stat file")
	}
	sshSigner, _ := ssh.NewSignerFromSigner(privateKey)
	return &Signer{signer: sshSigner, private: privateKey, public: string(bytes.TrimSuffix(ssh.MarshalAuthorizedKey(sshSigner.PublicKey()), []byte("\n")))}, nil
}

func (s *Signer) PublicKey() string {
	return s.public
}

func (s *Signer) Signer() ssh.Signer {
	return s.signer
}

func (s *Signer) Sign(message io.Reader) ([]byte, error) {
	p, err := ssh.MarshalPrivateKey(s.private, "GitRoot master ssh key")
	if err != nil {
		return nil, oops.Wrapf(err, "can't marshal private key")
	}
	return sshsig.Sign(pem.EncodeToMemory(p), message, "git")
}
