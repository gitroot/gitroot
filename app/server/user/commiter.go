package user

type Commiter struct {
	SimpleUser
	Signer *Signer
}

func (m *Manager) NewCommiter(pseudo string) (*Commiter, error) {
	return newCommiter(pseudo, m.conf)
}

func newCommiter(pseudo string, conf needConf) (*Commiter, error) {
	path := conf.GetDirPathDataPlugin(pseudo)
	if conf.IsRootCommiter(pseudo) {
		path = conf.GetDirPathData()
	}
	email := conf.GetEmail(pseudo)
	signer, err := generateIfNotExist(path, pseudo)
	if err != nil {
		return nil, err
	}
	return &Commiter{
		SimpleUser: SimpleUser{Pseudo: pseudo, Email: email, Ssh: signer.PublicKey()},
		Signer:     signer,
	}, nil
}
