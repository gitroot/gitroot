package user

import (
	"bytes"
	"fmt"

	"github.com/goccy/go-yaml"
	"github.com/goccy/go-yaml/parser"
	"github.com/samber/oops"
)

type GitRootGroup struct {
	GroupName string
	Branches  []string
}

type GitRootUser struct {
	Group     *GitRootGroup
	Email     string
	Pseudo    string
	AllGroups []*GitRootGroup
	PubKey    string
}

type Group struct {
	Name     string `yaml:"-"`
	Branches []Branch
	Users    []User
}

type Branch struct {
	Name string
	Not  []string `yaml:",omitempty"`
	Only []string `yaml:",omitempty"`
}

type User struct {
	Pseudo string
	Avatar string
	Emails []string
	Ssh    []string
}

func ParseGroups(fileContent []byte) ([]Group, error) {
	groups := make([]Group, 0)
	groupsByName := make(map[string]Group)
	if err := yaml.UnmarshalWithOptions(fileContent, &groupsByName, yaml.UseOrderedMap()); err != nil {
		return nil, oops.Wrapf(err, "can't parse yaml users")
	}
	for name, group := range groupsByName {
		group.Name = name
		groups = append(groups, group)
	}
	return groups, nil
}

type SimpleUser struct {
	Pseudo string
	Email  string
	Ssh    string
}

func CreateFileUser(defaultBranch string, users ...SimpleUser) ([]byte, error) {
	u := make([]User, len(users))
	for i, j := range users {
		u[i] = User{
			Pseudo: j.Pseudo,
			Avatar: "",
			Emails: []string{j.Email},
			Ssh:    []string{j.Ssh},
		}
	}
	g := Group{
		Name:     "owner",
		Branches: []Branch{{Name: defaultBranch}},
		Users:    u,
	}
	content, err := yaml.Marshal(map[string]Group{"owner": g})
	return content, oops.Wrapf(err, "can't marshal yaml users")
}

func AppendUserToGroup(file []byte, groupName string, users ...SimpleUser) ([]byte, error) {
	urlPath, err := yaml.PathString(fmt.Sprintf("$.%s.users", groupName))
	if err != nil {
		return nil, oops.Wrapf(err, "invalid path yaml")
	}
	f, err := parser.ParseBytes(file, 0)
	if err != nil {
		return nil, oops.Wrapf(err, "can't parse file")
	}
	u := make([]User, 0)
	for _, j := range users {
		if !bytes.Contains(file, []byte(j.Ssh)) {
			u = append(u, User{
				Pseudo: j.Pseudo,
				Avatar: "",
				Emails: []string{j.Email},
				Ssh:    []string{j.Ssh},
			})
		}
	}
	content, err := yaml.Marshal(u)
	if err != nil {
		return nil, oops.Wrapf(err, "can't marshal u")
	}
	err = urlPath.MergeFromReader(f, bytes.NewReader(content))
	return []byte(f.String()), oops.Wrapf(err, "can't merge yaml")
}

func AppendBranchToGroup(file []byte, groupName string, branches ...string) ([]byte, error) {
	urlPath, err := yaml.PathString(fmt.Sprintf("$.%s.branches", groupName))
	if err != nil {
		return nil, oops.Wrapf(err, "invalid path yaml")
	}
	f, err := parser.ParseBytes(file, 0)
	if err != nil {
		return nil, oops.Wrapf(err, "can't parse file")
	}
	u := make([]Branch, len(branches))
	for i, j := range branches {
		u[i] = Branch{
			Name: j,
		}
	}
	content, err := yaml.Marshal(u)
	if err != nil {
		return nil, oops.Wrapf(err, "can't marshal u")
	}
	err = urlPath.MergeFromReader(f, bytes.NewReader(content))
	return []byte(f.String()), oops.Wrapf(err, "can't merge yaml")
}

func AppendBranch(file []byte, groupName string, users ...SimpleUser) ([]byte, error) {
	u := make([]User, len(users))
	for i, j := range users {
		u[i] = User{
			Pseudo: j.Pseudo,
			Avatar: "",
			Emails: []string{j.Email},
			Ssh:    []string{j.Ssh},
		}
	}
	group := Group{Name: groupName, Branches: []Branch{{Name: groupName}}, Users: u}
	content, err := yaml.Marshal(map[string]Group{groupName: group})
	if err != nil {
		return nil, oops.Wrapf(err, "can't marshal u")
	}

	return AppendBranchToGroup(append(file, content...), "owner", groupName)
}

func FindUser(file []byte, sshKey string) (*GitRootUser, error) {
	groups, err := ParseGroups(file)
	if err != nil {
		return nil, oops.Wrapf(err, "can't read group")
	}
	found := false
	var goodUser User
	var goodGroup *GitRootGroup
	allGroups := make([]*GitRootGroup, 0)
	for _, g := range groups {
		branches := make([]string, len(g.Branches))
		for i, b := range g.Branches {
			branches[i] = b.Name
		}
		currentGroup := &GitRootGroup{
			GroupName: g.Name,
			Branches:  branches,
		}
		allGroups = append(allGroups, currentGroup)
		for _, u := range g.Users {
			for _, s := range u.Ssh {
				if s == sshKey {
					found = true
					goodUser = u
					goodGroup = currentGroup
					break
				}
			}
			if found {
				break
			}
		}

	}
	if !found {
		return &GitRootUser{
			Group:     nil,
			Email:     "",
			Pseudo:    "",
			AllGroups: allGroups,
			PubKey:    sshKey,
		}, nil
	}
	return &GitRootUser{
		Group:     goodGroup,
		Email:     goodUser.Emails[0],
		Pseudo:    goodUser.Pseudo,
		AllGroups: allGroups,
		PubKey:    sshKey,
	}, nil
}

func (u *GitRootUser) CanWrite(branch string) bool {
	// user is in the group of the branch
	if u.Group != nil { // new user
		for _, b := range u.Group.Branches {
			if b == branch || b == "*" {
				return true
			}
		}
	}
	// other group have right for this branch
	for _, g := range u.AllGroups {
		if g.GroupName == "*" {
			return false
		}
		if u.Group == nil || u.Group.GroupName != g.GroupName {
			for _, b := range g.Branches {
				if b == branch {
					return false
				}
			}
		}
	}
	// nobody take care of this branch
	return true
}
