package user

import (
	"gitroot.com/server/logger"
)

type Manager struct {
	rootCommiter *Commiter
	logger       *logger.Logger
	conf         needConf
}

type needConf interface {
	RootCommiterPseudo() string
	IsRootCommiter(pseudo string) bool
	GetEmail(pseudo string) string
	GetDirPathData() string
	GetDirPathDataPlugin(pluginName string) string
}

func NewManager(conf needConf) (*Manager, error) {
	commiter, err := newCommiter(conf.RootCommiterPseudo(), conf)
	if err != nil {
		return nil, err
	}
	return &Manager{
		rootCommiter: commiter,
		logger:       logger.NewLogger(logger.USER_MANAGER),
		conf:         conf,
	}, nil
}

func (m *Manager) RootCommiter() *Commiter {
	return m.rootCommiter
}
