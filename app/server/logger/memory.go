package logger

import (
	"runtime"
)

func (l *Logger) PrintMemUsage() {
	var m runtime.MemStats
	runtime.ReadMemStats(&m)
	// For info on each, see: https://golang.org/pkg/runtime/#MemStats
	l.Info("Memory snapshot",
		NewLoggerPair("Alloc (MiB)", bToMb(m.Alloc)),
		NewLoggerPair("TotalAlloc (MiB)", bToMb(m.TotalAlloc)),
		NewLoggerPair("Sys (MiB)", bToMb(m.Sys)),
		NewLoggerPair("NumGC", m.NumGC),
	)
}

func bToMb(b uint64) uint64 {
	return b / 1024 / 1024
}
