package logger

import "time"

func (l *Logger) Time(msg string) func() {
	start := time.Now()
	return func() {
		elapsed := time.Since(start)
		l.Info(msg, NewLoggerPair("time", elapsed))
	}
}
