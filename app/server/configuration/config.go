package configuration

import (
	"fmt"
	"io/fs"
	"os"
	"path/filepath"
	"strings"
)

type Configuration struct {
	rootCommiterPseudo string

	SshAddr  string
	HttpAddr string

	pathRepositories          string
	pathData                  string
	pathDataPlugin            string
	pathDataWeb               string
	pathCache                 string
	forgeConfigName           string
	defaultBranchName         string
	repositoriesFilename      string
	fileUsersName             string
	pathConfig                string
	filePluginsName           string
	fileRepoConfigurationName string

	nbWorkerBackground int
}

func NewConfiguration(rootPath string) *Configuration {
	return &Configuration{
		rootCommiterPseudo: "GitRoot",

		SshAddr:  fmt.Sprintf("%s:%d", "127.0.0.1", 4545),
		HttpAddr: fmt.Sprintf("%s:%d", "127.0.0.1", 4546),

		pathRepositories:          fmt.Sprintf("%s/repositories", rootPath),
		pathData:                  fmt.Sprintf("%s/data", rootPath),
		pathDataPlugin:            fmt.Sprintf("%s/data/plugins", rootPath),
		pathDataWeb:               fmt.Sprintf("%s/data/web", rootPath),
		pathCache:                 fmt.Sprintf("%s/data/cache", rootPath),
		forgeConfigName:           "root",
		defaultBranchName:         "main",
		repositoriesFilename:      "repositories.yml",
		fileUsersName:             "users.yml",
		pathConfig:                ".gitroot",
		filePluginsName:           "plugins.yml",
		fileRepoConfigurationName: "repoConfiguration.yml",

		nbWorkerBackground: 3,
	}
}

func (c *Configuration) GetEmail(pseudo string) string {
	return fmt.Sprintf("%s@gitroot.com", pseudo)
}

func (c *Configuration) RootCommiterPseudo() string {
	return c.rootCommiterPseudo
}

func (c *Configuration) IsRootCommiter(pseudo string) bool {
	return c.rootCommiterPseudo == pseudo
}

func (c *Configuration) ForgeConfigName() string {
	return c.forgeConfigName
}

func (c *Configuration) DefaultBranchName() string {
	return c.defaultBranchName
}

func (c *Configuration) IsForgeRepo(name string) bool {
	return c.forgeConfigName == name
}

func (c *Configuration) IsForgeRepoDirPath(dir string) bool {
	return strings.HasSuffix(fmt.Sprintf("%s/%s", c.pathRepositories, c.forgeConfigName), dir)
}

func (c *Configuration) PathRepositories() string {
	return c.pathRepositories
}

func (c *Configuration) PathFileUsers() string {
	return fmt.Sprintf("%s/%s", c.pathConfig, c.fileUsersName)
}

func (c *Configuration) PathFileRepositories() string {
	return fmt.Sprintf("%s/%s", c.pathConfig, c.repositoriesFilename)
}

func (c *Configuration) PathFilePlugins() string {
	return fmt.Sprintf("%s/%s", c.pathConfig, c.filePluginsName)
}

func (c *Configuration) PathFileRepoConfigurationName() string {
	return fmt.Sprintf("%s/%s", c.pathConfig, c.fileRepoConfigurationName)
}

func (c *Configuration) GetDirPathForRepo(repoName string) string {
	return fmt.Sprintf("%s/%s", c.pathRepositories, repoName)
}

func (c *Configuration) GetDirPathData() string {
	return c.pathData
}

func (c *Configuration) PathDataPlugin() string {
	return c.pathDataPlugin
}

func (c *Configuration) DataWeb(repoName string) fs.FS {
	return os.DirFS(c.PathDataWeb(repoName))
}

func (c *Configuration) PathDataWeb(repoName string) string {
	return fmt.Sprintf("%s/%s/", c.pathDataWeb, repoName)
}

func (c *Configuration) Cache(repoName string, pluginName string) fs.FS {
	return os.DirFS(filepath.Join(c.pathCache, repoName, pluginName))
}

func (c *Configuration) PathCache() string {
	return c.pathCache
}

func (c *Configuration) GetDirPathDataPlugin(pluginName string) string {
	return fmt.Sprintf("%s/%s", c.pathDataPlugin, pluginName)
}

func (c *Configuration) ForPathConfig(name string) string {
	return fmt.Sprintf("%s/%s", c.pathConfig, name)
}

func (c *Configuration) NbWorkerBackground() int {
	return c.nbWorkerBackground
}
