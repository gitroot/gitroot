package background

import (
	"bytes"
	"os"

	"github.com/samber/oops"
	"gitroot.com/server/plugin"
	"gitroot.com/server/user"
)

type checkPluginsTaskInput struct {
	repoName         string
	isRootRepo       bool
	availablePlugins []plugin.Plugin
}

func (m *Manager) syncPluginsInExistingRepos() {
	plugins, err := m.pluginManager.Availables(m.ctx)
	if err != nil {
		m.logger.Error("can't get availables plugins", err)
		return
	}

	entries, err := os.ReadDir(m.conf.PathRepositories())
	if err != nil {
		m.logger.Error("can't read dir of repositories", err)
		return
	}

	for _, repoName := range entries {
		m.Start(Task{
			call: m.checkPlugins,
			t: checkPluginsTaskInput{
				repoName:         repoName.Name(),
				isRootRepo:       m.conf.IsForgeRepo(repoName.Name()),
				availablePlugins: plugins,
			},
		})
	}
}

func (m *Manager) checkPlugins(input interface{}) error {
	checkInput := input.(checkPluginsTaskInput)
	repoName := checkInput.repoName
	isRootRepo := checkInput.isRootRepo
	availablePlugins := checkInput.availablePlugins

	repo, err := m.repoManager.Open(m.ctx, repoName)
	defer repo.Close()
	if err != nil {
		return oops.Wrapf(err, "can't open repo")
	}
	repo.RLock()
	pluginsContent, err := repo.ContentPluginsConf()
	if err != nil {
		repo.RUnlock()
		return oops.Wrapf(err, "can't get plugin conf")
	}
	repo.RUnlock()
	plugins, err := plugin.Parse(pluginsContent)
	if err != nil {
		return oops.Wrapf(err, "can't parse plugin conf")
	}

	usersToAdd := make([]user.SimpleUser, 0)
	usersToDel := make([]user.SimpleUser, 0)

	for _, ap := range availablePlugins {
		found := false
		for i, p := range plugins {
			if ap.Name == p.Name {
				found = true
				plugins[i] = ap.SetActive(p.Active) //replace plugin in project by fullloaded one
				break
			}
		}
		if !found {
			plugins = append(plugins, ap)
		}
		usersToAdd = append(usersToAdd, ap.Commiter().SimpleUser)
	}

	for i, p := range plugins {
		found := false
		for _, ap := range availablePlugins {
			if ap.Name == p.Name {
				found = true
				break
			}
		}
		if !found {
			plugins = append(plugins[:i], plugins[i+1:]...)
			usersToDel = append(usersToDel, p.Commiter().SimpleUser)
		}
	}

	newPluginsContent, _ := m.pluginManager.ToNewRepo(isRootRepo, plugins)

	repo.WLock()
	defer repo.WUnlock()

	if err := repo.Write(m.conf.PathFilePlugins(), newPluginsContent); err != nil {
		return oops.Wrapf(err, "can't write new plugin conf")
	}

	fileUsersContent, err := repo.AppendUserToGroup("owner", usersToAdd...)
	if err != nil {
		return oops.With("userToAdd", usersToAdd).Wrapf(err, "can't add user to users file")
	}
	err = repo.Write(m.conf.PathFileUsers(), fileUsersContent)
	if err != nil {
		return oops.Wrapf(err, "can't write users file")
	}

	allowedSignersContent, err := repo.Content(m.conf.ForPathConfig("allowed_signers"))
	if err != nil {
		return oops.Wrapf(err, "can't read allowed_signers file")
	}

	sshString := bytes.NewBuffer(allowedSignersContent)
	for _, u := range usersToAdd {
		sshString.WriteString(u.Email)
		sshString.WriteString(" ")
		sshString.WriteString(u.Ssh)
		sshString.WriteString("\n")
	}

	if err := repo.Write(m.conf.ForPathConfig("allowed_signers"), sshString.Bytes()); err != nil {
		return oops.Wrapf(err, "can't write allowed_signers file")
	}

	// TODO delete

	if _, err := repo.CommitAll("sync plugins", m.userManager.RootCommiter()); err != nil {
		return err
	}

	return nil
}
