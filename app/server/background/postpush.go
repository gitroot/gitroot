package background

import (
	"github.com/go-git/go-git/v5/plumbing/protocol/packp"
	"github.com/samber/oops"
	"gitroot.com/server/logger"
	"gitroot.com/server/plugin"
	"gitroot.com/server/user"
)

type postPushTaskInput struct {
	pusher   user.SimpleUser
	repoName string
	commands []*packp.Command
}

func (m *Manager) PostPush(pusher user.SimpleUser, repoName string, commands []*packp.Command) {
	m.Start(Task{
		call: m.postPush,
		t: postPushTaskInput{
			pusher:   pusher,
			repoName: repoName,
			commands: commands,
		},
	})
}

func (m *Manager) postPush(input interface{}) error {
	postPushInput := input.(postPushTaskInput)
	errorHandler := oops.
		With("pusher", postPushInput.pusher).
		With("repoName", postPushInput.repoName).
		With("commands", len(postPushInput.commands))

	repo, err := m.repoManager.Open(m.ctx, postPushInput.repoName)
	if err != nil {
		return errorHandler.Wrapf(err, "can't open repository")
	}

	repo.RLock()

	cmds, err := plugin.CommandForDiffFromPackpCmd(m.ctx, repo, postPushInput.commands, postPushInput.pusher)
	if err != nil {
		return errorHandler.Wrapf(err, "CommandForDiffFromPackpCmd error")
	}

	if m.conf.IsForgeRepo(postPushInput.repoName) {
		repoConfiguration, err := repo.Configuration()
		if err != nil {
			return errorHandler.Wrapf(err, "can't read configuration in forgerepo")
		}
		forgeRepoRepositoriesFileTouched := plugin.IsFileTouched(cmds, repoConfiguration.DefaultBranch, m.conf.PathFileRepositories())
		forgeRepoPluginsFileTouched := plugin.IsFileTouched(cmds, repoConfiguration.DefaultBranch, m.conf.PathFilePlugins())
		m.logger.Info(
			"postpush commands",
			logger.NewLoggerPair("repo", postPushInput.repoName),
			logger.NewLoggerPair("defaultBranch", repoConfiguration.DefaultBranch),
			logger.NewLoggerPair("forgeRepoRepositoriesFileTouched", forgeRepoRepositoriesFileTouched),
			logger.NewLoggerPair("forgeRepoPluginsFileTouched", forgeRepoPluginsFileTouched),
		)

		if forgeRepoPluginsFileTouched {
			filePlugin, err := repo.ContentPluginsConf()
			if err != nil {
				return errorHandler.Wrapf(err, "can't open file plugin in forgerepo")
			}
			if err := m.pluginManager.Sync(filePlugin); err != nil {
				return errorHandler.Wrapf(err, "can't sync plugins files")
			}
			defer m.syncPluginsInExistingRepos()
		}

		if forgeRepoRepositoriesFileTouched {
			fileContent, err := repo.ContentRepositoriesConf()
			if err != nil {
				errorHandler.Wrapf(err, "can't open file repositories")
			}
			plugins, err := m.pluginManager.Availables(m.ctx)
			if err != nil {
				return errorHandler.Wrapf(err, "can't get availables plugins")
			}
			contentPlugins, users := m.pluginManager.ToNewRepo(false, plugins)
			if err := m.repoManager.ForgeRepoMakeDiffRepos(m.ctx, fileContent, append(users, postPushInput.pusher), contentPlugins); err != nil {
				return errorHandler.Wrapf(err, "can't diff repositories between forgeConfg and local")
			}
		}
	}

	repo.RUnlock()

	if err := m.pluginManager.Run(m.ctx, postPushInput.repoName, cmds); err != nil {
		return errorHandler.Wrapf(err, "plugin error")
	}

	return nil
}
