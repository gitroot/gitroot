package background

import (
	"context"

	"gitroot.com/server/logger"
	"gitroot.com/server/plugin"
	"gitroot.com/server/repository"
	"gitroot.com/server/user"
)

type Task struct {
	call func(input interface{}) error
	t    interface{}
}

type Manager struct {
	logger        *logger.Logger
	ctx           context.Context
	conf          needConf
	queue         chan Task
	repoManager   needRepo
	userManager   needUser
	pluginManager needPlugin
}

type needConf interface {
	PathRepositories() string
	IsForgeRepo(name string) bool
	PathFileRepositories() string
	PathFilePlugins() string
	PathFileUsers() string
	ForPathConfig(filename string) string
	NbWorkerBackground() int
}

type needRepo interface {
	Open(ctx context.Context, repoName string) (*repository.GitRootRepository, error)
	ForgeRepoMakeDiffRepos(ctx context.Context, repositories []byte, users []user.SimpleUser, plugins []byte) error
}

type needUser interface {
	RootCommiter() *user.Commiter
}

type needPlugin interface {
	Availables(ctx context.Context) ([]plugin.Plugin, error)
	ToNewRepo(isRootRepo bool, plugins []plugin.Plugin) ([]byte, []user.SimpleUser)
	Sync(filePlugin []byte) error
	Run(ctx context.Context, repoName string, commands []plugin.CommandForDiff) error
}

func NewManager(ctx context.Context, conf needConf, repoManager needRepo, userManager needUser, pluginManager needPlugin) *Manager {
	m := &Manager{
		logger:        logger.NewLogger(logger.BACKGROUND_MANAGER),
		ctx:           ctx,
		conf:          conf,
		queue:         make(chan Task, 100), //block after 100 jobs in queue
		repoManager:   repoManager,
		userManager:   userManager,
		pluginManager: pluginManager,
	}
	m.work()
	return m
}

func (m *Manager) Start(task Task) {
	go func() {
		m.queue <- task
	}()
}

func (m *Manager) work() {
	for i := 0; i < m.conf.NbWorkerBackground(); i++ {
		go func() {
			for {
				select {
				case job := <-m.queue:
					if err := job.call(job.t); err != nil {
						m.logger.Error("Job error", err)
					}
				case <-m.ctx.Done():
					return
				}
			}
		}()
	}
}
