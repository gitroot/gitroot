package fs

import (
	"context"
	"io/fs"

	"github.com/go-git/go-billy/v5/memfs"
	"gitroot.com/server/logger"
)

type UpdatableFs interface {
	fs.FS
	Clear()
	Update(fs fs.FS)
}

type updatableFs struct {
	ctx    context.Context
	logger *logger.Logger
	sub    fs.FS
}

func NewUpdatableFs(ctx context.Context, sub fs.FS) *updatableFs {
	return &updatableFs{
		ctx:    ctx,
		logger: logger.NewLoggerCtx(logger.FS_LOGGER_NAME, ctx).NewSubLogger("updatableFs"),
		sub:    sub,
	}
}

func (m *updatableFs) Clear() {
	m.sub = ToFs(m.ctx, memfs.New())
}

func (m *updatableFs) Update(filesystem fs.FS) {
	m.sub = filesystem
}

func (m *updatableFs) ReadFile(name string) ([]byte, error) {
	m.logger.Debug("ReadFile", logger.NewLoggerPair("file", name))
	return fs.ReadFile(m.sub, name)
}

func (m *updatableFs) ReadDir(name string) ([]fs.DirEntry, error) {
	m.logger.Debug("ReadDir", logger.NewLoggerPair("file", name))
	return fs.ReadDir(m.sub, name)
}

func (m *updatableFs) Stat(name string) (fs.FileInfo, error) {
	m.logger.Debug("Stat", logger.NewLoggerPair("file", name))
	return fs.Stat(m.sub, name)
}

func (m *updatableFs) Sub(dir string) (fs.FS, error) {
	m.logger.Debug("Sub", logger.NewLoggerPair("dir", dir))
	return fs.Sub(m.sub, dir)
}

func (m *updatableFs) Open(name string) (fs.File, error) {
	m.logger.Debug("Open", logger.NewLoggerPair("name", name))
	return m.sub.Open(name)
}
