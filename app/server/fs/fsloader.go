package fs

import (
	"context"

	"github.com/go-git/go-billy/v5"
	"github.com/go-git/go-git/v5/plumbing/cache"
	"github.com/go-git/go-git/v5/plumbing/storer"
	"github.com/go-git/go-git/v5/plumbing/transport"
	"github.com/go-git/go-git/v5/storage/filesystem"
	"github.com/go-git/go-git/v5/storage/memory"
	"github.com/go-git/go-git/v5/storage/transactional"
	"github.com/samber/oops"
)

type gitRootFsLoader struct {
	ctx       context.Context
	base      billy.Filesystem
	storer    transactional.Storage
	localRepo *filesystem.Storage
}

func NewGitRootFsLoader(ctx context.Context, base billy.Filesystem) *gitRootFsLoader {
	return &gitRootFsLoader{
		ctx:  ctx,
		base: base,
	}
}

func (fs *gitRootFsLoader) Load(ep *transport.Endpoint) (storer.Storer, error) {
	f, err := fs.base.Chroot(ep.Path)
	if err != nil {
		return nil, err
	}

	if _, err := f.Stat("config"); err != nil {
		return nil, transport.ErrRepositoryNotFound
	}
	fs.localRepo = filesystem.NewStorage(f, cache.NewObjectLRUDefault())
	tempStorage := memory.NewStorage()
	fs.storer = transactional.NewStorage(fs.localRepo, tempStorage)
	return NewGitRootFsStorer(fs.ctx, fs.storer), nil
}

func (fs *gitRootFsLoader) GetStorer() (transactional.Storage, error) {
	if fs.storer == nil {
		return nil, oops.Errorf("can't open repository before load it")
	}
	return fs.storer, nil
}

func (fs *gitRootFsLoader) Commit() error {
	// TODO close fs.localRepo
	return fs.storer.Commit()
}
