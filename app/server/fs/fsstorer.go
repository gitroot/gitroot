package fs

import (
	"context"
	"io"

	"github.com/go-git/go-git/v5/plumbing"
	"github.com/go-git/go-git/v5/plumbing/storer"
	"gitroot.com/server/logger"
)

type gitRootFsStorer struct {
	logger *logger.Logger
	storer storer.Storer
}

func NewGitRootFsStorer(ctx context.Context, storer storer.Storer) *gitRootFsStorer {
	return &gitRootFsStorer{
		logger: logger.NewLoggerCtx(logger.FS_STORER_LOGGER_NAME, ctx),
		storer: storer,
	}
}

func (st *gitRootFsStorer) RawObjectWriter(typ plumbing.ObjectType, sz int64) (w io.WriteCloser, err error) {
	st.logger.Debug("RawObjectWriter")
	return st.storer.RawObjectWriter(typ, sz)
}
func (st *gitRootFsStorer) NewEncodedObject() plumbing.EncodedObject {
	st.logger.Debug("NewEncodedObject")
	return st.storer.NewEncodedObject()
}
func (st *gitRootFsStorer) SetEncodedObject(obj plumbing.EncodedObject) (plumbing.Hash, error) {
	st.logger.Debug("SetEncodedObject", logger.NewLoggerPair("type", obj.Type().String()))
	return st.storer.SetEncodedObject(obj)
}
func (st *gitRootFsStorer) EncodedObject(obj plumbing.ObjectType, hash plumbing.Hash) (plumbing.EncodedObject, error) {
	st.logger.Debug("EncodedObject", logger.NewLoggerPair("obj", obj.String()))
	return st.storer.EncodedObject(obj, hash)
}
func (st *gitRootFsStorer) IterEncodedObjects(obj plumbing.ObjectType) (storer.EncodedObjectIter, error) {
	st.logger.Debug("IterEncodedObjects", logger.NewLoggerPair("obj", obj.String()))
	return st.storer.IterEncodedObjects(obj)
}
func (st *gitRootFsStorer) HasEncodedObject(hash plumbing.Hash) error {
	st.logger.Debug("HasEncodedObject", logger.NewLoggerPair("hash", hash.String()))
	return st.storer.HasEncodedObject(hash)
}
func (st *gitRootFsStorer) EncodedObjectSize(hash plumbing.Hash) (int64, error) {
	st.logger.Debug("EncodedObjectSize", logger.NewLoggerPair("hash", hash.String()))
	return st.storer.EncodedObjectSize(hash)
}
func (st *gitRootFsStorer) AddAlternate(remote string) error {
	st.logger.Debug("AddAlternate", logger.NewLoggerPair("remote", remote))
	return st.storer.AddAlternate(remote)
}

func (st *gitRootFsStorer) SetReference(ref *plumbing.Reference) error {
	st.logger.Debug("SetReference", logger.NewLoggerPair("refName", ref.Name().String()), logger.NewLoggerPair("refType", ref.Type().String()))
	return st.storer.SetReference(ref)
}
func (st *gitRootFsStorer) CheckAndSetReference(new, old *plumbing.Reference) error {
	st.logger.Debug("CheckAndSetReference", logger.NewLoggerPair("newName", new.Name().String()), logger.NewLoggerPair("newType", new.Type().String()))
	return st.storer.CheckAndSetReference(new, old)
}
func (st *gitRootFsStorer) Reference(ref plumbing.ReferenceName) (*plumbing.Reference, error) {
	st.logger.Debug("Reference", logger.NewLoggerPair("ref", ref.String()))
	return st.storer.Reference(ref)
}
func (st *gitRootFsStorer) IterReferences() (storer.ReferenceIter, error) {
	st.logger.Debug("IterReferences")
	return st.storer.IterReferences()
}
func (st *gitRootFsStorer) RemoveReference(ref plumbing.ReferenceName) error {
	st.logger.Debug("RemoveReference", logger.NewLoggerPair("ref", ref.String()))
	return st.storer.RemoveReference(ref)
}
func (st *gitRootFsStorer) CountLooseRefs() (int, error) {
	st.logger.Debug("CountLooseRefs")
	return st.storer.CountLooseRefs()
}
func (st *gitRootFsStorer) PackRefs() error {
	st.logger.Debug("PackRefs")
	return st.storer.PackRefs()
}

// TODO memory storage is not a packfilewriter
// see fs.storer = transactional.NewStorage(fs.localRepo, memory.NewStorage()) in app/server/fs/fsloader.go
// func (st *gitRootFsStorer) PackfileWriter() (io.WriteCloser, error) {
// 	st.logger.Debug("PackfileWriter")
// 	if pw, ok := st.storer.(storer.PackfileWriter); ok {
// 		return pw.PackfileWriter()
// 	}
// 	return nil, errors.New("not implemented")
// }
