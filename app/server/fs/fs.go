package fs

import (
	"context"
	"os"

	"github.com/go-git/go-billy/v5"
	"github.com/go-git/go-billy/v5/osfs"
	"gitroot.com/server/logger"
)

type gitRootFs struct {
	logger *logger.Logger
	os     billy.Filesystem
}

func NewGitRootFs(ctx context.Context, dir string) *gitRootFs {
	return &gitRootFs{
		logger: logger.NewLoggerCtx(logger.FS_LOGGER_NAME, ctx).With("dir", dir),
		os:     osfs.New(dir, osfs.WithChrootOS()),
	}
}

func (fs *gitRootFs) Create(filename string) (billy.File, error) {
	fs.logger.Debug("Create", logger.NewLoggerPair("filename", filename))
	return fs.os.Create(filename)
}
func (fs *gitRootFs) Open(filename string) (billy.File, error) {
	fs.logger.Debug("Open", logger.NewLoggerPair("filename", filename))
	return fs.os.Open(filename)
}
func (fs *gitRootFs) OpenFile(filename string, flag int, perm os.FileMode) (billy.File, error) {
	fs.logger.Debug("OpenFile", logger.NewLoggerPair("filename", filename), logger.NewLoggerPair("flag", flag), logger.NewLoggerPair("perm", perm))
	return fs.os.OpenFile(filename, flag, perm)
}
func (fs *gitRootFs) Stat(filename string) (os.FileInfo, error) {
	fs.logger.Debug("Stat", logger.NewLoggerPair("filename", filename))
	return fs.os.Stat(filename)
}
func (fs *gitRootFs) Rename(oldpath, newpath string) error {
	fs.logger.Debug("Rename", logger.NewLoggerPair("oldpath", oldpath), logger.NewLoggerPair("newpath", newpath))
	return fs.os.Rename(oldpath, newpath)
}
func (fs *gitRootFs) Remove(filename string) error {
	fs.logger.Debug("Remove", logger.NewLoggerPair("filename", filename))
	return fs.os.Remove(filename)
}
func (fs *gitRootFs) Join(elem ...string) string {
	fs.logger.Debug("Join", logger.NewLoggerPair("elem", elem))
	return fs.os.Join(elem...)
}

func (fs *gitRootFs) TempFile(dir, prefix string) (billy.File, error) {
	fs.logger.Debug("TempFile", logger.NewLoggerPair("dir", dir), logger.NewLoggerPair("prefix", prefix))
	return fs.os.TempFile(dir, prefix)
}

func (fs *gitRootFs) ReadDir(path string) ([]os.FileInfo, error) {
	fs.logger.Debug("ReadDir", logger.NewLoggerPair("path", path))
	return fs.os.ReadDir(path)
}
func (fs *gitRootFs) MkdirAll(filename string, perm os.FileMode) error {
	fs.logger.Debug("MkdirAll", logger.NewLoggerPair("filename", filename), logger.NewLoggerPair("perm", perm))
	return fs.os.MkdirAll(filename, perm)
}

func (fs *gitRootFs) Lstat(filename string) (os.FileInfo, error) {
	fs.logger.Debug("Lstat", logger.NewLoggerPair("filename", filename))
	return fs.os.Lstat(filename)
}
func (fs *gitRootFs) Symlink(target, link string) error {
	fs.logger.Debug("Symlink", logger.NewLoggerPair("target", target), logger.NewLoggerPair("link", link))
	return fs.os.Symlink(target, link)
}
func (fs *gitRootFs) Readlink(link string) (string, error) {
	fs.logger.Debug("Readlink", logger.NewLoggerPair("link", link))
	return fs.os.Readlink(link)
}

func (fs *gitRootFs) Chroot(path string) (billy.Filesystem, error) {
	fs.logger.Debug("Chroot", logger.NewLoggerPair("path", path))
	chroot, err := fs.os.Chroot(path)
	if err != nil {
		fs.logger.Error("Chroot error", err)
		return nil, err
	}
	return &gitRootFs{
		logger: fs.logger.NewSubLogger(chroot.Root()),
		os:     chroot,
	}, nil
}
func (fs *gitRootFs) Root() string {
	fs.logger.Debug("Root", logger.NewLoggerPair("Root", fs.os.Root()))
	return fs.os.Root()
}
