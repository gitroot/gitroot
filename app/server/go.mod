module gitroot.com/server

go 1.22.0

toolchain go1.22.4

replace github.com/go-git/go-git/v5 v5.12.0 => ../../../go-git

require (
	github.com/anmitsu/go-shlex v0.0.0-20200514113438-38f4b401e2be
	github.com/cyphar/filepath-securejoin v0.3.5
	github.com/go-git/go-billy/v5 v5.6.0
	github.com/go-git/go-git/v5 v5.12.0
	github.com/goccy/go-yaml v1.13.6
	github.com/samber/oops v1.14.1
	github.com/tetratelabs/wazero v1.8.2
	golang.org/x/crypto v0.32.0
)

require (
	github.com/davecgh/go-spew v1.1.2-0.20180830191138-d8f796af33cc // indirect
	go.opentelemetry.io/otel v1.29.0 // indirect
	go.opentelemetry.io/otel/trace v1.29.0 // indirect
	golang.org/x/exp v0.0.0-20241215155358-4a5509556b9e // indirect
	golang.org/x/text v0.21.0 // indirect
)

require (
	dario.cat/mergo v1.0.1 // indirect
	github.com/42wim/sshsig v0.0.0-20240818000253-e3a6333df815
	github.com/Microsoft/go-winio v0.6.2 // indirect
	github.com/ProtonMail/go-crypto v1.1.5 // indirect
	github.com/cloudflare/circl v1.5.0 // indirect
	github.com/emirpasic/gods v1.18.1 // indirect
	github.com/fatih/color v1.18.0 // indirect
	github.com/go-git/gcfg v1.5.1-0.20230307220236-3a3c6141e376 // indirect
	github.com/golang/groupcache v0.0.0-20241129210726-2c02b8208cf8 // indirect
	github.com/kevinburke/ssh_config v1.2.0 // indirect
	github.com/mattn/go-colorable v0.1.13 // indirect
	github.com/mattn/go-isatty v0.0.20 // indirect
	github.com/oklog/ulid/v2 v2.1.0 // indirect
	github.com/pjbgf/sha1cd v0.3.2 // indirect
	github.com/samber/lo v1.47.0 // indirect
	github.com/sergi/go-diff v1.3.2-0.20230802210424-5b0b94c5c0d3 // indirect
	golang.org/x/net v0.34.0 // indirect
	golang.org/x/sys v0.29.0 // indirect
	gopkg.in/warnings.v0 v0.1.2 // indirect
)
