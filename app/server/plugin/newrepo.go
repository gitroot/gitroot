package plugin

import (
	"github.com/goccy/go-yaml"
	"gitroot.com/server/user"
)

func (m *Manager) ToNewRepo(isRootRepo bool, plugins []Plugin) ([]byte, []user.SimpleUser) {
	toSerialize := make([]Plugin, len(plugins))
	users := make([]user.SimpleUser, len(plugins))
	for i, p := range plugins {
		url := ""
		crc := ""
		if isRootRepo {
			url = p.Url
			crc = p.Crc
		}
		toSerialize[i] = Plugin{
			Url:           url,
			Crc:           crc,
			Name:          p.Name,
			Branch:        p.Branch,
			Path:          p.Path,
			Active:        p.Active,
			Configuration: p.Configuration,
		}
		users[i] = p.commiter.SimpleUser
	}
	b, _ := yaml.Marshal(toSerialize)
	return b, users
}
