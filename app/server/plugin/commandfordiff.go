package plugin

import (
	"context"

	"github.com/go-git/go-git/v5/plumbing"
	"github.com/go-git/go-git/v5/plumbing/object"
	"github.com/go-git/go-git/v5/plumbing/protocol/packp"
	"gitroot.com/server/repository"
	"gitroot.com/server/user"
)

type commitForDiffAction int

const (
	commitForDiffActionAdd commitForDiffAction = iota
	commitForDiffActionMod
	commitForDiffActionDel
	commitForDiffActionRename
)

type CommandForDiff struct {
	branch       plumbing.ReferenceName
	branchAction commitForDiffAction
	commits      []commitForDiffCommit
	pusher       user.SimpleUser
}

type commitForDiffCommit struct {
	parent      *object.Commit
	hash        plumbing.Hash
	message     string
	sshCommiter string
	files       []commitForDiffFile
}

type commitForDiffFile struct {
	path    string
	oldPath string //only if action == commitForDiffActionRename
	action  commitForDiffAction
}

func CommandForDiffFromPackpCmd(ctx context.Context, repo *repository.GitRootRepository, commands []*packp.Command, pusher user.SimpleUser) ([]CommandForDiff, error) {
	res := make([]CommandForDiff, len(commands))
	for i, cmd := range commands {
		branchAction := commitForDiffActionMod
		if cmd.New == plumbing.ZeroHash {
			branchAction = commitForDiffActionDel
		} else if cmd.Old == plumbing.ZeroHash {
			branchAction = commitForDiffActionAdd
		}
		commits := make([]commitForDiffCommit, 0)
		if cmd.Old.String() != cmd.New.String() { //nothing todo
			if err := repo.WalkCommit(cmd.Old, cmd.New, func(com *object.Commit) error {
				parent, err := com.Parent(0) // TODO what parent?
				if err != nil {
					return err
				}
				patch, err := parent.PatchContext(ctx, com)
				if err != nil {
					return err
				}
				files := make([]commitForDiffFile, 0)
				for _, d := range patch.FilePatches() {
					from, to := d.Files()
					fileAction := commitForDiffActionMod
					path := ""
					oldPath := ""
					if from == nil && to != nil {
						fileAction = commitForDiffActionAdd
						path = to.Path()
					} else if from != nil && to == nil {
						fileAction = commitForDiffActionDel
						path = from.Path()
					} else if from.Path() != to.Path() {
						fileAction = commitForDiffActionRename
						path = to.Path()
						oldPath = from.Path()
					} else {
						fileAction = commitForDiffActionMod
						path = from.Path()
					}
					files = append(files, commitForDiffFile{path: path, oldPath: oldPath, action: fileAction})
				}
				commits = append(commits, commitForDiffCommit{
					parent:      parent,
					hash:        com.Hash,
					message:     com.Message,
					sshCommiter: com.PGPSignature,
					files:       files,
				})
				return nil
			}); err != nil {
				return nil, err
			}
		}
		res[i] = CommandForDiff{
			branch:       cmd.Name,
			branchAction: branchAction,
			commits:      commits,
			pusher:       pusher,
		}
	}
	return res, nil
}

func (c CommandForDiff) IsFileTouched(branch plumbing.ReferenceName, filepath string) bool {
	if branch != c.branch {
		return false
	}
	for _, com := range c.commits {
		for _, f := range com.files {
			if f.path == filepath || f.oldPath == filepath {
				return true
			}
		}
	}
	return false
}

func IsFileTouched(cmds []CommandForDiff, branch plumbing.ReferenceName, filepath string) bool {
	for _, c := range cmds {
		if branch != c.branch {
			continue
		}
		for _, com := range c.commits {
			for _, f := range com.files {
				if f.path == filepath || f.oldPath == filepath {
					return true
				}
			}
		}
	}
	return false
}
