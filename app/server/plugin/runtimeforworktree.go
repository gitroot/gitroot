package plugin

import (
	"context"
	"fmt"
	"io/fs"
	"os"
	"strings"

	"github.com/tetratelabs/wazero"
	"github.com/tetratelabs/wazero/sys"
	"gitroot.com/server/logger"
	"gitroot.com/server/repository"
)

func (r *runtime) worktree(ctx context.Context, repo *repository.GitRootRepository, plugins []Plugin, cmds []CommandForDiff) error {
	r.fsWorktree.Update(repo.ToFs(ctx))
	r.fsWebcontent.Update(r.manager.conf.DataWeb(repo.Name()))
	r.repo = repo
	r.command = &cmds[len(cmds)-1] // TODO should not be last cmd see ./manager.go line 65 todo
	r.commit = &r.command.commits[len(r.command.commits)-1]

	for _, plugin := range plugins {
		r.fsCache.Update(r.manager.conf.Cache(repo.Name(), plugin.Name))
		r.plugin = plugin
		timerStop := r.logger.Time(fmt.Sprintf("Timer %s", plugin.Name))

		r.logger.Debug("start plugin worktree", logger.NewLoggerPair("name", plugin.Name))
		m := r.wazRun.Module(plugin.Name)
		if m == nil {
			r.logger.Debug("instantiate plugin conf", logger.NewLoggerPair("name", plugin.Name))
			config := wazero.NewModuleConfig().
				WithStdout(os.Stdout).WithStderr(os.Stderr).
				WithFSConfig(wazero.NewFSConfig().
					WithFSMount(r.fsWorktree, "worktree").
					WithFSMount(r.fsWebcontent, "webcontent").
					WithFSMount(r.fsCache, "cache"))
			mod, err := r.wazRun.InstantiateWithConfig(ctx, plugin.content, config.WithName(plugin.Name).WithStartFunctions("_initialize", "install"))
			if err != nil {
				if exitErr, ok := err.(*sys.ExitError); ok && exitErr.ExitCode() != 0 {
					fmt.Fprintf(os.Stderr, "exit_code: %d\n", exitErr.ExitCode())
					return err
				} else if !ok {
					return err
				}
			}
			m = mod
		} else {
			r.logger.Debug("already exist conf", logger.NewLoggerPair("name", plugin.Name))
		}
		startCommit := m.ExportedFunction("startCommit")
		endCommit := m.ExportedFunction("endCommit")
		addFile := m.ExportedFunction("addFile")
		malloc := m.ExportedFunction("malloc")
		free := m.ExportedFunction("free")

		if init := m.ExportedFunction("init"); init != nil {
			arg, err := plugin.Marshal()
			if err != nil {
				return err
			}
			if err := writeMemoryAndCall(ctx, m, init, malloc, free, repo.Name(), arg); err != nil {
				return err
			}
		}

		if startCommit != nil {
			if err := writeMemoryAndCall(ctx, m, startCommit, malloc, free, r.command.branch.Short(), r.commit.hash.String(), r.commit.message, r.commit.parent.PGPSignature); err != nil {
				return err
			}
		}

		fs.WalkDir(r.fsWorktree, ".", func(path string, d fs.DirEntry, err error) error {
			if !d.IsDir() && plugin.glob.Match(path) {
				writeMemoryAndCall(ctx, m, addFile, malloc, free, strings.TrimPrefix(path, "/"))
			}
			return nil
		})

		if err := writeMemoryAndCall(ctx, m, endCommit, malloc, free, r.command.branch.Short(), r.commit.hash.String(), r.commit.message); err != nil {
			return err
		}

		if finish := m.ExportedFunction("finish"); finish != nil {
			if _, err := finish.Call(ctx); err != nil {
				return err
			}
		}

		r.logger.Debug("finish plugin conf", logger.NewLoggerPair("name", plugin.Name))
		timerStop()
	}

	return nil
}
