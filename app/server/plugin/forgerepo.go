package plugin

import (
	"io/fs"
	"os"

	"github.com/goccy/go-yaml"
	"github.com/samber/oops"
)

// Sync add/remove wasm package in global forge directory
func (m *Manager) Sync(fileContent []byte) error {
	errorHandler := oops.With("pluginsDataDir", m.conf.PathDataPlugin()).With("file", fileContent)
	needReloadPlugins := false

	plugins := make([]Plugin, 0)
	if err := yaml.Unmarshal(fileContent, &plugins); err != nil {
		return errorHandler.Wrapf(err, "can't unmarshal yaml")
	}
	entries, err := os.ReadDir(m.conf.PathDataPlugin())
	if err != nil {
		if os.IsNotExist(err) {
			if err := os.MkdirAll(m.conf.PathDataPlugin(), os.ModePerm); err != nil {
				return errorHandler.Wrapf(err, "can't create plugin directory")
			}
			entries = make([]fs.DirEntry, 0)
		} else {
			return errorHandler.Wrapf(err, "can't read directory")
		}
	}
	for _, plugin := range plugins {
		found := false
		for _, e := range entries {
			if e.Name() == plugin.Name {
				found = true
				break
			}
		}
		if !found {
			if err := m.add(plugin); err != nil {
				return errorHandler.Wrap(err)
			}
			needReloadPlugins = true
		}
	}

	for _, e := range entries {
		found := false
		for _, plugin := range plugins {
			if e.Name() == plugin.Name {
				found = true
				break
			}
		}
		if !found {
			if err := os.Remove(m.conf.GetDirPathDataPlugin(e.Name())); err != nil {
				return errorHandler.With("plugin", e.Name()).Wrapf(err, "can't delete plugin")
			}
			needReloadPlugins = true
		}
	}

	if needReloadPlugins {
		m.reloadPlugins()
	}

	return nil
}

func (m *Manager) add(plugin Plugin) error {
	if plugin.Name == "" {
		plugin = plugin.DetermineNameFromUrl()
	}
	if err := os.MkdirAll(m.conf.GetDirPathDataPlugin(plugin.Name), os.ModePerm); err != nil {
		return oops.With("plugin", plugin.Name).Wrapf(err, "can't MkdirAll")
	}
	if err := m.Copy(plugin); err != nil {
		return oops.With("plugin", plugin.Url).Wrapf(err, "can't copy plugin")
	}
	return nil
}
