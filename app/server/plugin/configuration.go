package plugin

import (
	"fmt"
	"io"
	"os"
	"reflect"
	"strconv"
	"strings"

	"github.com/goccy/go-yaml"
	"github.com/samber/oops"
	"gitroot.com/server/user"
)

type Plugin struct {
	Url           string `yaml:",omitempty"`
	Crc           string `yaml:",omitempty"`
	Path          string
	Branch        []string
	glob          *glob
	Name          string
	Active        bool
	Configuration []map[string]interface{}
	content       []byte
	commiter      *user.Commiter
}

func Parse(fileContent []byte) ([]Plugin, error) {
	plugins := make([]Plugin, 0)
	if err := yaml.Unmarshal(fileContent, &plugins); err != nil {
		return nil, oops.Wrapf(err, "can't parse yaml config")
	}
	return plugins, nil
}

func (p Plugin) SetActive(active bool) Plugin {
	return Plugin{
		Url:           p.Url,
		Crc:           p.Crc,
		Path:          p.Path,
		Branch:        p.Branch,
		glob:          p.glob,
		Name:          p.Name,
		Active:        active,
		Configuration: p.Configuration,
		content:       p.content,
		commiter:      p.commiter,
	}
}

func (p Plugin) Commiter() *user.Commiter {
	return p.commiter
}

func (p Plugin) DetermineNameFromUrl() Plugin {
	versions := strings.Split(p.Url, "/") // todo find a better way to have version
	name := versions[len(versions)-1]
	name = strings.Split(name, "-")[0]
	return Plugin{
		Url:           p.Url,
		Crc:           p.Crc,
		Path:          p.Path,
		Branch:        p.Branch,
		Name:          name,
		Active:        p.Active,
		Configuration: p.Configuration,
		content:       p.content,
		commiter:      p.commiter,
	}
}

func (p Plugin) Marshal() (string, error) {
	res := ""
	for i, m := range p.Configuration {
		for k, v := range m {
			s, err := p.serializeValue(v)
			if err != nil {
				return "", err
			}
			res += fmt.Sprintf("\"%s\"=\"%s\"_;_", k, s)
		}
		if i < len(p.Configuration)-1 {
			res += "_|_"
		}
	}
	return res, nil
}

func (p Plugin) serializeValue(v interface{}) (string, error) {
	switch x := v.(type) {
	case string:
		return x, nil
	case int:
		return strconv.Itoa(int(x)), nil
	case int8:
		return strconv.Itoa(int(x)), nil
	case int16:
		return strconv.Itoa(int(x)), nil
	case int32:
		return strconv.Itoa(int(x)), nil
	case int64:
		return strconv.Itoa(int(x)), nil
	case uint64:
		return strconv.Itoa(int(x)), nil
	case bool:
		if x {
			return "true", nil
		} else {
			return "false", nil
		}
	case nil:
		return "null", nil
	default:
		return "", oops.With("v", v).With("type", reflect.TypeOf(v).String()).Errorf("can't seralize value")
	}
}

func (m *Manager) PathWasm(p Plugin) string {
	pathPlugin := m.conf.GetDirPathDataPlugin(p.Name)
	versions := strings.Split(p.Url, "/") // TODO find a better way to have version
	version := versions[len(versions)-1]
	version = strings.Split(version, "-")[1]
	return fmt.Sprintf("%s/%s", pathPlugin, fmt.Sprintf("%s-%s", p.Name, version))
}

func (m *Manager) Copy(p Plugin) error {
	r, err := os.Open(p.Url)
	if err != nil {
		return err
	}
	defer r.Close()
	w, err := os.Create(m.PathWasm(p))
	if err != nil {
		return err
	}
	defer func() {
		if c := w.Close(); err == nil {
			err = c
		}
	}()
	_, err = io.Copy(w, r)
	return err
}
