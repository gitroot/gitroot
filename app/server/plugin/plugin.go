package plugin

import (
	"context"
	"errors"
	"os"

	"github.com/go-git/go-git/v5/plumbing/object"
	"github.com/goccy/go-yaml"
	"github.com/samber/oops"
	"gitroot.com/server/logger"
)

func (pm *Manager) reloadPlugins() {
	pm.pluginsLock.Lock()
	pm.plugins = nil
	pm.pluginsLock.Unlock()
}

func (pm *Manager) Availables(ctx context.Context) ([]Plugin, error) {
	pm.pluginsLock.RLock()
	needLoad := pm.plugins == nil
	pm.pluginsLock.RUnlock()
	if needLoad {
		errorHandler := oops.With("rootData", pm.conf.PathDataPlugin())
		forgeRepo, err := pm.repoManager.OpenForgeRepo(ctx)
		defer forgeRepo.Close()
		if err != nil {
			return nil, errorHandler.Wrapf(err, "can't open forgeRepo")
		}
		forgeRepo.RLock()
		filecontent, err := forgeRepo.ContentPluginsConf()
		if errors.Is(err, object.ErrFileNotFound) {
			return []Plugin{}, nil
		} else if err != nil {
			return nil, oops.Wrapf(err, "can't read plugins filecontent from repo")
		}
		forgeRepo.RUnlock()
		ps, err := Parse(filecontent)
		if err != nil {
			return nil, oops.Wrapf(err, "can't parse plugins from repo")
		}
		pm.pluginsLock.Lock()
		pm.plugins = make([]Plugin, len(ps))
		for i, cp := range ps {
			content, err := os.ReadFile(pm.PathWasm(cp))
			if err != nil {
				return nil, errorHandler.With("filename", cp.Name).Wrapf(err, "can't read plugin")
			}
			if cp.Name == "" {
				cp = cp.DetermineNameFromUrl()
			}
			if len(cp.Configuration) == 0 {
				cp.Configuration = make([]map[string]interface{}, 0)
				conf, err := pm.runtimes.Conf(ctx, Plugin{Name: cp.Name, content: content})
				if err != nil {
					pm.logger.Error("can't get default conf", err, logger.NewLoggerPair("filename", cp.Name))
				} else if err := yaml.Unmarshal(conf.configuration, &cp.Configuration); err != nil {
					pm.logger.Error("can't unmarshal default conf", err, logger.NewLoggerPair("filename", cp.Name), logger.NewLoggerPair("conf", conf))
				}
				cp.Path = string(conf.path)
				cp.Branch = make([]string, len(conf.branch))
				for i, b := range conf.branch {
					cp.Branch[i] = string(b)
				}
			}
			g, err := newGlob(cp.Path)
			if err != nil {
				pm.logger.Error("glob path is not parsable", err, logger.NewLoggerPair("pluginName", cp.Name), logger.NewLoggerPair("path", cp.Path))
			}
			commiter, err := pm.userManager.NewCommiter(cp.Name)
			if err != nil {
				pm.logger.Error("NewCommiter", err, logger.NewLoggerPair("pluginName", cp.Name), logger.NewLoggerPair("path", cp.Path))
			}

			pm.plugins[i] = Plugin{
				Path:          cp.Path,
				Branch:        cp.Branch,
				glob:          g,
				Url:           cp.Url,
				Crc:           cp.Crc,
				Active:        cp.Active,
				Configuration: cp.Configuration,
				Name:          cp.Name,
				content:       content,
				commiter:      commiter,
			}
			pm.logger.Info("available plugin", logger.NewLoggerPair("name", cp.Name))
		}
		pm.pluginsLock.Unlock()
	}
	pm.pluginsLock.RLock()
	defer pm.pluginsLock.RUnlock()
	if len(pm.plugins) == 0 {
		pm.logger.Info("no available plugin", logger.NewLoggerPair("in dir", pm.conf.PathDataPlugin()))
	}
	return pm.plugins, nil
}

func (pm *Manager) usable(ctx context.Context, repoName string) ([]Plugin, error) {
	repo, err := pm.repoManager.Open(ctx, repoName)
	if err != nil {
		return nil, oops.Wrapf(err, "can't open repo")
	}
	repo.RLock()
	filecontent, err := repo.ContentPluginsConf()
	if err != nil {
		repo.RUnlock()
		return nil, oops.Wrapf(err, "can't read plugins from repo")
	}
	repo.RUnlock()
	confPlugins, err := Parse(filecontent)
	if err != nil {
		return nil, oops.Wrapf(err, "can't parse plugins from repo")
	}
	if len(confPlugins) == 0 {
		pm.logger.Debug("no usable plugin in conf")
	}
	plugins, err := pm.Availables(ctx)
	if err != nil {
		return nil, oops.Wrapf(err, "can't get availables plugin from forgerepo")
	}
	usablePlugins := make([]Plugin, 0)
	for _, p := range plugins {
		for _, cp := range confPlugins {
			if cp.Active && p.Name == cp.Name {
				goodP := Plugin{ //TODO factorise plugin creation/copy...
					Url:           p.Url,
					Crc:           p.Crc,
					Path:          p.Path,
					Branch:        p.Branch,
					glob:          p.glob,
					Name:          p.Name,
					Active:        p.Active,
					Configuration: cp.Configuration,
					content:       p.content,
					commiter:      p.commiter,
				}
				usablePlugins = append(usablePlugins, goodP)
				pm.logger.Info("usable plugin", logger.NewLoggerPair("name", cp.Name))
				break
			}
		}
	}
	if len(usablePlugins) == 0 {
		pluginsLog := make([]Plugin, len(plugins))
		for i, p := range plugins {
			pluginsLog[i] = Plugin{Name: p.Name, Active: p.Active}
		}
		confPluginsLog := make([]Plugin, len(confPlugins))
		for i, p := range confPlugins {
			confPluginsLog[i] = Plugin{Name: p.Name, Active: p.Active}
		}
		pm.logger.Info("no usable plugin", logger.NewLoggerPair("availables", pluginsLog), logger.NewLoggerPair("conf", confPluginsLog))
	}
	return usablePlugins, nil
}
