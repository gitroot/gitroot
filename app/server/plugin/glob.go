package plugin

import "regexp"

// inspired by https://github.com/golang/go/issues/11862#issuecomment-1517792571

var replaces = regexp.MustCompile(`(\.)|(\*\*\/)|(\*)|([^\/\*]+)|(\/)`)

func toRegexp(pattern string) string {
	pat := replaces.ReplaceAllStringFunc(pattern, func(s string) string {
		switch s {
		case "/":
			return "\\/"
		case ".":
			return "\\."
		case "**/":
			return ".*"
		case "*":
			return "[^/]*"
		default:
			return s
		}
	})
	return "^" + pat + "$"
}

type glob struct {
	reg *regexp.Regexp
}

func newGlob(path string) (*glob, error) {
	reg, err := regexp.Compile(toRegexp(path))
	if err != nil {
		return nil, err
	}
	return &glob{
		reg: reg,
	}, nil
}

func (g *glob) Match(path string) bool {
	return g.reg.MatchString(path)
}
