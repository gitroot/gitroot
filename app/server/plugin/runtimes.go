package plugin

import (
	"context"
	"fmt"
	"sync"

	"gitroot.com/server/logger"
)

type runtimes struct {
	lockRuntime sync.Mutex
	runtime     *runtime
	log         *logger.Logger
	start       chan runtimeInputs
}

func newRuntimes(manager *Manager, log *logger.Logger) *runtimes {
	chanStart := make(chan runtimeInputs)
	r, err := newRuntime(context.Background(), manager, log)
	if err != nil {
		log.Error("can't create runtime", err)
	}
	go func() {
		r.listen(chanStart)
	}()
	return &runtimes{
		runtime: r,
		log:     log,
		start:   chanStart,
	}
}

func (r *runtimes) Start(ctx context.Context, repoName string, plugins []Plugin, kind runtimeInputsKind, commands []CommandForDiff) {
	r.lockRuntime.Lock()
	defer r.lockRuntime.Unlock()
	r.start <- runtimeInputs{
		ctx:      ctx,
		repoName: repoName,
		kind:     kind,
		plugins:  plugins,
		commands: commands,
	}
}

func (r *runtimes) Conf(ctx context.Context, plugin Plugin) (*defaultConf, error) {
	timerStop := r.log.Time(fmt.Sprintf("Timer conf plugin %s", plugin.Name))
	defer timerStop()
	r.lockRuntime.Lock()
	defer r.lockRuntime.Unlock()
	return r.runtime.conf(ctx, plugin)
}
