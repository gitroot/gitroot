package plugin

import (
	"context"
	"io/fs"
	"sync"

	"github.com/samber/oops"
	"gitroot.com/server/logger"
	"gitroot.com/server/repository"
	"gitroot.com/server/user"
)

type Manager struct {
	logger      *logger.Logger
	conf        needConf
	repoManager needRepo
	userManager needUser

	plugins     []Plugin
	pluginsLock sync.RWMutex
	runtimes    *runtimes
}

type needConf interface {
	GetDirPathDataPlugin(pluginName string) string
	PathDataPlugin() string
	PathFilePlugins() string
	DataWeb(repoName string) fs.FS
	PathCache() string
	Cache(repoName string, pluginName string) fs.FS
}

type needRepo interface {
	OpenForgeRepo(ctx context.Context) (*repository.GitRootRepository, error)
	Open(ctx context.Context, repoName string) (*repository.GitRootRepository, error)
}

type needUser interface {
	NewCommiter(pseudo string) (*user.Commiter, error)
}

func NewManager(conf needConf, repoManager needRepo, userManager needUser) *Manager {
	log := logger.NewLogger(logger.PLUGIN_MANAGER)
	m := &Manager{
		logger:      log,
		conf:        conf,
		repoManager: repoManager,
		userManager: userManager,

		plugins:     nil,
		pluginsLock: sync.RWMutex{},
	}
	m.runtimes = newRuntimes(m, log)
	return m
}

func (m *Manager) Run(ctx context.Context, repoName string, commands []CommandForDiff) error {
	if len(commands) > 0 {
		plugins, err := m.usable(ctx, repoName)
		if err != nil {
			return err
		}

		// TODO rework plugin start/activation
		// for each command (branch) we have a list of plugins to start/activate
		// maybe cut commands into one command previously in code
		// and so run plugins/commits for one cmd at a time

		pluginsActivated, err := m.checkPluginActivation(ctx, repoName, commands)
		if err != nil {
			return err
		}

		if len(plugins) > 0 {
			pluginsAlreadyPresent := make([]Plugin, 0)
			toActivatePlugin := make([]Plugin, 0)
			for _, p := range plugins {
				found := false
				// var cpa commandPluginActivation
				for _, pp := range pluginsActivated {
					for _, ppp := range pp.plugins {
						if p.Name == ppp.Name {
							found = true
							break
						}
					}
					if found {
						break
					}
				}
				if !found {
					pluginsAlreadyPresent = append(pluginsAlreadyPresent, p)
				} else {
					toActivatePlugin = append(toActivatePlugin, p)
				}
			}
			if len(pluginsAlreadyPresent) > 0 {
				m.runtimes.Start(ctx, repoName, pluginsAlreadyPresent, runtimeInputsKindDiff, commands)
			}
			if len(toActivatePlugin) > 0 {
				m.runtimes.Start(ctx, repoName, toActivatePlugin, runtimeInputsKindWorktree, commands)
			}
		}
	}
	return nil
}

type commandPluginActivation struct {
	command CommandForDiff
	plugins []Plugin
}

func (m *Manager) checkPluginActivation(ctx context.Context, repoName string, commands []CommandForDiff) ([]commandPluginActivation, error) {
	pluginsActivated := make([]commandPluginActivation, 0)
	if len(commands) == 0 {
		return pluginsActivated, nil
	}
	repo, err := m.repoManager.Open(ctx, repoName)
	if err != nil {
		return nil, oops.Wrapf(err, "can't open repo")
	}
	repo.RLock()
	defer repo.RUnlock()

	for _, cmd := range commands {
		if !cmd.IsFileTouched(cmd.branch, m.conf.PathFilePlugins()) || len(cmd.commits) == 0 {
			continue
		}

		oldFilecontent, err := repo.ContentPluginsConfAtHash(cmd.commits[0].parent.Hash)
		if err != nil {
			return nil, oops.With("repo", repoName, "hash", cmd.commits[0].parent.Hash.String()).Wrapf(err, "can't get repo plugin conf at hash")
		}
		oldConfPlugins, err := Parse(oldFilecontent)
		if err != nil {
			return nil, oops.With("repo", repoName, "hash", cmd.commits[0].parent.Hash.String()).Wrapf(err, "can't parse repo plugin oldconf")
		}

		newFilecontent, err := repo.ContentPluginsConfAtRef(cmd.branch)
		if err != nil {
			return nil, oops.With("repo", repoName, "ref", cmd.branch.Short()).Wrapf(err, "can't get repo plugin conf at ref")
		}
		newConfPlugins, err := Parse(newFilecontent)
		if err != nil {
			return nil, oops.With("repo", repoName, "Branch", cmd.branch).Wrapf(err, "can't parse repo plugin newconf")
		}

		plugins := make([]Plugin, 0)

		for _, oldConfPlugin := range oldConfPlugins {
			for _, newConfPlugin := range newConfPlugins {
				if oldConfPlugin.Name == newConfPlugin.Name {
					if !oldConfPlugin.Active {
						plugins = append(plugins, newConfPlugin)
						break
					}
				}
			}
		}

		if len(plugins) > 0 {
			pluginsActivated = append(pluginsActivated, commandPluginActivation{command: cmd, plugins: plugins})
		}
	}
	return pluginsActivated, nil
}
