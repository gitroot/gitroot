package plugin

import (
	"context"

	"github.com/samber/oops"
	"github.com/tetratelabs/wazero/api"
	"gitroot.com/server/logger"
	"gitroot.com/server/repository"
)

type callPlugin struct {
	manager *Manager
	plugin  Plugin
	repo    *repository.GitRootRepository
	module  api.Module
	logger  *logger.Logger
}

func (c callPlugin) callPluginForDiff(ctx context.Context, r *runtime, commands []CommandForDiff) error {
	startCommit := c.module.ExportedFunction("startCommit")
	addFile := c.module.ExportedFunction("addFile")
	modFile := c.module.ExportedFunction("modFile")
	delFile := c.module.ExportedFunction("delFile")
	renameFile := c.module.ExportedFunction("renameFile")
	endCommit := c.module.ExportedFunction("endCommit")
	malloc := c.module.ExportedFunction("malloc")
	free := c.module.ExportedFunction("free")

	currentBranch, err := c.repo.CurrentBranch()
	if err != nil {
		return oops.Wrapf(err, "can't get CurrentBranch")
	}

	if init := c.module.ExportedFunction("init"); init != nil {
		arg, err := c.plugin.Marshal()
		if err != nil {
			return err
		}
		c.logger.Debug("init plugin", logger.NewLoggerPair("name", c.plugin.Name), logger.NewLoggerPair("arg", arg))
		if err := writeMemoryAndCall(ctx, c.module, init, malloc, free, c.repo.Name(), arg); err != nil {
			return err
		}
	}

	for _, cmd := range commands {
		r.command = &cmd
		isAuthorized := true
		for _, b := range c.plugin.Branch {
			if b == "*" {
				isAuthorized = true
				break
			}
			if cmd.branch.Short() != b {
				isAuthorized = false
			}
		}
		if !isAuthorized || len(c.plugin.Branch) == 0 {
			continue
		}

		if cmd.branchAction == commitForDiffActionDel {
			c.logger.Info("delete branch", logger.NewLoggerPair("branch", cmd.branch))
			continue
		}

		if err := c.repo.Checkout(cmd.branch); err != nil {
			c.logger.Error("can't Checkout", err, logger.NewLoggerPair("branch", cmd.branch))
			continue
		}

		for _, com := range cmd.commits {
			r.commit = &com
			if startCommit != nil {
				if err := writeMemoryAndCall(ctx, c.module, startCommit, malloc, free, cmd.branch.Short(), com.hash.String(), com.message, cmd.pusher.Ssh); err != nil {
					return err
				}
			}

			for _, f := range com.files {
				if f.action == commitForDiffActionAdd && addFile != nil {
					c.logger.Debug("add", logger.NewLoggerPair("confPath", c.plugin.Path), logger.NewLoggerPair("currentPath", f.path))
					if c.plugin.glob.Match(f.path) {
						// creation
						writeMemoryAndCall(ctx, c.module, addFile, malloc, free, f.path)
					}
				} else {
					if c.plugin.glob.Match(f.path) {
						if f.action == commitForDiffActionDel && delFile != nil {
							// deletion
							writeMemoryAndCall(ctx, c.module, delFile, malloc, free, f.path)
						} else if f.action == commitForDiffActionMod && modFile != nil {
							//modification
							writeMemoryAndCall(ctx, c.module, modFile, malloc, free, f.path)
						} else if f.action == commitForDiffActionRename && renameFile != nil {
							//rename
							writeMemoryAndCall(ctx, c.module, renameFile, malloc, free, f.path, f.oldPath)
						}
					}
				}
			}

			if err := writeMemoryAndCall(ctx, c.module, endCommit, malloc, free, cmd.branch.Short(), com.hash.String(), com.message); err != nil {
				return err
			}
		}

		if finish := c.module.ExportedFunction("finish"); finish != nil {
			if _, err := finish.Call(ctx); err != nil {
				return err
			}
		}
	}

	if err := c.repo.Checkout(currentBranch); err != nil {
		c.logger.Error("can't Checkout", err, logger.NewLoggerPair("branch", currentBranch.Short()))
		return err
	}
	return nil
}

func writeMemoryAndCall(ctx context.Context, module api.Module, toCall api.Function, malloc api.Function, free api.Function, message ...string) error {
	params := make([]uint64, 0)
	for _, m := range message {
		size := uint64(len(m))

		results, err := malloc.Call(ctx, size)
		if err != nil {
			return oops.Errorf("can't malloc memory")
		}
		ptr := results[0]

		// The pointer is a linear memory offset, which is where we write the name.
		if !module.Memory().Write(uint32(ptr), []byte(m)) {
			return oops.Errorf("can't write memory")
		}

		params = append(params, ptr, size)
	}

	if _, err := toCall.Call(ctx, params...); err != nil {
		return err
	}
	for i, d := range params {
		if i%2 == 0 {
			free.Call(ctx, d)
		}
	}
	return nil
}
