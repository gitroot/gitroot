package plugin

import (
	"bytes"
	"context"

	"github.com/samber/oops"
	"github.com/tetratelabs/wazero/api"
	"gitroot.com/server/logger"
)

type defaultConf struct {
	configuration []byte
	path          []byte
	branch        [][]byte
}

func callPluginForConf(ctx context.Context, module api.Module, logger *logger.Logger) (*defaultConf, error) {
	defaultConf := &defaultConf{configuration: []byte(""), path: []byte(""), branch: [][]byte{}}

	free := module.ExportedFunction("free")
	if defaultConfiguration := module.ExportedFunction("defaultConfiguration"); defaultConfiguration != nil {
		ptrSize, err := defaultConfiguration.Call(ctx)
		if err != nil {
			return nil, oops.Wrapf(err, "defaultConfiguration call")
		}
		defaultConfigurationPtr := uint32(ptrSize[0] >> 32)
		defaultConfigurationSize := uint32(ptrSize[0])
		if defaultConfigurationPtr != 0 {
			defer func() {
				_, err := free.Call(ctx, uint64(defaultConfigurationPtr))
				if err != nil {
					logger.Error("can't free defaultConfiguration", err)
				}
			}()
		}
		res, ok := module.Memory().Read(defaultConfigurationPtr, defaultConfigurationSize)
		if !ok {
			return nil, oops.Errorf("can't read memory")
		}
		defaultConf.configuration = res
	}
	if defaultPath := module.ExportedFunction("defaultPath"); defaultPath != nil {
		ptrSize, err := defaultPath.Call(ctx)
		if err != nil {
			return nil, oops.Wrapf(err, "defaultPath call")
		}
		defaultPathPtr := uint32(ptrSize[0] >> 32)
		defaultPathSize := uint32(ptrSize[0])
		if defaultPathPtr != 0 {
			defer func() {
				_, err := free.Call(ctx, uint64(defaultPathPtr))
				if err != nil {
					logger.Error("can't free defaultPath", err)
				}
			}()
		}
		res, ok := module.Memory().Read(defaultPathPtr, defaultPathSize)
		if !ok {
			return nil, oops.Errorf("can't read memory")
		}
		defaultConf.path = res
	}
	if defaultBranch := module.ExportedFunction("defaultBranch"); defaultBranch != nil {
		ptrSize, err := defaultBranch.Call(ctx)
		if err != nil {
			return nil, oops.Wrapf(err, "defaultBranch call")
		}
		defaultBranchPtr := uint32(ptrSize[0] >> 32)
		defaultBranchSize := uint32(ptrSize[0])
		if defaultBranchPtr != 0 {
			defer func() {
				_, err := free.Call(ctx, uint64(defaultBranchPtr))
				if err != nil {
					logger.Error("can't free defaultPath", err)
				}
			}()
		}
		res, ok := module.Memory().Read(defaultBranchPtr, defaultBranchSize)
		if !ok {
			return nil, oops.Errorf("can't read memory")
		}
		defaultConf.branch = bytes.Split(res, []byte(";"))
	}
	return defaultConf, nil
}
