package plugin

import (
	"context"
	"errors"
	"fmt"
	"os"
	"path/filepath"

	"github.com/go-git/go-git/v5/plumbing"
	"github.com/samber/oops"
	"github.com/tetratelabs/wazero"
	"github.com/tetratelabs/wazero/api"
	"github.com/tetratelabs/wazero/imports/wasi_snapshot_preview1"
	"github.com/tetratelabs/wazero/sys"
	grfs "gitroot.com/server/fs"
	"gitroot.com/server/logger"
	"gitroot.com/server/repository"
)

type runtimeInputsKind int

const (
	runtimeInputsKindDiff     runtimeInputsKind = iota
	runtimeInputsKindWorktree runtimeInputsKind = iota
)

type runtimeInputs struct {
	ctx      context.Context
	repoName string
	kind     runtimeInputsKind
	plugins  []Plugin
	commands []CommandForDiff
}

type runtime struct {
	ctx          context.Context
	manager      *Manager
	logger       *logger.Logger
	repo         *repository.GitRootRepository
	plugin       Plugin
	command      *CommandForDiff
	commit       *commitForDiffCommit
	wazRun       wazero.Runtime
	fsWorktree   grfs.UpdatableFs
	fsWebcontent grfs.UpdatableFs
	fsCache      grfs.UpdatableFs
}

func newRuntime(ctx context.Context, manager *Manager, logger *logger.Logger) (*runtime, error) {
	r := &runtime{
		ctx:          ctx,
		manager:      manager,
		logger:       logger,
		wazRun:       wazero.NewRuntime(ctx),
		fsWorktree:   grfs.NewUpdatableFs(ctx, nil),
		fsWebcontent: grfs.NewUpdatableFs(ctx, nil),
		fsCache:      grfs.NewUpdatableFs(ctx, nil),
	}
	_, err := r.wazRun.
		NewHostModuleBuilder("gitroot").
		NewFunctionBuilder().WithFunc(r.ModifyContent).Export("modifyContent").
		NewFunctionBuilder().WithFunc(r.ModifyWebContent).Export("modifyWebContent").
		NewFunctionBuilder().WithFunc(r.ModifyCacheContent).Export("modifyCacheContent").
		NewFunctionBuilder().WithFunc(r.CommitAll).Export("commitAll").
		NewFunctionBuilder().WithFunc(r.DiffWithParent).Export("diffWithParent").
		NewFunctionBuilder().WithFunc(r.Log).Export("log").
		NewFunctionBuilder().WithFunc(r.LogError).Export("logError").
		NewFunctionBuilder().WithFunc(r.Merge).Export("merge").
		Instantiate(ctx)
	if err != nil {
		return nil, err
	}
	_, err = wasi_snapshot_preview1.Instantiate(ctx, r.wazRun)
	if err != nil {
		return nil, err
	}
	return r, nil
}

func (r *runtime) listen(c chan runtimeInputs) {
	for i := range c {
		repo, err := r.manager.repoManager.Open(r.ctx, i.repoName)
		if err != nil {
			r.logger.Error("open error in listen", err)
			continue
		}
		repo.WLock()
		if i.kind == runtimeInputsKindDiff {
			timerStop := r.logger.Time(fmt.Sprintf("Timer %s all plugins", repo.Name()))
			if err := r.start(i.ctx, repo, i.plugins, i.commands); err != nil {
				r.logger.Error("start error", err)
			}
			timerStop()
		} else if i.kind == runtimeInputsKindWorktree {
			r.logger.Info("start worktree", logger.NewLoggerPair("repo", repo.Name()))
			timerStop := r.logger.Time(fmt.Sprintf("Timer %s worktree", repo.Name()))
			if err := r.worktree(i.ctx, repo, i.plugins, i.commands); err != nil {
				r.logger.Error("start error", err)
			}
			timerStop()
		}
		repo.WUnlock()
	}
}

func (r *runtime) conf(ctx context.Context, plugin Plugin) (*defaultConf, error) {
	r.fsWorktree.Clear()
	r.fsWebcontent.Clear()
	r.fsCache.Clear()
	r.repo = nil
	r.plugin = plugin
	r.command = nil
	r.commit = nil
	timerStop := r.logger.Time(fmt.Sprintf("Timer %s", plugin.Name))

	r.logger.Debug("start plugin conf", logger.NewLoggerPair("name", plugin.Name))
	m := r.wazRun.Module(plugin.Name)
	if m == nil {
		r.logger.Debug("instantiate plugin conf", logger.NewLoggerPair("name", plugin.Name))
		config := wazero.NewModuleConfig().
			WithStdout(os.Stdout).WithStderr(os.Stderr).
			WithFSConfig(
				wazero.NewFSConfig().
					WithFSMount(r.fsWorktree, "worktree").
					WithFSMount(r.fsWebcontent, "webcontent").
					WithFSMount(r.fsCache, "cache"))
		mod, err := r.wazRun.InstantiateWithConfig(ctx, plugin.content, config.WithName(plugin.Name).WithStartFunctions("_initialize", "install"))
		if err != nil {
			if exitErr, ok := err.(*sys.ExitError); ok && exitErr.ExitCode() != 0 {
				fmt.Fprintf(os.Stderr, "exit_code: %d\n", exitErr.ExitCode())
				return nil, err
			} else if !ok {
				return nil, err
			}
		}
		m = mod
	} else {
		r.logger.Debug("already exist conf", logger.NewLoggerPair("name", plugin.Name))
	}
	conf, err := callPluginForConf(ctx, m, r.logger.NewSubLogger(plugin.Name))
	if err != nil {
		r.logger.Error("finish plugin conf with error", err, logger.NewLoggerPair("name", plugin.Name))
	}

	r.logger.Debug("finish plugin conf", logger.NewLoggerPair("name", plugin.Name))
	timerStop()
	return conf, err
}

func (r *runtime) start(ctx context.Context, repo *repository.GitRootRepository, plugins []Plugin, commands []CommandForDiff) error {
	r.fsWorktree.Update(repo.ToFs(ctx))
	r.fsWebcontent.Update(r.manager.conf.DataWeb(repo.Name()))

	r.repo = repo
	r.command = nil
	r.commit = nil
	for _, plugin := range plugins {
		r.fsCache.Update(r.manager.conf.Cache(repo.Name(), plugin.Name))
		r.plugin = plugin
		timerStop := r.logger.Time(fmt.Sprintf("Timer %s", plugin.Name))
		r.logger.Debug("start plugin", logger.NewLoggerPair("name", plugin.Name))
		m := r.wazRun.Module(plugin.Name)
		if m == nil {
			r.logger.Debug("instantiate plugin", logger.NewLoggerPair("name", plugin.Name))
			config := wazero.NewModuleConfig().
				WithStdout(os.Stdout).WithStderr(os.Stderr).
				WithFSConfig(wazero.NewFSConfig().
					WithFSMount(r.fsWorktree, "worktree").
					WithFSMount(r.fsWebcontent, "webcontent").
					WithFSMount(r.fsCache, "cache"))
			mod, err := r.wazRun.InstantiateWithConfig(ctx, plugin.content, config.WithName(plugin.Name).WithStartFunctions("_initialize", "install"))
			if err != nil {
				if exitErr, ok := err.(*sys.ExitError); ok && exitErr.ExitCode() != 0 {
					fmt.Fprintf(os.Stderr, "exit_code: %d\n", exitErr.ExitCode())
					return nil
				} else if !ok {
					return err
				}
			}
			m = mod
		} else {
			r.logger.Debug("already exist", logger.NewLoggerPair("name", plugin.Name))
		}
		cp := callPlugin{
			manager: r.manager,
			plugin:  plugin,
			repo:    repo,
			module:  m,
			logger:  r.logger.NewSubLogger(plugin.Name),
		}
		if err := cp.callPluginForDiff(ctx, r, commands); err != nil {
			r.logger.Error("finish plugin with error", err, logger.NewLoggerPair("name", plugin.Name))
		}

		r.logger.Debug("finish plugin", logger.NewLoggerPair("name", plugin.Name))
		timerStop()
	}
	return nil
}

func (r *runtime) ModifyContent(_ context.Context, m api.Module, offset, byteCount, offset2, byteCount2 uint32) {
	buf, ok := m.Memory().Read(offset, byteCount)
	if !ok {
		r.logger.Error("modifyContent can't read filepath", errors.New("Memory.Read out of range"), logger.NewLoggerPair("offset", offset), logger.NewLoggerPair("byteCount", byteCount))
		return
	}
	buf2, ok2 := m.Memory().Read(offset2, byteCount2)
	if !ok2 {
		r.logger.Error("modifyContent can't read file content", errors.New("Memory.Read out of range"), logger.NewLoggerPair("offset2", offset2), logger.NewLoggerPair("byteCount2", byteCount2))
		return
	}
	tmp := make([]byte, byteCount2)
	copy(tmp, buf2)
	r.logger.Debug("modifyContent", logger.NewLoggerPair("file", string(buf)))
	if err := r.repo.Write(string(buf), tmp); err != nil {
		r.logger.Error("modifyContent can't open file", err, logger.NewLoggerPair("filepath", string(buf)))
		return
	}
}

func (r *runtime) ModifyWebContent(_ context.Context, m api.Module, offset, byteCount, offset2, byteCount2 uint32) {
	buf, ok := m.Memory().Read(offset, byteCount)
	if !ok {
		r.logger.Error("modifyWebContent can't read filepath", errors.New("Memory.Read out of range"), logger.NewLoggerPair("offset", offset), logger.NewLoggerPair("byteCount", byteCount))
		return
	}
	buf2, ok2 := m.Memory().Read(offset2, byteCount2)
	if !ok2 {
		r.logger.Error("modifyWebContent can't read file content", errors.New("Memory.Read out of range"), logger.NewLoggerPair("offset2", offset2), logger.NewLoggerPair("byteCount2", byteCount2))
		return
	}
	fullPath := r.repo.PathDataWeb(string(buf))
	dir, _ := filepath.Split(fullPath)
	if err := os.MkdirAll(dir, os.ModePerm); err != nil {
		r.logger.Error("modifyWebContent can't mkdirAll", err, logger.NewLoggerPair("filepath", string(buf)))
		return
	}
	if err := os.WriteFile(fullPath, buf2, 0666); err != nil {
		r.logger.Error("modifyWebContent can't open file", err, logger.NewLoggerPair("filepath", string(buf)))
		return
	}
	r.logger.Debug("Write in web", logger.NewLoggerPair("fullPath", fullPath))
}

func (r *runtime) ModifyCacheContent(_ context.Context, m api.Module, offset, byteCount, offset2, byteCount2 uint32) {
	buf, ok := m.Memory().Read(offset, byteCount)
	if !ok {
		r.logger.Error("ModifyCacheContent can't read filepath", errors.New("Memory.Read out of range"), logger.NewLoggerPair("offset", offset), logger.NewLoggerPair("byteCount", byteCount))
		return
	}
	buf2, ok2 := m.Memory().Read(offset2, byteCount2)
	if !ok2 {
		r.logger.Error("ModifyCacheContent can't read file content", errors.New("Memory.Read out of range"), logger.NewLoggerPair("offset2", offset2), logger.NewLoggerPair("byteCount2", byteCount2))
		return
	}
	fullPath := filepath.Join(r.manager.conf.PathCache(), r.repo.Name(), r.plugin.Name, string(buf))
	dir, _ := filepath.Split(fullPath)
	if err := os.MkdirAll(dir, os.ModePerm); err != nil {
		r.logger.Error("ModifyCacheContent can't mkdirAll", err, logger.NewLoggerPair("filepath", string(buf)))
		return
	}
	if err := os.WriteFile(fullPath, buf2, 0666); err != nil {
		r.logger.Error("ModifyCacheContent can't open file", err, logger.NewLoggerPair("filepath", string(buf)))
		return
	}
	r.logger.Debug("Write in cache", logger.NewLoggerPair("fullPath", fullPath), logger.NewLoggerPair("content", string(buf2)))
}

func (r *runtime) CommitAll(_ context.Context, m api.Module, offset, byteCount uint32) {
	buf, ok := m.Memory().Read(offset, byteCount)
	if !ok {
		r.logger.Error("commitAll can't read message", errors.New("Memory.Read out of range"), logger.NewLoggerPair("offset", offset), logger.NewLoggerPair("byteCount", byteCount))
		return
	}

	if h, err := r.repo.CommitAll(string(buf), r.plugin.commiter); err != nil {
		r.logger.Error("commitAll can't commit", err)
		return
	} else {
		r.logger.Debug("Commit", logger.NewLoggerPair("message", string(buf)), logger.NewLoggerPair("hash", h.String()))
	}
}

func (r *runtime) DiffWithParent(ctx context.Context, m api.Module, filePtr, fileSize, hashPtr, hashSize, resPtr uint32) (resSize uint32) {
	r.logger.Debug("In diffWithParent server side")
	file, ok := m.Memory().Read(filePtr, fileSize)
	if !ok {
		r.logger.Error("diffWithParent can't read file", errors.New("Memory.Read out of range"), logger.NewLoggerPair("ptr", filePtr), logger.NewLoggerPair("size", fileSize))
		return
	}
	hash, ok := m.Memory().Read(hashPtr, hashSize)
	if !ok {
		r.logger.Error("diffWithParent can't read hash", errors.New("Memory.Read out of range"), logger.NewLoggerPair("ptr", hashPtr), logger.NewLoggerPair("size", hashSize))
		return
	}
	r.logger.Info("In diffWithParent before found ancestor")
	diffStr, err := r.repo.GetDiff(plumbing.NewHash(string(hash)), string(file))
	if err != nil {
		r.logger.Error("GetDiff", err)
		return
	}
	r.logger.Debug("In diffWithParent send diff", logger.NewLoggerPair("diff", diffStr))
	size, err := sendData(m, diffStr, resPtr)
	if err != nil {
		r.logger.Error("can't sendData", err, logger.NewLoggerPair("message", diffStr))
		return 0
	}
	return size
}

func (r *runtime) Log(_ context.Context, m api.Module, offset, byteCount uint32) {
	buf, ok := m.Memory().Read(offset, byteCount)
	if !ok {
		r.logger.Error("log can't read message", errors.New("Memory.Read out of range"), logger.NewLoggerPair("offset", offset), logger.NewLoggerPair("byteCount", byteCount))
		return
	}
	r.logger.Debug(string(buf), logger.NewLoggerPair("repo", r.repo.Name()), logger.NewLoggerPair("plugin", r.plugin.Name))
}

func (r *runtime) LogError(_ context.Context, m api.Module, offset, byteCount, errPtr, errSize uint32) {
	buf, ok := m.Memory().Read(offset, byteCount)
	if !ok {
		r.logger.Error("logError can't read message", errors.New("Memory.Read out of range"), logger.NewLoggerPair("offset", offset), logger.NewLoggerPair("byteCount", byteCount))
		return
	}
	bufErr, ok2 := m.Memory().Read(errPtr, errSize)
	if !ok2 {
		r.logger.Error("logError can't read message", errors.New("Memory.Read out of range err"), logger.NewLoggerPair("errPtr", errPtr), logger.NewLoggerPair("errSize", errSize))
		return
	}
	r.logger.Error(string(buf), errors.New(string(bufErr)), logger.NewLoggerPair("repo", r.repo.Name()), logger.NewLoggerPair("plugin", r.plugin.Name))
}

func (r *runtime) Merge(_ context.Context, m api.Module, fromPtr, fromSize, toPtr, toSize uint32) {
	from, ok := m.Memory().Read(fromPtr, fromSize)
	if !ok {
		r.logger.Error("merge can't read message", errors.New("Memory.Read out of range"), logger.NewLoggerPair("fromPtr", fromPtr), logger.NewLoggerPair("fromSize", fromSize))
		return
	}
	to, ok := m.Memory().Read(toPtr, toSize)
	if !ok {
		r.logger.Error("merge can't read message", errors.New("Memory.Read out of range"), logger.NewLoggerPair("toPtr", toPtr), logger.NewLoggerPair("toSize", toSize))
		return
	}
	r.logger.Debug("try to merge", logger.NewLoggerPair("from", string(from)), logger.NewLoggerPair("to", string(to)))
	if err := r.repo.Merge(string(from), string(to), r.plugin.commiter, r.command.pusher); err != nil {
		r.logger.Error("can't Merge", err, logger.NewLoggerPair("from", string(from)), logger.NewLoggerPair("to", string(to)))
	}
}

func (r *runtime) Close() error {
	return r.wazRun.Close(r.ctx)
}

func sendData(module api.Module, message string, ptr uint32) (size uint32, err error) {
	s := uint32(len(message))

	// The pointer is a linear memory offset, which is where we write the name.
	if !module.Memory().WriteString(ptr, message) {
		return 0, oops.Errorf("can't write memory")
	}
	return s, nil
}
