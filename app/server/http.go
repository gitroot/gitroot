package main

import (
	"context"
	_ "embed"
	"fmt"
	"io/fs"
	"net/http"
	"strings"

	"gitroot.com/server/configuration"
	"gitroot.com/server/logger"
	"gitroot.com/server/repository"
)

const index = `<html>
<head>
<title>%s</title>
<link rel="stylesheet" href="/rootstyle.css">
</head>
<body>
<header>
	<h1>%s</h1>
</header>
%s
<footer>
	<p>🚀 git clone <code>ssh://%s/%s</code></p>
	<small>Hosted with ❤️ by Gitroot</small>
</footer>
</body>
</html>`

const defaultIndex = `<p>🚀 git clone <code>ssh://%s/%s</code> to begin!</p>
<article>📖 Need help to configure your repository? 
<ul>
	<li><a href=\"\">doc</a></li>
	<li><a href=\"\">web configuration</a></li>
</ul>
</article>`

//go:embed resources/styles/simple.min.css
var simpleStyle []byte

type httpServer struct {
	logger      *logger.Logger
	conf        *configuration.Configuration
	repoManager *repository.Manager
}

func NewServerHttp(conf *configuration.Configuration, repoManager *repository.Manager) *httpServer {
	return &httpServer{
		logger:      logger.NewLoggerCtx(logger.HTTP_SERVER_LOGGER_NAME, context.Background()),
		conf:        conf,
		repoManager: repoManager,
	}
}

func (srv *httpServer) ListenAndServe() error {
	http.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {
		// / --> root+index.html
		// /notRepo --> root+notRepo
		// /repo1 --> repo1+index.html
		// /repo1/2.html --> repo1+2.html
		repoName := srv.conf.ForgeConfigName()
		file := ""
		if r.URL.Path != "/" {
			url := strings.TrimPrefix(r.URL.Path, "/")
			paths := strings.Split(url, "/")
			if srv.repoManager.Exists(paths[0]) {
				repoName = paths[0]
				file = strings.Join(paths[1:], "/")
			} else {
				repoName = srv.conf.ForgeConfigName()
				file = url
			}
		}

		if file == "" {
			file = "index.html"
		} else if strings.HasSuffix(file, "/") {
			file = file + "index.html"
		}

		srv.logger.Info("serve", logger.NewLoggerPair("repo", repoName), logger.NewLoggerPair("file", file))

		fsDataWeb := srv.conf.DataWeb(repoName)
		_, err := fs.Stat(fsDataWeb, file)
		if err == nil {
			http.ServeFileFS(w, r, fsDataWeb, file)
			return
		}

		repo, err := srv.repoManager.Open(r.Context(), repoName)
		if err != nil {
			srv.logger.Error("invalid repo", err, logger.NewLoggerPair("repoName", repoName))
			srv.error(w, repoName)
			return
		}
		repo.RLock()
		defer repo.RUnlock()
		defer repo.Close()

		fsRepo := repo.ToFs(r.Context())
		_, err = fs.Stat(fsRepo, file)
		if err == nil {
			http.ServeFileFS(w, r, fsRepo, file)
			return
		}

		if r.URL.Path == "/rootstyle.css" {
			w.Write(simpleStyle)
			return
		}

		if strings.HasSuffix(file, "index.html") {
			srv.index(w, repoName)
		} else {
			srv.notFound(w, repoName)
		}
	})
	srv.logger.Warn("starting HTTP server on", logger.NewLoggerPair("addr", srv.conf.HttpAddr))
	return http.ListenAndServe(srv.conf.HttpAddr, nil)
}

func (srv *httpServer) index(w http.ResponseWriter, repoName string) {
	content := fmt.Sprintf(defaultIndex, srv.conf.SshAddr, repoName)
	fmt.Fprintf(w, index, repoName, repoName, content, srv.conf.SshAddr, repoName)
}

func (srv *httpServer) notFound(w http.ResponseWriter, repoName string) {
	w.WriteHeader(http.StatusNotFound)
	fmt.Fprintf(w, index, repoName, repoName, "<p>Not found</p>", srv.conf.SshAddr, repoName)
}

func (srv *httpServer) error(w http.ResponseWriter, repoName string) {
	w.WriteHeader(http.StatusInternalServerError)
	fmt.Fprintf(w, index, repoName, repoName, "<p>Error</p>", srv.conf.SshAddr, repoName)
}
