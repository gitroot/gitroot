package main

import (
	"context"
	"flag"
	"os"
	"time"

	"gitroot.com/server/background"
	"gitroot.com/server/configuration"
	"gitroot.com/server/logger"
	"gitroot.com/server/plugin"
	"gitroot.com/server/repository"
	"gitroot.com/server/user"
)

func main() {
	log := logger.NewLogger("root")
	var cfg string
	flag.StringVar(&cfg, "data", "./data", "path to store data")
	flag.Parse()

	conf := configuration.NewConfiguration(cfg)

	userManager, err := user.NewManager(conf)
	if err != nil {
		log.Error("Initialisation failed", err)
		os.Exit(1)
	}

	var backgroundManager *background.Manager
	repoManager := repository.NewManager(context.Background(), conf, userManager)
	pluginManager := plugin.NewManager(conf, repoManager, userManager)
	backgroundManager = background.NewManager(context.Background(), conf, repoManager, userManager, pluginManager)
	repoManager.SetBackgroundManager(backgroundManager)

	go func() {
		time.Sleep(10 * time.Millisecond)
		if err := repoManager.CreateRootRepoIfNeeded(); err != nil {
			log.Error("Error in CreateRootRepoIfNeeded", err)
		}
	}()

	go func() {
		if err := NewServerHttp(conf, repoManager).ListenAndServe(); err != nil {
			log.Error("Http closed", err)
		}
	}()

	if err := NewServerSsh(conf, repoManager, userManager, pluginManager, backgroundManager).ListenAndServe(); err != nil {
		log.Error("Ssh closed", err)
	}
}
