package gitroot

import "strings"

func parseConf(conf string) []map[string]string {
	objs := strings.Split(conf, "_|_")
	res := make([]map[string]string, len(objs))
	for i, obj := range objs {
		props := strings.Split(obj, "_;_")
		res[i] = make(map[string]string)
		for _, prop := range props {
			if strings.Contains(prop, "\"=\"") {
				keyValue := strings.Split(prop, "\"=\"")
				key := strings.TrimPrefix(keyValue[0], "\"")
				value := strings.TrimSuffix(keyValue[1], "\"")
				res[i][key] = value
			}
		}
	}
	return res
}
