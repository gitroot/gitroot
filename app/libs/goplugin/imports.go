package gitroot

//go:wasmimport gitroot modifyContent
func _modifyContent(filepathPtr, filepathSsize uint32, contentPtr, contentSize uint32)

//go:wasmimport gitroot modifyWebContent
func _modifyWebContent(filepathPtr, filepathSsize uint32, contentPtr, contentSize uint32)

//go:wasmimport gitroot modifyCacheContent
func _modifyCacheContent(filepathPtr, filepathSsize uint32, contentPtr, contentSize uint32)

//go:wasmimport gitroot commitAll
func _commitAll(messagePtr, messageSize uint32)

//go:wasmimport gitroot diffWithParent
func _diffWithParent(filePtr, fileSize, hashPtr, hashSize, resPtr uint32) (resSize uint32)

//go:wasmimport gitroot log
func _log(messagePtr, messageSize uint32)

//go:wasmimport gitroot logError
func _logError(messagePtr, messageSize, errPtr, errSize uint32)

//go:wasmimport gitroot merge
func _merge(fromPtr, fromSize, toPtr, toSize uint32)
