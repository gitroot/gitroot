package gitroot

import "strings"

//go:wasmexport init
func Init(repoNamePtr, repoNameSize, confPtr, confSize uint32) {
	repoName := ptrToString(repoNamePtr, repoNameSize)
	conf := make([]map[string]string, 0)
	if confSize > 0 {
		conf = parseConf(ptrToString(confPtr, confSize))
	}
	server.plugin.Init(repoName, conf)
}

//go:wasmexport startCommit
func StartCommit(branchPtr, branchSize, hashPrt, hashSize, msgPrt, msgSize, sshUserPtr, sshUserSize uint32) {
	commit := Commit{
		Branch:  ptrToString(branchPtr, branchSize),
		Hash:    ptrToString(hashPrt, hashSize),
		Message: ptrToString(msgPrt, msgSize),
		SshUser: ptrToString(sshUserPtr, sshUserSize),
	}
	server.plugin.StartCommit(commit)
}

//go:wasmexport addFile
func AddFile(ptr, size uint32) {
	server.plugin.AddFile(ptrToString(ptr, size))
}

//go:wasmexport modFile
func ModFile(ptr, size uint32) {
	server.plugin.ModFile(ptrToString(ptr, size))
}

//go:wasmexport delFile
func DelFile(ptr, size uint32) {
	server.plugin.DelFile(ptrToString(ptr, size))
}

//go:wasmexport renameFile
func RenameFile(fromPtr, fromSize, toPtr, toSize uint32) {
	server.plugin.RenameFile(ptrToString(fromPtr, fromSize), ptrToString(toPtr, toSize))
}

//go:wasmexport endCommit
func EndCommit(branchPtr, branchSize, hashPrt, hashSize, msgPrt, msgSize uint32) {
	commit := Commit{
		Branch:  ptrToString(branchPtr, branchSize),
		Hash:    ptrToString(hashPrt, hashSize),
		Message: ptrToString(msgPrt, msgSize),
	}
	server.plugin.EndCommit(commit)
}

//go:wasmexport finish
func Finish() {
	server.plugin.Finish()
}

//go:wasmexport defaultConfiguration
func DefaultConfiguration() uint64 {
	ptr, size := stringToLeakedPtr(server.defaultConfiguration)
	return (uint64(ptr) << uint64(32)) | uint64(size)
}

//go:wasmexport defaultPath
func DefaultPath() uint64 {
	ptr, size := stringToLeakedPtr(server.defaultPath)
	return (uint64(ptr) << uint64(32)) | uint64(size)
}

//go:wasmexport defaultBranch
func DefaultBranch() uint64 {
	ptr, size := stringToLeakedPtr(strings.Join(server.defaultBranch, ";"))
	return (uint64(ptr) << uint64(32)) | uint64(size)
}
