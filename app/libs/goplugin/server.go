package gitroot

// #include <stdlib.h>
import "C"

import (
	"errors"
	"io/fs"
	"os"
	"runtime"
	"strings"
	"unsafe"
)

type PluginFactory = func(server Server) Plugin

type Commit struct {
	Branch  string
	Hash    string
	Message string
	SshUser string
}

type Plugin interface {
	Init(repoName string, conf []map[string]string) error
	StartCommit(commit Commit) error
	AddFile(path string) error
	ModFile(path string) error
	DelFile(path string) error
	RenameFile(fromPath string, toPath string) error
	EndCommit(commit Commit) error
	Finish() error
}

type Server interface {
	Worktree() fs.FS
	Webcontent() fs.FS
	Cache() fs.FS
	ModifyContent(filepath, content string)
	ModifyWebContent(filepath, content string)
	ModifyCacheContent(filepath, content string)
	CommitAllIfNeeded(message string)
	DiffWithParent(file string, hash string) (string, error)
	Log(message string)
	LogError(message string, err error)
	Merge(from string, to string)
}

type pServer struct {
	plugin               Plugin
	defaultConfiguration string
	defaultPath          string
	defaultBranch        []string
	atLeastOneChange     bool
}

var server = &pServer{
	atLeastOneChange: false,
}

func Register(defaultConfiguration string, defaultPath string, branch []string, p PluginFactory) {
	server.defaultConfiguration = defaultConfiguration
	server.defaultPath = defaultPath
	server.defaultBranch = branch
	server.plugin = p(server)
}

func (s *pServer) Worktree() fs.FS {
	return os.DirFS("/worktree")
}

func (s *pServer) Webcontent() fs.FS {
	return os.DirFS("/webcontent")
}

func (s *pServer) Cache() fs.FS {
	return os.DirFS("/cache")
}

func (s *pServer) ModifyContent(filepath, content string) {
	s.atLeastOneChange = true
	ptr, size := stringToPtr(filepath)
	ptr2, size2 := stringToPtr(content)
	_modifyContent(ptr, size, ptr2, size2)
	runtime.KeepAlive(filepath)
	runtime.KeepAlive(content)
}

func (s *pServer) ModifyWebContent(filepath, content string) {
	ptr, size := stringToPtr(filepath)
	ptr2, size2 := stringToPtr(content)
	_modifyWebContent(ptr, size, ptr2, size2)
	runtime.KeepAlive(filepath)
	runtime.KeepAlive(content)
}

func (s *pServer) ModifyCacheContent(filepath, content string) {
	ptr, size := stringToPtr(filepath)
	ptr2, size2 := stringToPtr(content)
	_modifyCacheContent(ptr, size, ptr2, size2)
	runtime.KeepAlive(filepath)
	runtime.KeepAlive(content)
}

func (s *pServer) CommitAllIfNeeded(message string) {
	if s.atLeastOneChange {
		ptr, size := stringToPtr(message)
		_commitAll(ptr, size)
		runtime.KeepAlive(message)
	}
}

func (s *pServer) DiffWithParent(file string, hash string) (string, error) {
	ptrFile, sizeFile := stringToPtr(file)
	ptrHash, sizeHash := stringToPtr(hash)
	ptrDiff := uint32(uintptr(unsafe.Pointer(unsafe.StringData(strings.Repeat("?", 2048))))) // TODO only 2048 chars, find better way
	sizeDiff := _diffWithParent(ptrFile, sizeFile, ptrHash, sizeHash, ptrDiff)
	if sizeDiff == 0 {
		return "", errors.New("can't make diff")
	}
	return ptrToString(ptrDiff, sizeDiff), nil
}

func (s *pServer) Log(message string) {
	ptr, size := stringToPtr(message)
	_log(ptr, size)
	runtime.KeepAlive(message)
}

func (s *pServer) LogError(message string, err error) {
	ptr, size := stringToPtr(message)
	errPtr, errSize := stringToPtr(err.Error())
	_logError(ptr, size, errPtr, errSize)
	runtime.KeepAlive(message)
	runtime.KeepAlive(err)
}

func (s *pServer) Merge(from string, to string) {
	fromPtr, fromSize := stringToPtr(from)
	toPtr, toSize := stringToPtr(to)
	_merge(fromPtr, fromSize, toPtr, toSize)
	runtime.KeepAlive(from)
	runtime.KeepAlive(to)
}
