#!/usr/bin/env bash

ROOT_REPO_NAME="root"
REPO1_NAME="repo1"
REPO2_NAME="repo2"

SCRIPT_DIR=$( cd -- "$( dirname -- "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )

SERVER_PORT="4545"
SERVER_DATA_DIR="/tmp/gitrootData"

NB_BRANCH=50
NB_COMMIT=50

quiet_git() {
    echo "🚀 git $@" >> /tmp/mylog.txt
    GIT_PROTOCOL=version=1 GIT_TRACE=true GIT_TRACE_PACKET=true git "$@" &>> /tmp/mylog.txt
}

real_quiet_git() {
    git "$@" &>> /dev/null
}

report() {
    echo "$1" >> /tmp/mylog.txt
    echo "$1"
}

APP=$(lsof -i tcp:${SERVER_PORT} | awk 'NR!=1 {print $2}') 
if [ -z "$APP" ]; then
    report "🟢 Gitroot not launched"
else 
    kill ${APP}
    report "🟢 Gitroot killed"
fi

##### launch gitroot
report "🏁 launch gitroot"

cd ${SCRIPT_DIR}/../server
GIT_TRACE_PACKET=true go run . -data="${SERVER_DATA_DIR}" &>> /tmp/mylog.txt &

sleep 1.3

cd /tmp/${REPO1_NAME}

real_quiet_git checkout main

for (( i=0; i<=$NB_BRANCH; i++ ))
do
  real_quiet_git branch -D bigCommitsBranch_$i
done

for (( i=0; i<=$NB_BRANCH; i++ ))
do
  SHOULD_START_MAIN=$(shuf -i 0-2 -n 1)
  if (( SHOULD_START_MAIN > 1 )); then
    real_quiet_git checkout main
  elif (( SHOULD_START_MAIN > 0 )); then
    real_quiet_git checkout -
  fi

  real_quiet_git checkout -b bigCommitsBranch_$i

  for (( j=0; j<=$NB_COMMIT; j++ ))
  do
    echo "${i}_${j}" >> issues/bigCommits.md
    real_quiet_git add .
    real_quiet_git commit -m "my ${i}_${j} commit"
  done

  quiet_git push -f origin bigCommitsBranch_$i
  report "🟢 push bigCommitsBranch_$i"
done

cd /tmp/${REPO1_NAME}_2
real_quiet_git checkout main
quiet_git pull --rebase

cd /tmp/${REPO1_NAME}

for (( i=0; i<=$NB_BRANCH; i++ ))
do
  real_quiet_git checkout bigCommitsBranch_$i

  RESET=$(shuf -i 0-$NB_COMMIT -n 1)
  real_quiet_git reset --hard HEAD~$RESET

  count=$((NB_COMMIT-RESET))

  for (( j=0; j<=$count; j++ ))
  do
    echo "${i}_${j}_2" >> issues/bigCommits.md
    real_quiet_git add .
    real_quiet_git commit -m "my ${i}_${j}_2 commit"
  done

  real_quiet_git push -f origin bigCommitsBranch_$i
  report "🟢 push bigCommitsBranch_$i"
done

cd /tmp/${REPO1_NAME}_2
quiet_git pull --rebase