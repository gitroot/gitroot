#!/usr/bin/env bash

SCRIPT_DIR=$( cd -- "$( dirname -- "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )

SERVER_PORT="4545"
SERVER_URL=${1:-"127.0.0.1:$SERVER_PORT"}
SERVER_DATA_DIR="/tmp/gitrootData"

ROOT_REPO_NAME="root"
ROOT_REPO_URL="ssh://user@${SERVER_URL}/${ROOT_REPO_NAME}.git"
REPO1_NAME="repo1"
REPO1_URL="ssh://user@${SERVER_URL}/${REPO1_NAME}"

SSH_KEY="${SCRIPT_DIR}/user1/ed25519"
SSH_KEY2="${SCRIPT_DIR}/user2/ed25519"

report() {
    echo "$1" >> /tmp/mylog.txt
    echo -e "$1"
}

quiet_git() {
    echo "🚀 git $@" >> /tmp/mylog.txt
    GIT_TRACE=false GIT_TRACE_PACKET=false git "$@" &>> /tmp/mylog.txt
}

my_read() {
    tput sc
    read  -n 1 -p "$1"
    tput rc
    tput ed
}

report ""
report " _______ __________________ _______  _______  _______ _________"
report "(  ____ \\__   __/\__   __/(  ____ )(  ___  )(  ___  )\__   __/"
report "| (    \/   ) (      ) (   | (    )|| (   ) || (   ) |   ) (   "
report "| |         | |      | |   | (____)|| |   | || |   | |   | |   "
report "| | ____    | |      | |   |     __)| |   | || |   | |   | |   "
report "| | \_  )   | |      | |   | (\ (   | |   | || |   | |   | |   "
report "| (___) |___) (___   | |   | ) \ \__| (___) || (___) |   | |   "
report "(_______)\_______/   )_(   |/   \__/(_______)(_______)   )_(   "
report ""
report " | 🏁 means start a new chapter"
report " | 👽 means a command will be launch"
report " | 🟢 means a command is successfull"
report " | 🛑 means a command has fail"
my_read "Press enter to start"

##### clean
report "🏁 clean"

cd /tmp
rm -rf ${SERVER_DATA_DIR}
rm -rf ${ROOT_REPO_NAME}
rm -rf ${REPO1_NAME}
rm -rf ${REPO2_NAME}
rm -rf ${REPO1_NAME}_2
rm -rf ${REPO2_NAME}_2
rm -f /tmp/mylog.txt
APP=$(lsof -i tcp:${SERVER_PORT} | awk 'NR!=1 {print $2}') 
if [ -z "$APP" ]; then
    report " | 🟢 Gitroot not launched"
else 
    kill ${APP}
    report " | 🟢 Gitroot killed"
fi
ssh-keygen -f "$HOME/.ssh/known_hosts" -R "[127.0.0.1]:$SERVER_PORT" &>> /tmp/mylog.txt

##### init
report "🏁 before start configure the demo: "
read -p " | Name of the forge administrator: " NAME_ADMIN
NAME_ADMIN="${NAME_ADMIN:-admin}"
read -p " | Name of the owner project: " NAME_OWNER_PROJECT
NAME_OWNER_PROJECT="${NAME_OWNER_PROJECT:-owner}"
read -p " | Name of the project: " NAME_PROJECT
REPO1_NAME="${NAME_PROJECT:-project}"
REPO1_URL="ssh://user@${SERVER_URL}/${REPO1_NAME}"
read -p " | Name of the contributor: " NAME_CONTRIBUTOR
NAME_CONTRIBUTOR="${NAME_CONTRIBUTOR:-contributor}"

##### launch gitroot
report "🏁 $NAME_ADMIN launch gitroot: "
report " | 👽 ./gitroot -data=\"${SERVER_DATA_DIR}\""

cd ${SCRIPT_DIR}/../server
GIT_TRACE_PACKET=false go run . -data="${SERVER_DATA_DIR}" &>> /tmp/mylog.txt &

report " | 🟢 Gitroot launched"

codium -n "${SERVER_DATA_DIR}"

my_read "Press enter to clone root repo"

report " | 👽 git clone ${ROOT_REPO_URL}"

cd /tmp
quiet_git clone -c "core.sshCommand=ssh -i ${SSH_KEY} -o IdentitiesOnly=yes -o StrictHostKeyChecking=accept-new" ${ROOT_REPO_URL}
cd ${ROOT_REPO_NAME}

my_read "Press enter to open /tmp/${ROOT_REPO_NAME}"

codium -n "/tmp/${ROOT_REPO_NAME}"

my_read "Press enter to configure"

report " | 👽 .gitroot/init.sh"

.gitroot/init.sh --pubKey "${SSH_KEY}.pub" --privKey "${SSH_KEY}" --email $NAME_ADMIN@gitroot.com --name $NAME_ADMIN >> /tmp/mylog.txt

my_read "Press enter to commit user"

quiet_git add .
quiet_git commit -m "init user"
report " | 🟢 ${ROOT_REPO_NAME} commit user"

my_read "Press enter to create $REPO1_NAME repo"

printf "${REPO1_NAME}:\n  defaultbranch: main" >> .gitroot/repositories.yml

codium "/tmp/${ROOT_REPO_NAME}/.gitroot/repositories.yml"

my_read "Press enter to commit first repo"

quiet_git add .
quiet_git commit -m "create first repo"
quiet_git push origin main
report " | 🟢 ${ROOT_REPO_NAME} push $REPO1_NAME repo"

report "🏁 $NAME_OWNER_PROJECT use $REPO1_NAME"

my_read "Press enter to clone first $REPO1_NAME"

cd /tmp
GIT_SSH_COMMAND="ssh -i ${SSH_KEY} -o IdentitiesOnly=yes" quiet_git clone "${REPO1_URL}"
cd ${REPO1_NAME}
.gitroot/init.sh --pubKey "${SSH_KEY}.pub" --privKey "${SSH_KEY}" --email $NAME_OWNER_PROJECT@gitroot.com --name $NAME_OWNER_PROJECT >> /tmp/mylog.txt

report " | 👽 git clone ${REPO1_URL}"
report " | 🟢 ${REPO1_NAME} clone"
report " | 👽 .gitroot/init.sh"
report " | 🟢 ${REPO1_NAME} init"
report " | 👽 git commit -am \"init user\" && git push"
quiet_git add .
quiet_git commit -m "init user"
quiet_git push
report " | 🟢 ${REPO1_NAME} push user"

codium -n "/tmp/${REPO1_NAME}"

my_read "Press enter to add $NAME_CONTRIBUTOR"

report "🏁 $NAME_CONTRIBUTOR want to use $REPO1_NAME"
report " | 👽 git clone ${REPO1_URL} ${REPO1_NAME}_2"

cd /tmp
GIT_SSH_COMMAND="ssh -i ${SSH_KEY2} -o IdentitiesOnly=yes" quiet_git clone "${REPO1_URL}" ${REPO1_NAME}_2

report " | 🟢 ${REPO1_NAME}_2 clone"

cd ${REPO1_NAME}_2
.gitroot/init.sh --pubKey "${SSH_KEY2}.pub" --privKey "${SSH_KEY2}" --email $NAME_CONTRIBUTOR@gitroot.com --name $NAME_CONTRIBUTOR >> /tmp/mylog.txt

report " | 👽 .gitroot/init.sh \$SSH_KEY2"
codium -n "/tmp/${REPO1_NAME}_2"

my_read "Press enter to modif and commit"

echo "hello" > README.md

quiet_git add .
quiet_git commit -m "modif $NAME_CONTRIBUTOR"
report " | 🟢 ${REPO1_NAME}_2 commit $NAME_CONTRIBUTOR"

my_read "⚠ Press enter to push ⚠"

report " | 👽 git push origin main"

tput sc
git push origin main
report " | 🛑 ${REPO1_NAME}_2 push $NAME_CONTRIBUTOR on main"
read  -n 1 -p "??"
tput rc
tput ed
report " | 👽 git reset --soft HEAD^1"
report " | 👽 git checkout -b init_user2"
report " | 👽 git commit -am \"init $NAME_CONTRIBUTOR\""
report " | 👽 git push origin init_user2"

quiet_git reset --soft HEAD^1
quiet_git checkout -b init_user2
quiet_git add .
quiet_git commit -m "init $NAME_CONTRIBUTOR"
quiet_git push origin init_user2

report " | 🟢 ${REPO1_NAME}_2 push $NAME_CONTRIBUTOR on init_user2"

my_read "Press enter continue..."

report "🏁 $NAME_ADMIN install plugin"

cd /tmp/${ROOT_REPO_NAME}
echo "- url: '${SCRIPT_DIR}/../plugins/ladybug/ladybug-0.0.1.wasm'" >> .gitroot/plugins.yml
echo "  crc32: null" >> .gitroot/plugins.yml
echo "  name: ladybug" >> .gitroot/plugins.yml

codium /tmp/${ROOT_REPO_NAME}/.gitroot/plugins.yml

my_read "Press enter to commit and push"

quiet_git add .
quiet_git commit -m "init ladybug plugin"
quiet_git push origin main

report " | 🟢 ${ROOT_REPO_NAME} plugin ladybug installed"

my_read "Press enter to use in $REPO1_NAME"

cd /tmp/${REPO1_NAME}
report " | 👽 git pull in ${REPO1_NAME}"
quiet_git pull
codium /tmp/${REPO1_NAME}/.gitroot/plugins.yml

my_read "Press enter to active in repo1"

sed -i -e 's/active: false/active: true/g' .gitroot/plugins.yml

my_read "Press enter to commit"

quiet_git add .
quiet_git commit -m "init ladybug plugin"
quiet_git push origin main
report " | 🟢 ${REPO1_NAME} plugin ladybug activated"

my_read "Press enter to create first issue"

mkdir issues
echo "#my Issue" >> issues/1.md
echo "Beatiful issue" >> issues/1.md

my_read "Press enter to commit first issue"

quiet_git add .
quiet_git commit -m "first issue in second repo"
quiet_git push origin main
report " | 👽 git push origin main"

sleep 1

quiet_git pull
report " | 👽 git pull origin main"

my_read "Press enter to install silo"

cd /tmp/${ROOT_REPO_NAME}
echo "- url: '${SCRIPT_DIR}/../plugins/silo/silo-0.0.1.wasm'" >> .gitroot/plugins.yml
echo "  crc32: null" >> .gitroot/plugins.yml
echo "  name: silo" >> .gitroot/plugins.yml
quiet_git add .
quiet_git commit -m "init silo plugin"
quiet_git push origin main
report " | 🟢 ${ROOT_REPO_NAME} plugin silo installed"
sleep 1
cd /tmp/${REPO1_NAME}
report " | 👽 git pull in ${REPO1_NAME}"
quiet_git pull

sed -i -e 's/active: false/active: true/g' .gitroot/plugins.yml
quiet_git add .
quiet_git commit -m "init silo plugin"
quiet_git push origin main
report " | 👽 git push origin main"

sleep 1

quiet_git pull
report " | 👽 git pull origin main"
report " | 🟢 ${REPO1_NAME} plugin silo activated"

my_read "Press enter to create 50 issues"

rm issues/1.md
for (( i=0; i<=50; i++ ))
do
    echo "# my Issue $i" >> issues/$i.md
    echo "Beatiful issue" >> issues/$i.md
done
quiet_git add .
quiet_git commit -m "50 issues created"
quiet_git push origin main
report " | 🟢 ${REPO1_NAME} 50 issues created"

my_read "Press enter to pull"
quiet_git pull origin main
report " | 🟢 ${REPO1_NAME} 10 issues organized"
codium /tmp/${REPO1_NAME}/boards/triage.md

my_read "Press enter to install grafter"

cd /tmp/${ROOT_REPO_NAME}
echo "- url: '${SCRIPT_DIR}/../plugins/grafter/grafter-0.0.1.wasm'" >> .gitroot/plugins.yml
echo "  crc32: null" >> .gitroot/plugins.yml
echo "  name: grafter" >> .gitroot/plugins.yml
quiet_git add .
quiet_git commit -m "init grafter plugin"
quiet_git push origin main
report " | 🟢 ${ROOT_REPO_NAME} plugin grafter installed"
sleep 1
cd /tmp/${REPO1_NAME}
report " | 👽 git pull in ${REPO1_NAME}"
quiet_git pull

sed -i -e 's/active: false/active: true/g' .gitroot/plugins.yml
quiet_git add .
quiet_git commit -m "init grafter plugin"
quiet_git push origin main

my_read "Press enter to graft"

report " | 👽 git checkout -b first_graft"
quiet_git checkout -b first_graft
echo "#My fabulous project" >> README.md
codium /tmp/${REPO1_NAME}/README.md

my_read "Press enter to send graft"
quiet_git add .
quiet_git commit -m "first graft"
report " | 👽 git push origin first_graft"
quiet_git push origin first_graft
report " | 🟢 First graft created"

sleep 1
quiet_git pull origin first_graft
codium /tmp/${REPO1_NAME}/grafts/first_graft.md

my_read "Press enter to merge graft"

echo "---" >> grafts/first_graft.md
echo "/merge" >> grafts/first_graft.md

my_read "Press enter to merge graft"

quiet_git add .
quiet_git commit -m "accept first graft"
report " | 👽 git push origin first_graft"
quiet_git push origin first_graft
report " | 🟢 First graft merged"

sleep 1
quiet_git checkout main
report " | 👽 git checkout main"
quiet_git pull origin main
report " | 👽 git pull origin main"
codium /tmp/${REPO1_NAME}/README.md

my_read "Press enter to install apex"

cd /tmp/${ROOT_REPO_NAME}
echo "- url: '${SCRIPT_DIR}/../plugins/apex/apex-0.0.1.wasm'" >> .gitroot/plugins.yml
echo "  crc32: null" >> .gitroot/plugins.yml
echo "  name: apex" >> .gitroot/plugins.yml
quiet_git add .
quiet_git commit -m "init apex plugin"
quiet_git push origin main
report " | 🟢 ${ROOT_REPO_NAME} plugin apex installed"
sleep 1
cd /tmp/${REPO1_NAME}
report " | 👽 git pull in ${REPO1_NAME}"
quiet_git pull

sed -i -e 's/active: false/active: true/g' .gitroot/plugins.yml
quiet_git add .
quiet_git commit -m "init apex plugin"
quiet_git push origin main

report " | 🟢 http://127.0.0.1:4546/${REPO1_NAME}/"

my_read "Press enter to style"

quiet_git pull origin main

sed -i -e 's/active: true/active: false/g' .gitroot/plugins.yml

quiet_git add .
quiet_git commit -m "inactive all"
quiet_git push origin main

sed -i -e 's/active: false/active: true/g' .gitroot/plugins.yml
sed -i -e 's/style: simple.min.css/style: style.css/g' .gitroot/plugins.yml

cp ${SCRIPT_DIR}/../server/resources/styles/simple.min.css style.css
echo "body { background-color: 010409 }" >> style.css
echo "body > header { background-color: 010409; text-align: left; padding: 0 1rem }" >> style.css
echo "body > header h1 { font-size: 1rem }" >> style.css
echo "header > nav  ul { place-content: normal }" >> style.css
echo "header > nav  a { margin: 0 .5rem; border: 0 }" >> style.css

quiet_git add .
quiet_git commit -m "perso style"
quiet_git push origin main

my_read "Press enter to exit"