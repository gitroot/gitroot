---
id: '9df8'
priority: 50
sprint: ''
---

# Plugin path need love

Plugin path exec are matched against a string.

Need to be multiple strings for [`issues/*.md`, `projects2/issues/*.md`]

~~Need to have `**` possibility (not in go filematcher so find a lib or make manual think with a walk) for [`issues/**/*.md`]~~ done in Glob.go

Add `!` (not) in Glob.go, to be able to do [`issues/*.md`, `!issues/no_that.md`]
