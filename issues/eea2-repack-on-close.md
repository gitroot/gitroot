---
id: 'eea2'
priority: 50
sprint: ''
---

# Repack

Today when we close repository, we pack it. Sometimes it lead to error because other process had mounted repository in memory.

Found a way to do it correctly, maybe lock repo in read/write.

Maybe an async task in background with locked repository.

---

> Found a way to do it correctly, maybe lock repo in read/write.

Done

> Today when we close repository, we pack it

Not true anymore, need to find when run it
