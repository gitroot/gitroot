id="645e"
priority=10
sprint=""

---

# Repositories should use sha256 hash object

go-git permit to try sha256 hash object with `go run -tags sha256 ./app/server` and we need to force them in repo init options.

But it's not working. We need to debug this.
