---
id: '1108'
priority: 50
sprint: ''
---

# Silo duplicate last line on delete

In demo when I change metadata of an issue from `status: triage` to `status: open` in `boards/triage.md` the issue is deleted but last line is duplicate.
