---
id: '4c44'
priority: 50
sprint: ''
---

# Protect branches from force-push

Today only "main" branch is protected against force-push. Make an option to enable/desable branches.
