---
id: 'c95a'
priority: 50
sprint: ''
---

# Define plugin rights

If we add lot of fonctionalities like web, merge and exec cmd, user should be able to know and manage rights of each plugin.

Be default give rights they need, and user see them before activate the plugin or not.

Rights:

- Add/mod/del file
- Merge branchs (graft)
- Exec cmds (ci docker)
- call http (webhook, mastodon...)
- receive http
