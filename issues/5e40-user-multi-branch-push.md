---
id: '5e40'
priority: 50
sprint: ''
---

# User can create/update multi branches in 1 push

If a user do that, I suspect gitroot will not handle all possibilities correctly. Need to test and debug.
