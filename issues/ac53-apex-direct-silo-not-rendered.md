---
id: 'ac53'
priority: 50
sprint: ''
---

# Plugin need to be called on previous plugin commit

If I active ladybug, silo and apex:

- ladybug add metadata
- silo group them in report.md
- but apex don't know about silo report and don't generate html
