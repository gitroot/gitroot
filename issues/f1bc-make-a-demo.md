---
id: "f1bc"
priority: 100
sprint: ""
---

# It's time to make a demo server

To do that we need:

- a server
- doc:
  - host gitroot
  - add a repo
- bugs:
  - ~~rename root repo to root instead of root.git~~
  - change 127.0.0.1 to 0.0.0.0
  - change ports 4545 and 4546 --> config in root
  - clean users.yml
    - delete not used property
    - add `details:users/pseudo.md` for all others
  - ~~an external user should be able to create a repo~~
    - ~~add ssh key in repositories.yml~~
  - ~~users.yml modified in master instead of branch when new branch~~
  - ~~user of main should be able to modify current branch (to review)~~
  - plugin url implem `file://` or `http://`
  - ~~plugin not configurable/activable on root repository~~
