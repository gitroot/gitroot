---
id: 'f17d'
priority: 50
sprint: ''
---

# Plugin right to write

Today plugin can write in branch without restriction.

Should pass through same code as user to check if a plugin can write on a branch or need to make a branch and require graft to the target.
