---
id: '9a9c'
priority: 50
sprint: ''
---

# A user can be in multi groups

Today if a user is in "owner" group it can touch main branch. But what if he want to add another branch to work on a feature?
