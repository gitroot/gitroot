---
id: 'f1ab'
priority: 50
sprint: ''
---

# Garbage collecte plugins

Plugins are mounted in runtine and never closed/deleted.

Find a way to delete them:

- based on usage?
- regularly?
