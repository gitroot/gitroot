---
id: '12e2'
priority: 50
sprint: ''
---

# Grafter

Permit to requiere merge from branch to branch

- describe with md (meta from, to, author, reviewer, labels...)
- reviewer make review in ide (code worktree)
- when reviewer push grafter make a diff with previous commit and note in graft.md file all files/line modified
- author can view questions/code change in ide
- when author push grafter make a diff with previous commit and note in graft.md file all files/line modified
- when assignee add a comment `/merge` grafter make the merge
