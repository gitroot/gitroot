---
id: '6eb6'
priority: 50
sprint: ''
---

# A plugin activation should run it agasint defaultBranch~HEAD

If you activate a plugin after using gitroot, this plugin should be called on main.

Example:

- use plugin issue and create 3 issues
- silo active with conf
- board should been created
