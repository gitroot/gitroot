---
id: '8415'
priority: 50
sprint: ''
---

# Default conf

Plugin should give ti's default conf to be added at install time.

For exemple issue plugin should be able to:

- generate issue.sh
- add default conf in .gitroot/plugins.yml

What about all files present before installing plugin? Should plugin be runed against them or not?
