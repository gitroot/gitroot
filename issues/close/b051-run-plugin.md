---
id: 'b051'
priority: 50
sprint: ''
---

# Change way plugins run

Today, we do:

- for each plugin
- for each cmds
- for each files

It could be better to do:

- for each cmds
- for each files
- for each plugin

Like that we checkout and compute diff only one time by cmd.
