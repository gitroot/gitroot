---
id: '6c5f'
priority: 99
sprint: ''
---

# Plugin grafter /merge scenario

Today everyone can send a `/merge` in a graft and the plugin (if it can: no rebase) will merge it.

We need to check that the commiter of the `/merge` command has the right to merge this branch on the target branch.
