---
id: 'b1ce'
priority: 50
sprint: ''
---

# User push a new branch, what next?

Today when a user push a new branch, gitroot add a new group in users.yaml in main branch to be sure only him can change it.

Maybe we should update users.yaml in the branch itself (to not pollute main if the branch is never merged).

But if we do it, we need to check in ssh.go for main user.yaml AND branch user.yaml.
