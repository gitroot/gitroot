In ssh we use `commitOld.Patch(commitNew)` instead of `commitOld.PatchContext(commitNew)`. Add a context to the ssh connection and use it.
