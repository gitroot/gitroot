---
id: 'e8b2'
priority: 50
sprint: ''
---

# Plugin silo, delete and format: embed

If you delete a file referenced in silo, they will be wrong on delete because silo only remove 1 line.
