---
id: 'fd72'
priority: 50
sprint: ''
---

# Plugin silo

This plugin permit to aggregate files ref to a new file.

Usefull for:

- make a roadmap: `select issues/*.md where metadata="kind:roadmap" keep title as link into boards/roadmap`
- make todo is a lie `select * where body="/TODO" keep path as link into boards/todo`
- make a big refactor issue `select * where body="constructor(" path as link into issues/refacto_angular_new_inject`

Permit to render a list, tasks or table
