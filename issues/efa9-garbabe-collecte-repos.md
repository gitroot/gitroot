---
id: 'efa9'
priority: 50
sprint: ''
---

# Repositories are cached but never released

Find a way to delete them from cache:

- based on usage?
- when nobody use one?
