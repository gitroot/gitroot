---
id: 'a735'
priority: 50
sprint: ''
---

# Gitroot instance configuration

Today lot of config are stored in [conf.go](../app/server/configuration/config.go). Find a way to generate that file inside the root repository to be able to be edited by admin.

Maybe add a flag to gitroot `gitroot --only-conf` to generate the file, admin can edit it before launch of gitroot.

Or make them fully runtime and update them on push. How to manage a change of ssh port?
