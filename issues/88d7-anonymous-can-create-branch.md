id="88d7"
priority=50
sprint=""

---

# Anonymous is able to create branch

In `app/server/fsloader.go` function `func (u *GitRootUser) CanWrite(branch string) bool` by default an user can create a new brach.

Repository owner should be able to configure that:

- anonymous ok
- only known user
- never
