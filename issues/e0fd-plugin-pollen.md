---
id: 'e0fd'
priority: 50
sprint: ''
---

# Plugin pollen

## Probleme

How a user can know there is a change on its forge, its repo or its branch?

## Solutions

### Flux rss

Flux rss is easy to implement and permit to give feedback easily

### Webhook

Simple solution could be used to send a message in chat apps like mattermost

### Email

We have user email but its not mandatory

### Fediverse

Could be cool to have them on fediverse where a project has an account
