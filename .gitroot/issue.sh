#!/usr/bin/env bash

function calc_crc16() 
{
    while read -r -d "" -n 1 ; do astring+=( "$REPLY" ) ; done <<< "$1"

    cnt=${#1}
    c=65535

    for ((x=0;x<$cnt;x++)); do
        char=$(printf '%d' \'"${1:$x:1}")
        e=$(((c ^ char) & 0x00FF))
        s=$(((e << 4) & 0x00FF))
        f=$(((e ^ s) & 0x00FF))
        r1=$(((c >> 8) ^ (f << 8) ^ (f << 3) ^ (f >> 4)))
        c=$r1
    done
    c=$((c ^ 0xffff))
    printf "%x" $c
}

crc16=$(calc_crc16 $1)
filepath=$(echo "issues/$crc16-$1.md")

if [ ! -e $filepath ]
then 
    echo "---" > $filepath
    printf "id: '$crc16'\npriority: 50\nsprint: ''\n---\n\n#Title\n\nDescription" >> $filepath
    echo "Created: $filepath"
else
    echo "Already exist: $filepath"
    exit 1
fi

