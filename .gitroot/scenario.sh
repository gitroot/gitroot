cd /tmp

cd ../ && ssh-keygen -f "/home/rmaneschi/.ssh/known_hosts" -R "[127.0.0.1]:4545" && \
    rm -rf forgeConfig && git clone ssh://rom@127.0.0.1:4545/forgeConfig.git && \
    cd forgeConfig && \
    cat allowed_signers && \
    git config gpg.ssh.allowedSignersFile "$(pwd)/allowed_signers" && \
    git show --show-signature

git config commit.gpgSign true && \
    git config gpg.format ssh && \  
    git config user.signingkey ~/.ssh/id_ed25519.pub && \
    echo "coucou" > README.md && \
    git add . && \
    git commit -S -am "3" && \
    git push origin main

echo "romain.maneschi@itk.fr ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIB5B3d8EUDMTbPn/zQaXf2NCWg47+H5etYQ3Q88xJm0v" >> allowed_signers && \
    git show --show-signature

cd ../ && ssh-keygen -f "/home/rmaneschi/.ssh/known_hosts" -R "[127.0.0.1]:4545" && \
    cd forgeConfig && \
    echo "newRepo" >> repositories && \
    git commit -am "add newRepo" -S && \
    git push origin main && \
    cd ../ &&
    git clone ssh://rom@127.0.0.1:4545/newRepo && \
    cd newRepo && \
    cat allowed_signers