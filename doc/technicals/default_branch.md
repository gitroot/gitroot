# Default Branch

As GitRoot works with diff you need to specify a defaultBranch. This defaultBranch represent the main state of your repository.

This branch is really important in GitRoot. Lots of actions are taken when the diff in this branch. For example, repositories are created when they are added in this branch. Another exemple is the plugins, which are installed only when the change is in defaultBranch.

It permit to users of your instance/repository to "propose" change without taking effect before an "owner" has accepted this changes.
