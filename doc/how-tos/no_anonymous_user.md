# Not accepting anonymous contribution

If you want to manage finely who can do what a simple way is to say that all users not registered has no branch in `.gitroot/users.yml`:

```yml
noAnonymous:
  branches:
    - name: "*"
  users: []
```

You will need to add each ssh key of your contributors see [add an user](./add_user.md) to do it.

> Be sure to have at least one user in main branch to be able to modify it, else no one will be able to modify anything.
