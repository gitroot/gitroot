# Delete a plugin

Remove lines from `.gitroot/plugins.yml` and GitRoot will delete the plugin. Be carrefull when doing that GitRoot will not update `.gitroot/plugins.yml` of sub-repo which could use it.

```diff
- - url: "file:///plugins/ladybug/ladybug-0.0.1.wasm"
-   crc32: null
-   name: ladybug
```

Commit your change and push `git add . && git commit -m "delete plugin ladybug" && git push`. When the change will be in the [defaultBranch](../index.md#technicals) nobody on your instance will be able to use it anymore.

> You need to prevent your users that the plugin no longer exist.
