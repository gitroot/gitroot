# All members can write on all branches

If you want all your registered users can write on all branches of each other add them in `.gitroot/users.yml`:

```yml
allAuthorized:
  branches:
    - name: "*"
  users:
    - pseudo: user
      avatar: ""
      emails:
      - user@gitroot.com
      ssh:
      - ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIICjrWwMnCd32Z10ZMEGT8zslAivtsFh0zj1Iss3C5Kt
    - pseudo: "user2"
        avatar: ""
        emails:
        - ""
        ssh:
        - ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIAMXujp1NVOs5n1t8KsgzJQnYSzhe+Ht9PpKlsdZqeDa
```

If `user2` create a `branch2` even `user` will be able to touch it.
