---
kind: doc
---

Running unknow sh is risky. Please be sure to review the script before run it. In case you prefer to do it manually this are the steps.

GitRoot need all your commits to be signed, to configure your local git client:

```sh
git config commit.gpgSign true
git config gpg.format ssh
git config user.signingkey $SSH_PUB_KEY
git config gpg.ssh.allowedSignersFile "$(pwd)/.gitroot/allowed_signers"
git config core.sshCommand "ssh -i ${SSH_PRIV_KEY} -o IdentitiesOnly=yes"
```

Additionally you can add your information in the allowed_signers (to be compatible with git):

```sh
echo "$EMAIL $(cat $SSH_PUB_KEY)" >> "$(pwd)/.gitroot/allowed_signers"
```

And don't forget to add yourself in the users.yml of GitRoot. See [Add a user](./add_user.md) for mode details.

Don't forget to configure user.name and user.email like any other git repository:

```sh
git config user.email "$EMAIL"
git config user.name "$NAME"
```
