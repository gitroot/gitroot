# Add a user

In any GitRoot repository you will find users right by branch in `.gitroot/users.yml`

```yml
owner:
  branches:
    - name: main
  users:
    - pseudo: GitRoot
      avatar: ""
      emails:
        - GitRoot@gitroot.com
      ssh:
        - ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIEnkDo30AcFQ5A0I1MgWXbJiYG26es5SHOX+lZlIUD9U
```

By default GitRoot has added itself in the `owner` group. That means gitroot is able to touch every files in the `main` branch. To add a user insert their ssh key in this files:

```diff
owner:
  branches:
    - name: main
  users:
    - pseudo: GitRoot
      avatar: ""
      emails:
        - GitRoot@gitroot.com
      ssh:
        - ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIEnkDo30AcFQ5A0I1MgWXbJiYG26es5SHOX+lZlIUD9U
+    - pseudo: user
+      avatar: ""
+      emails:
+        - user@gitroot.com
+      ssh:
+        - ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIICjrWwMnCd32Z10ZMEGT8zslAivtsFh0zj1Iss3C5Kt
```

Commit your change and push `git add . && git commit -m "add user" && git push`. Then next time `user` will push change to the `main` branch GitRoot will accept their changes.

When a user create a new branche GitRoot will automaticaly add him in this file.
