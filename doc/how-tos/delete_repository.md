# Delete a repository

To delete a repository you can simply delete the lines conresponding and GitRoot will delete it.

Open `.gitroot/repositories.yml` in the root repository:

```diff
root:
  defaultbranch: main
  owners: []
- repo1:
-   defaultbranch: main
```

Commit your change and push `git add . && git commit -m "delete repo1 repository" && git push`. When the change will be in the [defaultBranch](../index.md#technicals) GitRoot will delete it.
