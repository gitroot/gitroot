# Delete a user

To delete a user you need to remove him from `.gitroot/users.yml` in your defaultBranch.

```diff
owner:
  branches:
    - name: main
  users:
    - pseudo: GitRoot
      avatar: ""
      emails:
        - GitRoot@gitroot.com
      ssh:
        - ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIEnkDo30AcFQ5A0I1MgWXbJiYG26es5SHOX+lZlIUD9U
-    - pseudo: user
-      avatar: ""
-      emails:
-        - user@gitroot.com
-      ssh:
-        - ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIICjrWwMnCd32Z10ZMEGT8zslAivtsFh0zj1Iss3C5Kt
```

Commit your change and push `git add . && git commit -m "delete user" && git push`. Then next time `user` will push change to the `main` branch GitRoot will not accept their changes.

But `user` will be able to make a new branch. If you want to ban it see [ban an user](./ban_user.md).
