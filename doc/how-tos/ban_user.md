# Ban user

To make an user unable to push new changes, even in branch, you need to move it in a group without right:

```diff
owner:
  branches:
    - name: main
  users:
    - pseudo: GitRoot
      avatar: ""
      emails:
        - GitRoot@gitroot.com
      ssh:
        - ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIEnkDo30AcFQ5A0I1MgWXbJiYG26es5SHOX+lZlIUD9U
-    - pseudo: user
-      avatar: ""
-      emails:
-        - user@gitroot.com
-      ssh:
-        - ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIICjrWwMnCd32Z10ZMEGT8zslAivtsFh0zj1Iss3C5Kt
+
+banned:
+  branches: []
+  users:
+    - pseudo: "user"
+      avatar: ""
+      emails:
+        - ""
+      ssh:
+        - ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIICjrWwMnCd32Z10ZMEGT8zslAivtsFh0zj1Iss3C5Kt
```

Commit your change and push `git add . && git commit -m "ban user" && git push`. This `user` will not be able to push anymore.
