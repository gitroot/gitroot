# GitRoot documentation

Want to try GitRoot? Find how in this documentation.

## How to test

Today you need to deploy GitRoot before behing able to try it. Soon I hope to deploy a demo instance, like that you will be able to try it without install.

## Tutorials

- [From zero to hero](./tutorials/from_zero_to_hero.md): install GitRoot, create a repo, install a first plugin and use it

## How-To

### Repositories

- create a repository
- [delete a repository](./how-tos/delete_repository.md)

### Managing users

- [add an user](./how-tos/add_user.md)
- [delete an user](./how-tos/delete_user.md)
- [ban an user](./how-tos/ban_user.md)

### Plugins

- install a plugin
- active a plugin
- [delete a plugin](./how-tos/delete_plugin.md)

### More advanced usage

- [no anonymous push](./how-tos/no_anonymous_user.md)
- [all contributors can push on all branch](./how-tos/contributors_can_write_all.md)
- [manually init git client](./how-tos/manual_init_client.md)

## Technicals

- [default branch](./technicals/default_branch.md)
- .gitroot/users.yml specification
- .gitroot/repositories.yml specification
- .gitroot/plugins.yml specification
